package dbmanager;

import model.Restaurant;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import model.User;
import java.util.UUID;
import model.Picture;
import model.Rate;
import model.json.notification.AnswerNotification;
import model.json.notification.ClaimNotification;
import org.json.simple.JSONArray;
import support.authenticators.AuthenticatorFactory;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.beans.PropertyVetoException;
import java.util.Arrays;
import java.util.Collections;
import java.util.ResourceBundle;
import model.json.notification.ImageReportedNotification;
import model.json.notification.NewImageNotification;
import model.json.notification.RateNotification;

/**
 * Manage all the interaction with database.
 * 
 */
public final class DBManager implements Serializable {

    //connection pooling
    private ComboPooledDataSource ds;
    //authenticator subsystem selection string
    private final String authenticator;
    //unique instances of varius sup-part managers, used to group funcionality
    public final UserManager users = new UserManager();
    public final RestaurantManager restaurants = new RestaurantManager();
    public final RateManager rates = new RateManager();
    public final PictureManager pictures = new PictureManager();
    
    public UserManager getUsers() {
        return users;
    }
    
    public RestaurantManager getRestaurants() {
        return restaurants;
    }
    
    public RateManager getRates() {
        return rates;
    }
    
    public PictureManager getPictures() {
        return pictures;
    }
    
    /**
     * Constructor
     * @param dburl connection string
     * @param authenticator authenticator subsystem selection string
     * @throws SQLException 
     */
    public DBManager(String dburl, String authenticator) throws SQLException {
        this.startup(dburl);
        this.authenticator = authenticator;
    }

    /**
     * initialize a connection to dburl, supposed to be postgresql
     * @param dburl connection string
     * @throws SQLException 
     */
    public void startup(String dburl) throws SQLException {
        ds = new ComboPooledDataSource();
        try {
            ds.setDriverClass( "org.postgresql.Driver" );
        } catch (PropertyVetoException ex) {
            throw new RuntimeException(ex);
        }
        ds.setJdbcUrl(dburl);
        ds.setMaxPoolSize(20);
        ds.setMinPoolSize(5);
        ds.setInitialPoolSize(5);
    }

    /**
     * Clean up connections
     */
    public void shutdown() {
        ds.close();
    }

    public final class UserManager {
        /**
         * Authenticate a user and give back the full User object
         * @param email user email, must not be null
         * @param password user password, must not be null
         * @return User object or null on fail
         * @throws SQLException
         */
        public User authenticate(String email, String password) throws SQLException {
            try (Connection con = ds.getConnection()) {
                try (PreparedStatement stm = con.prepareStatement("SELECT id, name, surname, avatar, is_adm, password FROM users WHERE email = ?")) {
                    stm.setString(1, email);

                    try (ResultSet rs = stm.executeQuery()) {
                        if (rs.next() && AuthenticatorFactory.authenticate(password, rs.getString("password"))) {
                            User user = new User();
                            user.setId(rs.getLong("id"));
                            user.setEmail(email);
                            user.setName(rs.getString("name"));
                            user.setSurname(rs.getString("surname"));
                            String avatar = rs.getString("avatar");
                            if (!rs.wasNull()) {
                                user.setAvatar(avatar);
                            }
                            user.setAdmin(rs.getBoolean("is_adm"));
                            return user;
                        } else {
                            return null;
                        }
                    }
                }
            }
        }

        /**
         * Clean old registrations requests
         * @throws SQLException
         */
        public void cleanRegistrations() throws SQLException {
            try (Connection con = ds.getConnection()) {
                try (PreparedStatement stm = con.prepareStatement("DELETE FROM registrations WHERE expiration < now()")) {
                    stm.executeUpdate();
                }
            }
        }

        /**
         * Start a new registration process
         * @param email restaurant email, must not be null
         * @param password restaurant password, must not be null
         * @return UUID registration temporary identifier
         * @throws SQLException
         */
        public UUID startRegistration(String email, String password) throws SQLException {
            try (Connection con = ds.getConnection()) {
                con.setAutoCommit(false);
                //check if user is already registered and or registering
                try (PreparedStatement present = con.prepareStatement("SELECT TRUE FROM (SELECT email FROM users UNION SELECT email FROM registrations) AS emails WHERE email=?")) {
                    present.setString(1, email);
                    try (ResultSet rs = present.executeQuery()) {
                        if (rs.next()) {
                            return null;
                        }
                    }
                }
                PreparedStatement stm = con.prepareStatement("INSERT INTO registrations(email, password, uuid) VALUES(?, ?, ?)");
                UUID uuid = UUID.randomUUID();
                try {
                    stm.setString(1, email);
                    stm.setString(2, AuthenticatorFactory.getAuthenticator(authenticator).encrypt(password));
                    stm.setString(3, uuid.toString());
                    stm.executeUpdate();
                    con.commit();
                    return uuid;
                } finally {
                    stm.close();
                    con.setAutoCommit(true);
                }
            }
        }

        /**
         * Check if given UUID is a restaurant on registration
         * @param user
         * @return t/f
         * @throws SQLException
         */
        public boolean isRegistering(UUID user) throws SQLException {
            try (Connection con = ds.getConnection()) {
                try (PreparedStatement present = con.prepareStatement("SELECT TRUE FROM registrations WHERE uuid=?")) {
                    present.setString(1, user.toString());
                    try (ResultSet rs = present.executeQuery()) {
                        return rs.next();
                    }
                }
            }
        }

        /**
         * Complete registration for restaurant uuid, if it exists and password matches
         * @param uuid restaurant temporary unique identifier
         * @param name restaurant first name, must not be null
         * @param surname restaurant surname, must not be null
         * @param avatar image file, can be null
         * @return User object or null on fail
         * @throws SQLException
         * @throws IOException
         */
        public User completeRegistration(UUID uuid, String name, String surname, File avatar) throws SQLException, IOException {
            try (Connection con = ds.getConnection()) {
                con.setAutoCommit(false);
                String email;
                String password;
                long user_id;
                //select the user from currently registering one's
                try (PreparedStatement email_getter = con.prepareStatement("SELECT email, password FROM registrations WHERE uuid = ?")) {
                    email_getter.setString(1, uuid.toString());

                    try (ResultSet rs = email_getter.executeQuery()) {
                        if (rs.next()) { //check if the user is effectively registering
                            email = rs.getString("email");
                            password = rs.getString("password");
                        } else { //the user was not registering, fail
                            return null;
                        }
                    }
                    //insert new user data
                    try (PreparedStatement user_inserter = con.prepareStatement("INSERT INTO users(email, password, name, surname, avatar) VALUES(?, ?, ?, ?, ?) RETURNING id")) {
                        user_inserter.setString(1, email);
                        user_inserter.setString(2, password);
                        user_inserter.setString(3, name);
                        user_inserter.setString(4, surname);
                        if (avatar != null) {
                            user_inserter.setString(5, avatar.getName());
                        } else {
                            user_inserter.setNull(5, java.sql.Types.VARCHAR);
                        }
                        try (ResultSet rs1 = user_inserter.executeQuery()) {
                            rs1.next();
                            user_id = rs1.getLong("id");
                        }
                        //delete temporary data from registering table
                        try (PreparedStatement temp_deleter = con.prepareStatement("DELETE FROM registrations WHERE uuid = ?")) {
                            temp_deleter.setString(1, uuid.toString());
                            temp_deleter.executeUpdate();
                        }
                        con.commit();
                    } catch (Exception e) {
                        con.rollback();
                        throw e;
                    }
                } finally {
                    con.setAutoCommit(true);
                }
                User user = new User();
                user.setId(user_id);
                user.setAdmin(false);
                if(avatar != null) {
                    user.setAvatar(avatar.getName());
                }
                user.setEmail(email);
                user.setName(name);
                user.setSurname(surname);
                return user;
            }
        }

        /**
         * Check if a restaurant has restaurants
         * @param user restaurant object, must not be null
         * @return true if any, false otherwise
         * @throws SQLException
         */
        public boolean hasRestaurants(User user) throws SQLException {
            try (Connection con = ds.getConnection()) {
                try (PreparedStatement restaurant_checker = con.prepareStatement("SELECT TRUE FROM restaurants WHERE owner = ? AND ownerapprover IS NOT NULL")) {
                    restaurant_checker.setLong(1, user.getId());
                    ResultSet rs = restaurant_checker.executeQuery();
                    return rs.next();
                }
            }
        }

        /**
         * Get a model.User object from id
         * @param id user id
         * @return model.User or null if not found
         * @throws SQLException 
         */
        public User getById(long id) throws SQLException {
            try (Connection con = ds.getConnection()) {
                User ret;
                try (PreparedStatement user_getter = con.prepareStatement("SELECT email, name, surname, avatar, is_adm FROM users WHERE id=?")) {
                    user_getter.setLong(1, id);
                    try (ResultSet rs = user_getter.executeQuery()) {
                        if(rs.next()) { //check user existance
                            ret = new User();
                            ret.setId(id);
                            ret.setEmail(rs.getString("email"));
                            ret.setName(rs.getString("name"));
                            ret.setSurname(rs.getString("surname"));
                            String avatar = rs.getString("avatar");
                            if(!rs.wasNull()) {
                                ret.setAvatar(avatar);
                            }
                            ret.setAdmin(rs.getBoolean("is_adm"));
                        }
                        else { //user did not exist
                            ret = null;
                        }
                    }
                }
                return ret;
            }
        }

        /**
         * Get user rate
         * @param u model.User object representing the user
         * @return [0-1] rate
         * @throws SQLException 
         */
        public double getRate(User u) throws SQLException {
            try (Connection con = ds.getConnection()) {
                try (PreparedStatement rate_getter = con.prepareStatement("SELECT rate FROM user_stats WHERE id=?")) {
                    rate_getter.setLong(1, u.getId());
                    try (ResultSet rs = rate_getter.executeQuery()) {
                        rs.next();
                        return rs.getDouble(1);
                    }
                }
            }
        }

        /**
         * Get last n User reviews
         * @param u
         * @param n
         * @return list of rates, up to n
         * @throws SQLException 
         */
        public  List<Rate> getLastNRates(User u, long n) throws SQLException {
            try (Connection con = ds.getConnection()) {
                List<Rate> rates = new ArrayList<>();
                try (PreparedStatement rates_getter = con.prepareStatement("SELECT restaurant_id, day, rate, review, owner_answer, answer_approver, up, down FROM rates JOIN rate_stats USING(user_id, restaurant_id, day) WHERE user_id=? ORDER BY day DESC LIMIT ?")) {
                    rates_getter.setLong(1, u.getId());
                    rates_getter.setLong(2, n);
                    try (ResultSet rs = rates_getter.executeQuery()) {
                        while(rs.next()) {
                            Rate rt = new Rate();
                            rt.setUser(u);
                            rt.setRestaurant(restaurants.getById(rs.getLong("restaurant_id")));
                            rt.setDay(rs.getDate("day"));
                            rt.setRate(rs.getInt("rate"));
                            rt.setReview(rs.getString("review"));
                            rt.setDown(rs.getLong("down"));
                            rt.setUp(rs.getLong("up"));
                            String answer = rs.getString("owner_answer");
                            if(rs.wasNull()) {
                                rt.setOwnerAnswer(null);
                            }
                            else {
                                rt.setOwnerAnswer(answer);
                            }
                            long aa = rs.getLong("answer_approver");
                            if(rs.wasNull()) {
                                rt.setAnswerApprover(null);
                            }
                            else {
                                rt.setAnswerApprover(this.getById(aa));
                            }
                            rates.add(rt);
                        }
                    }
                }
                return rates;
            }
        }

        /**
         * Check if user has up-voted or down-voted a given review
         * @param user
         * @param rate
         * @return t/f
         * @throws SQLException 
         */
        public boolean hasRatedRate(User user, Rate rate) throws SQLException {
            try (Connection con = ds.getConnection()) {
                try (PreparedStatement stm = con.prepareStatement("SELECT TRUE FROM rates_rates WHERE user_id=? AND r_user_id=? AND r_restaurant_id=? AND r_day=?")) {
                    stm.setLong(1, user.getId());
                    stm.setLong(2, rate.getUser().getId());
                    stm.setLong(3, rate.getRestaurant().getId());
                    stm.setDate(4, new java.sql.Date(rate.getDay().getTime()));
                    try (ResultSet rs = stm.executeQuery()) {
                        return rs.next();
                    }
                }
            }
        }

        /**
         * Get the up/down vote of a given user on a given review,
         * supposed to be voted
         * @param user
         * @param rate
         * @return
         * @throws SQLException 
         */
        public boolean getRateRate(User user, Rate rate) throws SQLException {
            try (Connection con = ds.getConnection()) {
                try (PreparedStatement stm = con.prepareStatement("SELECT rate FROM rates_rates WHERE user_id=? AND r_user_id=? AND r_restaurant_id=? AND r_day=?")) {
                    stm.setLong(1, user.getId());
                    stm.setLong(2, rate.getUser().getId());
                    stm.setLong(3, rate.getRestaurant().getId());
                    stm.setDate(4, new java.sql.Date(rate.getDay().getTime()));
                    try (ResultSet rs = stm.executeQuery()) {
                        rs.next();
                        return rs.getBoolean("rate");
                    }
                }
            }
        }

        /**
         * Return the list of a restaurants owned by user
         * @param user
         * @return a valid <code>List$lt;Restaurant&gt;</code> filled with owned restaurants
         * @throws SQLException
         */
        public List<Restaurant> getRestaurants(User user) throws SQLException {
            try (Connection con = ds.getConnection()) {
                ArrayList<Restaurant> list = new ArrayList<>();
                try (PreparedStatement stm = con.prepareStatement("SELECT name, id, descr, site, lon, lat, price, ownerapprover, rate, classification, timetable::text, city, rates_count FROM restaurants JOIN restaurant_stats USING (id) WHERE owner = ? AND ownerapprover IS NOT NULL")) {
                    stm.setLong(1, user.getId());

                    try (ResultSet rs = stm.executeQuery()) {
                        while (rs.next()) {
                            Restaurant restaurant = new Restaurant();
                            restaurant.setName(rs.getString("name"));
                            restaurant.setDescr(rs.getString("descr"));
                            restaurant.setId(rs.getLong("id"));
                            restaurant.setLat(rs.getDouble("lat"));
                            restaurant.setLon(rs.getDouble("lon"));
                            restaurant.setOwner(user);
                            restaurant.setClassification(rs.getInt("classification"));
                            restaurant.setRate(rs.getDouble("rate"));
                            restaurant.setTimetable(rs.getString("timetable"));
                            long tmp = rs.getLong("ownerapprover");
                            if(!rs.wasNull()) {
                                restaurant.setOwnerapprover(users.getById(tmp));
                            }
                            restaurant.setPrice(Restaurant.Price.valueOf(rs.getString("price")));
                            String site = rs.getString("site");
                            if (rs.wasNull()) {
                                restaurant.setSite(null);
                            } else {
                                restaurant.setSite(site);
                            }
                            String city = rs.getString("city");
                            if (rs.wasNull()) {
                                restaurant.setCity(null);
                            } else {
                                restaurant.setCity(city);
                            }
                            restaurant.setRatesCount(rs.getLong("rates_count"));
                            list.add(restaurant);
                        }
                        return list;
                    }
                }
            }
        }

        /**
         * Check if user has rated a given restaurant today
         * @param user
         * @param restaurant
         * @return t/f
         * @throws SQLException 
         */
        public boolean hasRatedToday(User user, Restaurant restaurant) throws SQLException {
            try (Connection con = ds.getConnection()) {
                try(PreparedStatement stm = con.prepareStatement("SELECT 1 FROM rates WHERE user_id=? AND restaurant_id=? AND day=current_date")) {
                    stm.setLong(1, user.getId());
                    stm.setLong(2, restaurant.getId());
                    try(ResultSet rs = stm.executeQuery()) {
                        return rs.next();
                    }
                }
            }
        }

        /**
         * Change a user password
         * @param id user id
         * @param password old password
         * @param new_password new password
         * @return t/f success/fail
         * @throws SQLException 
         */
        public boolean modifyPassword(long id, String password, String new_password) throws SQLException {
            try (Connection con = ds.getConnection()) {
                try(PreparedStatement stm = con.prepareStatement("SELECT password FROM users WHERE id=?")) {
                    stm.setLong(1, id);
                    try(ResultSet rs = stm.executeQuery()) {
                        if (rs.next() && AuthenticatorFactory.authenticate(password, rs.getString("password"))) {
                            try(PreparedStatement updater = con.prepareStatement("UPDATE users SET password=? WHERE id=?")) {
                                updater.setString(1, AuthenticatorFactory.getAuthenticator(authenticator).encrypt(new_password));
                                updater.setLong(2, id);
                                updater.executeUpdate();
                                return true;
                            }
                        }
                        else {
                            return false;
                        }
                    }
                }
            }
        }

        /**
         * change a user avatar
         * @param user user object
         * @param avatar file path
         * @throws SQLException 
         */
        public void modifyAvatar(User user, String avatar) throws SQLException {
            try (Connection con = ds.getConnection()) {
                try(PreparedStatement stm = con.prepareStatement("UPDATE users SET avatar=? WHERE id=?")) {
                    stm.setString(1, avatar);
                    stm.setLong(2, user.getId());
                    stm.executeUpdate();
                }
            }
        }

        /**
         * generate a random password for the user with the given email
         * @param email
         * @return new password or null if not found
         * @throws SQLException 
         */
        public String generate_password(String email) throws SQLException {
            try (Connection con = ds.getConnection()) {
                try(PreparedStatement stm = con.prepareStatement("UPDATE users SET password=? WHERE email=?")) {
                    String pwd = Base64.getEncoder().encodeToString(UUID.randomUUID().toString().getBytes());
                    pwd = pwd.substring(0, 8);
                    stm.setString(1, AuthenticatorFactory.getAuthenticator(authenticator).encrypt(pwd));
                    stm.setString(2, email);
                    if(stm.executeUpdate() == 1) {
                        return pwd;
                    }
                    else {
                        return null;
                    }
                }
            }
        }
        
        /**
         * get notifications list for the given user
         * @param bundle internationalization bundle
         * @param user
         * @return
         * @throws SQLException 
         */
        public JSONArray getNotifications(ResourceBundle bundle, User user) throws SQLException {
            try (Connection con = ds.getConnection()) {
                JSONArray ret = new JSONArray();
                if(user.isAdmin()) {
                    try (PreparedStatement stm = con.prepareStatement("SELECT id, name, descr, site, lon, lat, price, owner, ownerapprover, modified FROM restaurants WHERE owner IS NOT NULL AND ownerapprover IS NULL")) {
                        try (ResultSet rs = stm.executeQuery()) {
                            while (rs.next()) {
                                Restaurant restaurant = new Restaurant();
                                restaurant.setName(rs.getString("name"));
                                restaurant.setDescr(rs.getString("descr"));
                                restaurant.setId(rs.getLong("id"));
                                restaurant.setLat(rs.getDouble("lat"));
                                restaurant.setLon(rs.getDouble("lon"));
                                restaurant.setOwner(users.getById(rs.getLong("owner")));
                                restaurant.setOwnerapprover(null);
                                restaurant.setPrice(Restaurant.Price.valueOf(rs.getString("price")));
                                restaurant.setModified(rs.getTimestamp("modified"));
                                String site = rs.getString("site");
                                if (rs.wasNull()) {
                                    restaurant.setSite(null);
                                } else {
                                    restaurant.setSite(site);
                                }
                                restaurants.getTypes(restaurant);
                                ret.add(new ClaimNotification(bundle, restaurant));
                            }
                        }
                    }
                    try (PreparedStatement rates_getter = con.prepareStatement("SELECT user_id, restaurant_id, day, rate, review, owner_answer, answer_approver, up, down, modified FROM rates JOIN rate_stats USING(user_id, restaurant_id, day) WHERE owner_answer IS NOT NULL AND answer_approver IS NULL")) {
                        try (ResultSet rs = rates_getter.executeQuery()) {
                            while(rs.next()) {
                                Rate rt = new Rate();
                                rt.setUser(users.getById(rs.getLong("user_id")));
                                rt.setRestaurant(restaurants.getById(rs.getLong("restaurant_id")));
                                rt.setDay(rs.getDate("day"));
                                rt.setRate(rs.getInt("rate"));
                                rt.setReview(rs.getString("review"));
                                rt.setDown(rs.getLong("down"));
                                rt.setUp(rs.getLong("up"));
                                rt.setModified(rs.getTimestamp("modified"));
                                String answer = rs.getString("owner_answer");
                                if(rs.wasNull()) {
                                    rt.setOwnerAnswer(null);
                                }
                                else {
                                    rt.setOwnerAnswer(answer);
                                }
                                long aa = rs.getLong("answer_approver");
                                if(rs.wasNull()) {
                                    rt.setAnswerApprover(null);
                                }
                                else {
                                    rt.setAnswerApprover(users.getById(aa));
                                }
                                ret.add(new AnswerNotification(bundle, rt));
                            }
                        }
                    }
                    try (PreparedStatement pictures_getter = con.prepareStatement("SELECT id, restaurant_id, modified FROM pictures WHERE nvs=FALSE")) {
                        try (ResultSet rs = pictures_getter.executeQuery()) {
                            while(rs.next()) {
                                Picture p = new Picture();
                                p.setId(rs.getLong("id"));
                                p.setRestaurant(restaurants.getById(rs.getLong("restaurant_id")));
                                p.setModified(rs.getTimestamp("modified"));
                                ret.add(new ImageReportedNotification(bundle, p));
                            }
                        }
                    }
                }
                try (PreparedStatement rates_getter = con.prepareStatement("SELECT user_id, day, rate, review, owner_answer, answer_approver, up, down, modified FROM rates JOIN rate_stats USING(user_id, restaurant_id, day) WHERE owner_answer IS NULL AND restaurant_id=?");
                     PreparedStatement pictures_getter = con.prepareStatement("SELECT id, modified FROM pictures WHERE restaurant_id=? AND nvs IS NULL")) {
                    for(Restaurant restaurant:this.getRestaurants(user)) {
                        rates_getter.setLong(1, restaurant.getId());
                        try (ResultSet rs = rates_getter.executeQuery()) {
                            while(rs.next()) {
                                Rate rt = new Rate();
                                rt.setUser(users.getById(rs.getLong("user_id")));
                                rt.setRestaurant(restaurant);
                                rt.setDay(rs.getDate("day"));
                                rt.setRate(rs.getInt("rate"));
                                rt.setReview(rs.getString("review"));
                                rt.setDown(rs.getLong("down"));
                                rt.setUp(rs.getLong("up"));
                                rt.setModified(rs.getTimestamp("modified"));
                                String answer = rs.getString("owner_answer");
                                if(rs.wasNull()) {
                                    rt.setOwnerAnswer(null);
                                }
                                else {
                                    rt.setOwnerAnswer(answer);
                                }
                                long aa = rs.getLong("answer_approver");
                                if(rs.wasNull()) {
                                    rt.setAnswerApprover(null);
                                }
                                else {
                                    rt.setAnswerApprover(users.getById(aa));
                                }
                                ret.add(new RateNotification(bundle, rt));
                            }
                        }
                        pictures_getter.setLong(1, restaurant.getId());
                        try (ResultSet rs = pictures_getter.executeQuery()) {
                            while(rs.next()) {
                                Picture p = new Picture();
                                p.setId(rs.getLong("id"));
                                p.setRestaurant(restaurant);
                                p.setModified(rs.getTimestamp("modified"));
                                ret.add(new NewImageNotification(bundle, p));
                            }
                        }
                    }
                }
                ret.sort(Collections.reverseOrder());
                return ret;
            }
        }
    }

    public final class RestaurantManager {

        /**
         * get a model.Restaurant from id
         * @param id
         * @return Restaurant object or null on not found
         * @throws SQLException 
         */
        public Restaurant getById(long id) throws SQLException {
            try (Connection con = ds.getConnection()) {
                ArrayList<Restaurant> list = new ArrayList<>();
                try (PreparedStatement stm = con.prepareStatement("SELECT name, descr, site, lon, lat, price, owner, ownerapprover, rate, classification, timetable::text, city, rates_count FROM restaurants JOIN restaurant_stats USING (id) WHERE id = ?")) {
                    stm.setLong(1, id);
                    try (ResultSet rs = stm.executeQuery()) {
                        if (rs.next()) {
                            Restaurant restaurant = new Restaurant();
                            restaurant.setName(rs.getString("name"));
                            restaurant.setDescr(rs.getString("descr"));
                            restaurant.setId(id);
                            restaurant.setLat(rs.getDouble("lat"));
                            restaurant.setLon(rs.getDouble("lon"));
                            restaurant.setClassification(rs.getInt("classification"));
                            restaurant.setRate(rs.getDouble("rate"));
                            restaurant.setTimetable(rs.getString("timetable"));
                            long tmp = rs.getLong("owner");
                            if(!rs.wasNull()) {
                                restaurant.setOwner(users.getById(tmp));
                                tmp = rs.getLong("ownerapprover");
                                if(!rs.wasNull()) {
                                    restaurant.setOwnerapprover(users.getById(tmp));
                                }
                            }
                            restaurant.setPrice(Restaurant.Price.valueOf(rs.getString("price")));
                            String site = rs.getString("site");
                            if (rs.wasNull()) {
                                restaurant.setSite(null);
                            } else {
                                restaurant.setSite(site);
                            }
                            String city = rs.getString("city");
                            if (rs.wasNull()) {
                                restaurant.setCity(null);
                            } else {
                                restaurant.setCity(city);
                            }
                            restaurant.setRatesCount(rs.getLong("rates_count"));
                            this.getTypes(restaurant);
                            return restaurant;
                        }
                        else {
                            return null;
                        }
                    }
                }
            }
        }

        /**
         * Add a restaurant to the db
         * @param name restaurant name, must not be null
         * @param descr restaurant descr, must not be null
         * @param site restaurant home page link, can be null
         * @param lon longitude, pairs with lat, -180 &lt;= lon &lt;= 180
         * @param lat latitude, pairs with lon, -90 &lt;= lon &lt;= 90
         * @param state state string or null
         * @param province province string or null
         * @param city city string or null
         * @param price restaurant price
         * @param types list of cook types
         * @param timetable
         * @return newly created restaurant object
         * @throws SQLException
         */
        public Restaurant add(String name, String descr, String site, double lon, double lat, String state, String province, String city, Restaurant.Price price, List<String> types, String timetable) throws SQLException {
            try (Connection con = ds.getConnection()) {
                Restaurant restaurant = new Restaurant();
                //normalize site url
                if(site!= null && !site.equals("") && !(site.startsWith("http://") || site.startsWith("HTTP://") || site.startsWith("https://") || site.startsWith("HTTPS://"))) {
                    site = "http://"+site;
                }
                try (PreparedStatement restaurant_inserter = con.prepareStatement("INSERT INTO restaurants(name, descr, site, lon, lat, state, province, city, price, timetable) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?::price_t, ?::jsonb) RETURNING id")) {
                    restaurant_inserter.setString(1, name);
                    restaurant_inserter.setString(2, descr);
                    if (site != null) {
                        restaurant_inserter.setString(3, site);
                    } else {
                        restaurant_inserter.setNull(3, java.sql.Types.VARCHAR);
                    }
                    restaurant_inserter.setDouble(4, lon);
                    restaurant_inserter.setDouble(5, lat);
                    if (state != null) {
                        restaurant_inserter.setString(6, state);
                    } else {
                        restaurant_inserter.setNull(6, java.sql.Types.VARCHAR);
                    }
                    if (province != null) {
                        restaurant_inserter.setString(7, province);
                    } else {
                        restaurant_inserter.setNull(7, java.sql.Types.VARCHAR);
                    }
                    if (city != null) {
                        restaurant_inserter.setString(8, city);
                    } else {
                        restaurant_inserter.setNull(8, java.sql.Types.VARCHAR);
                    }
                    restaurant_inserter.setString(9, price.toString());
                    restaurant_inserter.setString(10, timetable);
                    try (ResultSet rs = restaurant_inserter.executeQuery()) {
                        rs.next();
                        restaurant.setId(rs.getLong("id"));
                        restaurant.setDescr(descr);
                        restaurant.setLat(lat);
                        restaurant.setLon(lon);
                        restaurant.setOwner(null);
                        restaurant.setOwnerapprover(null);
                    }
                }
                List<String> typesa = new ArrayList<>();

                try (PreparedStatement insertType = con.prepareStatement("INSERT INTO cooks(restaurant_id, type_id) VALUES(?, ?)")) {
                    for(String type:types) {
                        typesa.add(type);
                        insertType.setLong(1, restaurant.getId());
                        insertType.setString(2, type);
                        insertType.executeUpdate();
                    }
                }
                restaurant.setTypes(typesa);

                try (PreparedStatement stm = con.prepareStatement("SELECT rate, classification FROM restaurant_stats WHERE id=?")) {
                    stm.setLong(1, restaurant.getId());
                    try(ResultSet rs = stm.executeQuery()) {
                        if(rs.next()) {
                            restaurant.setClassification(rs.getInt("classification"));
                            restaurant.setRate(rs.getDouble("rate"));
                        }
                    }
                }
                return restaurant;
            }
        }

        /**
         * initialize cook types list for given restaurant
         * @param r
         * @throws SQLException 
         */
        public void getTypes(Restaurant r) throws SQLException {
            try (Connection con = ds.getConnection()) {
                List<String> types = new ArrayList<>();
                try (PreparedStatement types_getter = con.prepareStatement("SELECT type_id FROM cooks WHERE restaurant_id=?")) {
                    types_getter.setLong(1, r.getId());
                    try (ResultSet rs = types_getter.executeQuery()) {
                        while(rs.next()) {
                            types.add(rs.getString("type_id"));
                        }
                    }
                }
                r.setTypes(types);
            }
        }

        /**
         * Get restaurant rate
         * @param r
         * @return [0-5]
         * @throws SQLException 
         */
        public double getRate(Restaurant r) throws SQLException {
            try (Connection con = ds.getConnection()) {
                try (PreparedStatement rate_getter = con.prepareStatement("SELECT rate FROM restaurant_stats WHERE id=?")) {
                    rate_getter.setLong(1, r.getId());
                    try (ResultSet rs = rate_getter.executeQuery()) {
                        rs.next();
                        return rs.getDouble(1);
                    }
                }
            }
        }

        /**
         * get restaurant reviews list
         * @param r
         * @return a valid list
         * @throws SQLException 
         */
        public List<Rate> getRates(Restaurant r) throws SQLException {
            try (Connection con = ds.getConnection()) {
                List<Rate> rates = new ArrayList<>();
                try (PreparedStatement rates_getter = con.prepareStatement("SELECT user_id, day, rate, review, owner_answer, answer_approver, up, down FROM rates JOIN rate_stats USING(user_id, restaurant_id, day) WHERE restaurant_id=? ORDER BY day DESC")) {
                    rates_getter.setLong(1, r.getId());
                    try (ResultSet rs = rates_getter.executeQuery()) {
                        while(rs.next()) {
                            Rate rt = new Rate();
                            rt.setUser(users.getById(rs.getLong("user_id")));
                            rt.setRestaurant(r);
                            rt.setDay(rs.getDate("day"));
                            rt.setRate(rs.getInt("rate"));
                            rt.setReview(rs.getString("review"));
                            rt.setDown(rs.getLong("down"));
                            rt.setUp(rs.getLong("up"));
                            String answer = rs.getString("owner_answer");
                            if(rs.wasNull()) {
                                rt.setOwnerAnswer(null);
                            }
                            else {
                                rt.setOwnerAnswer(answer);
                            }
                            long aa = rs.getLong("answer_approver");
                            if(rs.wasNull()) {
                                rt.setAnswerApprover(null);
                            }
                            else {
                                rt.setAnswerApprover(users.getById(aa));
                            }
                            rates.add(rt);
                        }
                    }
                }
                return rates;
            }
        }

        /**
         * get the list of types for the given restaurant
         * @return a valid list
         * @throws SQLException 
         */
        public List<String> getTypes() throws SQLException {
            try (Connection con = ds.getConnection()) {
                ArrayList<String> ret = new ArrayList<>();
                try (PreparedStatement types_getter = con.prepareStatement("SELECT type FROM types"); ResultSet rs = types_getter.executeQuery()) {
                    while(rs.next()) {
                        ret.add(rs.getString("type"));
                    }
                }
                return ret;
            }
        }

        /**
         * Get the list of pictures for a given restaurant
         * @param r
         * @return a valid list
         * @throws SQLException 
         */
        public  List<Picture> getPictures(Restaurant r) throws SQLException {
            try (Connection con = ds.getConnection()) {
                List<Picture> ret = new ArrayList<>();
                try (PreparedStatement pictureGetter = con.prepareStatement("SELECT id, nvs FROM pictures WHERE restaurant_id=? AND (nvs<>FALSE OR nvs IS NULL)")) {
                    pictureGetter.setLong(1, r.getId());
                    try (ResultSet rs = pictureGetter.executeQuery()) {
                        while(rs.next()) {
                            boolean reportable = false;
                            rs.getBoolean("nvs");
                            if(rs.wasNull()) {
                                reportable = true;
                            }
                            ret.add(new Picture(rs.getLong("id"), reportable));
                        }
                    }
                }
                return ret;
            }
        }

        /**
         * add a picture to the given restaurant
         * @param restaurant_id
         * @param image
         * @throws SQLException 
         */
        public void addPicture(long restaurant_id, File image) throws SQLException {
            try (Connection con = ds.getConnection()) {
                try (PreparedStatement stm = con.prepareStatement("INSERT INTO pictures(restaurant_id, file) VALUES(?, ?)")) {
                    stm.setLong(1, restaurant_id);
                    stm.setString(2, image.getName());
                    stm.executeUpdate();
                }
            }
        }

        /**
         * Search restaurant matching on multiple criteria
         * @param type type of search
         * @param arg dynamic arguments depending on type
         * @param price price range
         * @param typelist slash (or) separated list of comma (and) separeted list of cook types
         * @param name search text for name and description
         * @return valid list of results
         * @throws SQLException 
         */
        public List<Restaurant> search(String type, String arg, String price, String typelist, String name) throws SQLException {
            List<Restaurant> ret = new ArrayList<>();
            try (Connection con = ds.getConnection()) {
                //dynamic query generation
                StringBuilder search = new StringBuilder("SELECT id, name, descr, site, lon, lat, price, owner, ownerapprover, rate, classification, timetable::text, city, rates_count FROM restaurants JOIN restaurant_stats USING (id)");
                boolean where = false;
                if(type!=null) {
                    type = type.substring(1);
                    if(!type.equals("")) {
                        if(!where) {
                            search.append(" WHERE");
                            where = true;
                        }
                        else {
                            search.append(" AND");
                        }
                        switch(type) {
                            case "state":
                                search.append(" state=?");
                                break;
                            case "city":
                                search.append(" city=?");
                                break;
                            case "province":
                                search.append(" province=?");
                                break;
                            case "near":
                                search.append(" (power((lon-?)*111.320*cos(radians(lat)), 2) + power((lat-?)*110.574, 2)) < power(?, 2)");
                                break;
                            case "address":
                                search.append(" (power((lon-?)*111.320*cos(radians(lat)), 2) + power((lat-?)*110.574, 2)) < power(0.05, 2)");
                                break;
                        }
                    }
                }

                if(!"ALL".equals(price)) {
                    if(!where) {
                        search.append(" WHERE");
                        where = true;
                    }
                    else {
                        search.append(" AND");
                    }
                    search.append(" price = ?::price_t");
                }

                String[] or;
                List<String[]> or_and = null;
                if(typelist!=null) {
                    or = typelist.split("/");
                    or_and = new ArrayList<>();
                    for(String term:or) {
                        or_and.add(term.split(","));
                    }
                    for(int i = 0; i < or_and.size(); ++i) {
                        if(i == 0) {
                            if(!where) {
                                search.append(" WHERE");
                                where = true;
                            }
                            else {
                                search.append(" AND");
                            }
                            search.append("(");
                        }
                        else {
                            search.append(" OR");
                        }
                        search.append("(");
                        for(int j = 0; j < or_and.get(i).length; ++j) {
                            if(j > 0) {
                                search.append(" AND");
                            }
                            search.append(" EXISTS (SELECT 1 FROM cooks WHERE restaurant_id=id AND type_id = ?)");
                        }
                        search.append(")");
                    }
                    search.append(")");
                }
                String[] sre = null;
                if(name!=null && !name.equals("")) {
                    sre = Arrays.asList(name.split("[ \t]+")).stream().filter(s -> s.length()>=3).map(s -> "%"+s.replaceAll("%", "\\\\%").replaceAll("_", "\\\\_")+"%").toArray(String[]::new);
                    if(!where) {
                        search.append(" WHERE (FALSE");
                        where = true;
                    }
                    else {
                        search.append(" AND (FALSE");
                    }
                    for(String s:sre) {
                        search.append(" OR name ILIKE ? OR descr ILIKE ?");
                    }
                    search.append(")");
                }

                search.append(" ORDER BY classification DESC, name ASC");

                try(PreparedStatement stm = con.prepareStatement(search.toString())) {
                    //query parameters filling, following the same structure of query construction
                    int a = 1; //current parameter
                    if(type!=null && !type.equals("")) {
                        switch(type) {
                            case "state": case "city": case "province":
                                try {
                                    stm.setString(a, URLDecoder.decode(arg, "UTF-8"));
                                    ++a;
                                } catch (UnsupportedEncodingException ex) {
                                    throw new AssertionError("UTF-8 must be present");
                                }
                                break;
                            case "near": {
                                    String[] args = arg.split(",");
                                    stm.setDouble(a, Double.parseDouble(args[0]));
                                    ++a;
                                    double lat = Double.parseDouble(args[1]);
                                    stm.setDouble(a, lat);
                                    ++a;
                                    double radius = Double.parseDouble(args[2]);
                                    stm.setDouble(a, radius);
                                    ++a;
                                }
                                break;
                            case "address": {
                                    String[] args = arg.split(",");
                                    stm.setDouble(a, Double.parseDouble(args[0]));
                                    ++a;
                                    double lat = Double.parseDouble(args[1]);
                                    stm.setDouble(a, lat);
                                    ++a;
                                }
                                break;
                        }
                    }
                    if(!"ALL".equals(price)) {
                        stm.setString(a, price);
                        ++a;
                    }
                    if(typelist!=null) {
                        for(int i = 0; i < or_and.size(); ++i) {
                            for(int j = 0; j < or_and.get(i).length; ++j) {
                                try {
                                    stm.setString(a, URLDecoder.decode(or_and.get(i)[j], "UTF-8"));
                                } catch (UnsupportedEncodingException ex) {
                                    throw new AssertionError("UTF-8 must be present");
                                }
                                ++a;
                            }
                        }
                    }
                    if(sre != null) {
                        for(String s:sre) {
                            stm.setString(a, s);
                            ++a;
                            stm.setString(a, s);
                            ++a;
                        }
                    }

                    try(ResultSet rs = stm.executeQuery()) {
                        while(rs.next()) {
                            Restaurant restaurant = new Restaurant();
                            restaurant.setName(rs.getString("name"));
                            restaurant.setDescr(rs.getString("descr"));
                            restaurant.setId(rs.getLong("id"));
                            restaurant.setLat(rs.getDouble("lat"));
                            restaurant.setLon(rs.getDouble("lon"));
                            restaurant.setClassification(rs.getInt("classification"));
                            restaurant.setRate(rs.getDouble("rate"));
                            restaurant.setTimetable(rs.getString("timetable"));
                            long tmp = rs.getLong("owner");
                            if(!rs.wasNull()) {
                                restaurant.setOwner(users.getById(tmp));
                                tmp = rs.getLong("ownerapprover");
                                if(!rs.wasNull()) {
                                    restaurant.setOwnerapprover(users.getById(tmp));
                                }
                            }
                            restaurant.setPrice(Restaurant.Price.valueOf(rs.getString("price")));
                            String site = rs.getString("site");
                            if (rs.wasNull()) {
                                restaurant.setSite(null);
                            } else {
                                restaurant.setSite(site);
                            }
                            String city = rs.getString("city");
                            if (rs.wasNull()) {
                                restaurant.setCity(null);
                            } else {
                                restaurant.setCity(city);
                            }
                            restaurant.setRatesCount(rs.getLong("rates_count"));
                            this.getTypes(restaurant);
                            ret.add(restaurant);
                        }
                    }
                }
            }
            return ret;
        }

        /**
         * Run search passing on parameters, then give back a json rappresentation
         * @param type
         * @param arg
         * @param price
         * @param typelist
         * @param name
         * @param bundle
         * @return JSONArray of model.json.Restaurant results
         * @throws SQLException 
         */
        public JSONArray searchJSON(String type, String arg, String price, String typelist, String name, ResourceBundle bundle) throws SQLException {
            JSONArray ret = new JSONArray();
            for(Restaurant r:this.search(type, arg, price, typelist, name)) {
                Long image = null;
                List<Picture> ps = restaurants.getPictures(r);
                if(!ps.isEmpty()) {
                    image = ps.get(0).getId();
                }
                model.json.Restaurant r1 = (model.json.Restaurant)r.toJSON(bundle);
                r1.setImage(image);
                ret.add(r1);
            }
            return ret;
        }

        /**
         * claim a restaurant possession
         * @param restaurant
         * @param user
         * @return t/f success/fail
         * @throws SQLException 
         */
        public boolean claim(long restaurant, User user) throws SQLException {
            try (Connection con = ds.getConnection()) {
                try(PreparedStatement stm = con.prepareStatement("UPDATE restaurants SET owner=?, ownerapprover=NULL WHERE id=? AND owner IS NULL")) {
                    stm.setLong(1, user.getId());
                    stm.setLong(2, restaurant);
                    return stm.executeUpdate()==1;
                }
            }
        }

        /**
         * approve a clain
         * @param restaurant_id
         * @param admin
         * @return t/s success/fail
         * @throws SQLException 
         */
        public boolean acceptClaim(long restaurant_id, User admin) throws SQLException {
            try (Connection con = ds.getConnection()) {
                if(admin.isAdmin()) {
                    try(PreparedStatement stm = con.prepareStatement("UPDATE restaurants SET ownerapprover=? WHERE id=? AND owner IS NOT NULL AND ownerapprover IS NULL")) {
                        stm.setLong(1, admin.getId());
                        stm.setLong(2, restaurant_id);
                        return stm.executeUpdate()==1;
                    }
                } else {
                    return false;
                }
            }
        }

        /**
         * refuse a claim
         * @param restaurant_id
         * @param admin
         * @return t/f success/fail
         * @throws SQLException 
         */
        public boolean refuseClaim(long restaurant_id, User admin) throws SQLException {
            try (Connection con = ds.getConnection()) {
                if(admin.isAdmin()) {
                    try(PreparedStatement stm = con.prepareStatement("UPDATE restaurants SET ownerapprover=NULL, owner=NULL WHERE id=? AND owner IS NOT NULL AND ownerapprover IS NULL")) {
                        stm.setLong(1, restaurant_id);
                        return stm.executeUpdate()==1;
                    }
                } else {
                    return false;
                }
            }
        }

        /**
         * modify all restaurant fields
         * @param owner
         * @param id
         * @param name
         * @param descr
         * @param site
         * @param price
         * @param types
         * @param timetable
         * @throws SQLException 
         */
        public void modify(User owner, Long id, String name, String descr, String site, Restaurant.Price price, List<String> types, String timetable) throws SQLException {
            try (Connection con = ds.getConnection()) {
                con.setAutoCommit(false);
                try {
                    if(site!= null && !site.equals("") && !(site.startsWith("http://") || site.startsWith("HTTP://") || site.startsWith("https://") || site.startsWith("HTTPS://"))) {
                        site = "http://"+site;
                    }
                    try (PreparedStatement restaurant_updater = con.prepareStatement("UPDATE restaurants SET name=?, descr=?, site=?, price=?::price_t, timetable=?::jsonb WHERE id=? AND owner=?")) {
                        restaurant_updater.setString(1, name);
                        restaurant_updater.setString(2, descr);
                        if (site != null) {
                            restaurant_updater.setString(3, site);
                        } else {
                            restaurant_updater.setNull(3, java.sql.Types.VARCHAR);
                        }
                        restaurant_updater.setString(4, price.toString());
                        restaurant_updater.setString(5, timetable);
                        restaurant_updater.setLong(6, id);
                        restaurant_updater.setLong(7, owner.getId());
                        if(restaurant_updater.executeUpdate()==0) return;
                    }

                    if(types != null) {
                        try (PreparedStatement deleteTypes = con.prepareStatement("DELETE FROM cooks WHERE restaurant_id=?")) {
                            deleteTypes.setLong(1, id);
                        }

                        try (PreparedStatement insertType = con.prepareStatement("INSERT INTO cooks(restaurant_id, type_id) VALUES(?, ?)")) {
                            for(String type:types) {
                                insertType.setLong(1, id);
                                insertType.setString(2, type);
                                insertType.executeUpdate();
                            }
                        }
                    }
                    con.commit();
                } finally {
                    con.rollback();
                    con.setAutoCommit(true);
                }
            }
        }
    }

    public final class PictureManager {

        /**
         * get filename from id
         * @param id
         * @return filename or null if not found
         * @throws SQLException 
         */
        public String getFilenameById(long id) throws SQLException {
            try (Connection con = ds.getConnection()) {
                String ret = null;
                try (PreparedStatement pictureGetter = con.prepareStatement("SELECT file FROM pictures WHERE id=?")) {
                    pictureGetter.setLong(1, id);
                    try (ResultSet rs = pictureGetter.executeQuery()) {
                        if(rs.next()) {
                            ret = rs.getString("file");
                        }
                    }
                }
                return ret;
            }
        }

        /**
         * Get model.Picture object from id
         * @param id
         * @return picture object or null if not found
         * @throws SQLException 
         */
        public Picture getById(long id) throws SQLException {
            try (Connection con = ds.getConnection()) {
                Picture ret = null;
                try (PreparedStatement stm = con.prepareStatement("SELECT restaurant_id, nvs, modified FROM pictures WHERE id=?")) {
                    stm.setLong(1, id);
                    try (ResultSet rs = stm.executeQuery()) {
                        if(rs.next()) {
                            ret = new Picture();
                            ret.setId(id);
                            ret.setModified(rs.getTimestamp("modified"));
                            ret.setRestaurant(restaurants.getById(rs.getLong("restaurant_id")));
                            rs.getBoolean("nvs");
                            ret.setReportable(rs.wasNull());
                        }
                    }
                }
                return ret;
            }
        }

        /**
         * Set report state of a given image, the reporter must be the owner of the restaurant
         * @param picture
         * @param user
         * @param action t/f accept/report
         * @return t/f success/fail
         * @throws SQLException 
         */
        public boolean report(long picture, User user, boolean action) throws SQLException {
            try (Connection con = ds.getConnection()) {
                try(PreparedStatement stm = con.prepareStatement("UPDATE pictures SET nvs=? WHERE id=? AND (SELECT owner FROM restaurants WHERE restaurants.id=restaurant_id)=?")) {
                    stm.setBoolean(1, action);
                    stm.setLong(2, picture);
                    stm.setLong(3, user.getId());
                    return stm.executeUpdate()==1;
                }
            }
        }

        /**
         * Accept or delete a reported picture
         * @param id
         * @param admin
         * @param keep
         * @return t/f success/fail
         * @throws SQLException 
         */
        public boolean reported(long id, User admin, boolean keep) throws SQLException {
            try (Connection con = ds.getConnection()) {
                if(admin.isAdmin()) {
                    if(!keep) {
                        int affected;
                        String file = pictures.getFilenameById(id);
                        try(PreparedStatement stm = con.prepareStatement("DELETE FROM pictures WHERE id=?")) {
                            stm.setLong(1, id);
                            affected = stm.executeUpdate();
                        }
                        new File("/WEB-INF/pictures", file).delete();
                        return affected == 1;
                    } else {
                        try(PreparedStatement stm = con.prepareStatement("UPDATE pictures SET nvs=TRUE WHERE id=?")) {
                            stm.setLong(1, id);
                            return stm.executeUpdate() == 1;
                        }
                    }
                }
                else {
                    return false;
                }
            }
        }
    }

    public class RateManager {
        /**
         * up-vote or down-vote a given review 
         * @param user_id
         * @param r_user_id
         * @param r_restaurant_id
         * @param r_day
         * @param rate t/f up/down
         * @throws SQLException 
         */
        public void rate(long user_id, long r_user_id, long r_restaurant_id, Date r_day, boolean rate) throws SQLException {
            try (Connection con = ds.getConnection()) {
                con.setAutoCommit(false);
                try (PreparedStatement update = con.prepareStatement("UPDATE rates_rates SET rate=? WHERE user_id = ? AND r_user_id = ? AND r_restaurant_id = ? AND r_day = ?")) {
                    update.setBoolean(1, rate);
                    update.setLong(2, user_id);
                    update.setLong(3, r_user_id);
                    update.setLong(4, r_restaurant_id);
                    update.setDate(5, new java.sql.Date(r_day.getTime()));
                    update.executeUpdate();
                    try (PreparedStatement insert = con.prepareStatement("INSERT INTO rates_rates(rate, user_id, r_user_id, r_restaurant_id, r_day) " +
                                                                         "SELECT ?, ?, ?, ?, ? WHERE NOT EXISTS" +
                                                                         "(SELECT 1 FROM rates_rates WHERE user_id = ? AND r_user_id = ? AND r_restaurant_id = ? AND r_day = ?)")) {
                        insert.setBoolean(1, rate);
                        insert.setLong(2, user_id);
                        insert.setLong(3, r_user_id);
                        insert.setLong(4, r_restaurant_id);
                        insert.setDate(5, new java.sql.Date(r_day.getTime()));
                        insert.setLong(6, user_id);
                        insert.setLong(7, r_user_id);
                        insert.setLong(8, r_restaurant_id);
                        insert.setDate(9, new java.sql.Date(r_day.getTime()));
                        insert.executeUpdate();
                    }
                } catch(SQLException ex) {
                    con.rollback();
                    throw ex;
                } finally {
                    con.setAutoCommit(true);
                }
            }
        }

        /**
         * add a review
         * @param user_id
         * @param restaurant_id
         * @param rate
         * @param review
         * @throws SQLException 
         */
        public void add(long user_id, long restaurant_id, int rate, String review) throws SQLException {
            try (Connection con = ds.getConnection()) {
                try(PreparedStatement stm = con.prepareStatement("INSERT INTO rates(user_id, restaurant_id, day, rate, review) VALUES(?, ?, current_date, ?, ?)")) {
                    stm.setLong(1, user_id);
                    stm.setLong(2, restaurant_id);
                    stm.setInt(3, rate);
                    stm.setString(4, review);
                    stm.executeUpdate();
                }
            }
        }

        /**
         * answer the given review, must be the owner
         * @param user
         * @param user_id
         * @param restaurant_id
         * @param day
         * @param answer
         * @return
         * @throws SQLException 
         */
        public boolean answer(User user, long user_id, long restaurant_id, Date day, String answer) throws SQLException {
            try (Connection con = ds.getConnection()) {
                if(user.equals(restaurants.getById(restaurant_id).getOwner())) {
                    try(PreparedStatement stm = con.prepareStatement("UPDATE rates SET owner_answer=?, answer_approver=NULL WHERE user_id=? AND restaurant_id=? AND day=?")) {
                        stm.setString(1, answer);
                        stm.setLong(2, user_id);
                        stm.setLong(3, restaurant_id);
                        stm.setDate(4, new java.sql.Date(day.getTime()));
                        return stm.executeUpdate()==1;
                    }
                } else {
                    return false;
                }
            }
        }

        /**
         * get a review by id
         * @param user_id
         * @param restaurant_id
         * @param day
         * @return object or null if not found
         * @throws SQLException 
         */
        public Rate getById(long user_id, long restaurant_id, Date day) throws SQLException {
            try (Connection con = ds.getConnection()) {
                try (PreparedStatement rates_getter = con.prepareStatement("SELECT rate, review, owner_answer, answer_approver, up, down FROM rates JOIN rate_stats USING(user_id, restaurant_id, day) WHERE user_id=? AND restaurant_id=? AND day=?")) {
                    rates_getter.setLong(1, user_id);
                    rates_getter.setLong(2, restaurant_id);
                    day = new java.sql.Date(day.getTime());
                    rates_getter.setDate(3, (java.sql.Date)day);
                    try (ResultSet rs = rates_getter.executeQuery()) {
                        if(rs.next()) {
                            Rate rt = new Rate();
                            rt.setUser(users.getById(user_id));
                            rt.setRestaurant(restaurants.getById(restaurant_id));
                            rt.setDay(day);
                            rt.setRate(rs.getInt("rate"));
                            rt.setReview(rs.getString("review"));
                            rt.setDown(rs.getLong("down"));
                            rt.setUp(rs.getLong("up"));
                            String answer = rs.getString("owner_answer");
                            if(rs.wasNull()) {
                                rt.setOwnerAnswer(null);
                            }
                            else {
                                rt.setOwnerAnswer(answer);
                            }
                            long aa = rs.getLong("answer_approver");
                            if(rs.wasNull()) {
                                rt.setAnswerApprover(null);
                            }
                            else {
                                rt.setAnswerApprover(users.getById(aa));
                            }
                            return rt;
                        }
                        else {
                            return null;
                        }
                    }
                }
            }
        }

        /**
         * accept the answer to a review
         * @param admin
         * @param user_id
         * @param restaurant_id
         * @param day
         * @return t/f success/fail
         * @throws SQLException 
         */
        public boolean acceptAnswer(User admin, long user_id, long restaurant_id, Date day) throws SQLException {
            try (Connection con = ds.getConnection()) {
                if(admin.isAdmin()) {
                    try(PreparedStatement stm = con.prepareStatement("UPDATE rates SET answer_approver=? WHERE user_id=? AND restaurant_id=? AND day=? AND owner_answer IS NOT NULL AND answer_approver IS NULL")) {
                        stm.setLong(1, admin.getId());
                        stm.setLong(2, user_id);
                        stm.setLong(3, restaurant_id);
                        stm.setDate(4, new java.sql.Date(day.getTime()));
                        return stm.executeUpdate() == 1;
                    }
                }
                else {
                    return false;
                }
            }
        }

        /**
         * refuse the answer to a review
         * @param admin
         * @param user_id
         * @param restaurant_id
         * @param day
         * @return t/f success/fail
         * @throws SQLException 
         */
        public boolean refuseAnswer(User admin, long user_id, long restaurant_id, Date day) throws SQLException {
            try (Connection con = ds.getConnection()) {
                if(admin.isAdmin()) {
                    try(PreparedStatement stm = con.prepareStatement("UPDATE rates SET owner_answer=NULL WHERE user_id=? AND restaurant_id=? AND day=? AND owner_answer IS NOT NULL AND answer_approver IS NULL")) {
                        stm.setLong(1, user_id);
                        stm.setLong(2, restaurant_id);
                        stm.setDate(3, new java.sql.Date(day.getTime()));
                        return stm.executeUpdate() == 1;
                    }
                }
                else {
                    return false;
                }
            }
        }
    }
}
