package exceptions;

import javax.servlet.ServletException;

/**
 * Duplicate key on db
 */
public class KeyDuplication extends ServletException {
    public KeyDuplication(String msg) {
        super(msg);
    }
}
