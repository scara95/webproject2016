package exceptions;

import javax.servlet.ServletException;

/**
 * Unauthorized access to some resource
 */
public class AccessForbidden extends ServletException {
    public AccessForbidden() {
        super();
    }
    
    public AccessForbidden(String msg) {
        super(msg);
    }
    
    public AccessForbidden(Exception ex) {
        super(ex);
    }
}
