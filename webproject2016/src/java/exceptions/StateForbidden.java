package exceptions;

import javax.servlet.ServletException;

/**
 * Inconsistent state of data
 */
public class StateForbidden extends ServletException {
    public StateForbidden() {
        super();
    }
    
    public StateForbidden(String msg) {
        super(msg);
    }
    
    public StateForbidden(Exception ex) {
        super(ex);
    }
}
