package model;

import java.io.Serializable;
import org.json.simple.JSONObject;

public class User implements Serializable {
    private long id;
    private String email;
    private String name;
    private String surname;
    private String avatar;
    private boolean isAdmin;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }
    
    public JSONObject toJSON() {
        model.json.User ret = new model.json.User();
        ret.setAdmin(isAdmin);
        ret.setId(id);
        ret.setName(name);
        ret.setSurname(surname);
        return ret;
    }

    @Override
    public int hashCode() {
        return (int)id;
    }

    @Override
    public boolean equals(Object user) {
        if(user == null) {
            return false;
        }
        if(user instanceof User) {
            return this.id == ((User) user).id;
        }
        else {
            return false;
        }
    }
}
