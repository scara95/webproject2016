package model.json;

import java.sql.Timestamp;
import java.util.List;
import model.User;
import org.json.simple.parser.JSONParser;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

public class Restaurant extends JSONObject {

    public void setClassification(Integer classification) {
        this.put("classification", classification);
    }

    public void setRate(Double rate) {
        this.put("rate", rate);
    }

    public void setModified(Timestamp modified) {
        //do not let internal rappresentation leak
        throw new AssertionError("Should not be called: internal rappresentation should not leak");
    }

    public void setName(String name) {
        this.put("name", name);
    }
    
    public void setCity(String city) {
        if(city != null && !city.equals("")) {
            this.put("city", city);
        }
    }

    public void setTypes(List<String> types) {
        this.put("types", types);
    }


    public void setId(long id) {
        this.put("id", id);
    }

    public void setDescr(String descr) {
        this.put("descr", descr);
    }

    public void setSite(String site) {
        if(site != null && !site.equals("")) {
            this.put("site", site);
        }
    }

    public void setLon(double lon) {
        this.put("lon", lon);
    }

    public void setLat(double lat) {
        this.put("lat", lat);
    }

    public void setPrice(model.Restaurant.Price price) {
        this.put("price", price.toString());
    }
    
    public void setTimetable(String timetable) {
        try {
            this.put("timetable", new JSONParser().parse(timetable));
        } catch (ParseException ex) {
            //for construction values stored in postgresql must be json formatted
            throw new AssertionError("Must be JSON");
        }
    }

    public void setOwner(User owner) {
        if(owner!=null) {
            this.put("owner", owner.toJSON());
        }
    }
    
    public void setImage(Long id) {
        if(id!=null) {
            this.put("image", id);
        }
    }

    public void setOwnerapprover(User ownerapprover) {
        if(ownerapprover!=null) {
            this.put("ownerapprover", ownerapprover.toJSON());
        }
    }
    
    public void setRatesCount(long rates_count) {
        this.put("ratesCount", rates_count);
    }
}
