package model.json.notification;

import java.util.ResourceBundle;
import model.Picture;
import org.json.simple.JSONObject;

public class NewImageNotification extends Notification {
    public NewImageNotification(ResourceBundle bundle, Picture p) {
        super(
            bundle.getString("titleNotificationNewImage"),
            new StringBuilder(bundle.getString("msgNotificationNewImageUploaded")).append(" ").append(p.getRestaurant().getName()).append(" ").append(bundle.getString("msgNotificationNewImageUploaded2")).toString(),
            p.getModified(),

            new Control(Control.Type.REDIRECT, bundle.getString("btnNotificationView"), true) {

                @Override
                public String getRedirectURL() {
                    return "/restaurant/"+p.getRestaurant().getId();
                }

                @Override
                public JSONObject getRequestParameter() {
                    JSONObject ret = new JSONObject();
                    return ret;
                }
            }
        );
    }
}
