package model.json.notification;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Represent a notification with a message and some controls
 */
public class Notification extends JSONObject implements Comparable {
    private final List<Control> controls;
    private final String message;
    private final String name;
    private final Timestamp modified;
    
    /**
     * Constructor
     * @param name name of notification
     * @param message message
     * @param modified last modification, implies ordering
     * @param controls list of controls
     */
    public Notification(String name, String message, Timestamp modified, List<Control> controls) {
        this.name = name;
        this.controls = controls;
        this.message = message;
        this.modified = modified;
        this.put("name", name);
        this.put("message", message);
        JSONArray cs = new JSONArray();
        cs.addAll(controls);
        this.put("controls", cs);
    }
    
    public Notification(String name, String message, Timestamp modified, Control... controls) {
        this(name, message, modified, Arrays.asList(controls));
    }

    public List<Control> getControls() {
        return controls;
    }

    public String getMessage() {
        return message;
    }

    public String getName() {
        return name;
    }

    @Override
    public int compareTo(Object o) {
        if(o instanceof Notification) {
            return modified.compareTo(((Notification)o).modified);
        }
        else {
            throw new IllegalArgumentException("Must be an istance of: " + Notification.class.toString());
        }
    }
    
}
