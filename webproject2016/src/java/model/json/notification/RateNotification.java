package model.json.notification;

import java.util.ResourceBundle;
import model.Rate;
import org.json.simple.JSONObject;

public class RateNotification extends Notification {
    public RateNotification(ResourceBundle bundle, Rate r) {
        super(
            bundle.getString("titleNotificationReview"),
            new StringBuilder(bundle.getString("msgNotificationTheUser")).append(" ").append(r.getUser().getName()).append(" ").append(r.getUser().getSurname()).append(" ").append(bundle.getString("msgNotificationWroteAReviewFor")).append(" ").append(r.getRestaurant().getName()).toString(),
            r.getModified(),

            new Control(Control.Type.REDIRECT, bundle.getString("btnNotificationView"), true) {

                @Override
                public String getRedirectURL() {
                    return "/restaurant/"+r.getRestaurant().getId();
                }

                @Override
                public JSONObject getRequestParameter() {
                    JSONObject ret = new JSONObject();
                    return ret;
                }
            }
        );
    }
}
