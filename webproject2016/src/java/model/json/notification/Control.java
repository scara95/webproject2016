package model.json.notification;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Represents a notification control, can be of 2 types AJAX or REDIRECT based
 * on behavior
 * 
 */
public abstract class Control extends JSONObject {
    public static enum Type {AJAX, REDIRECT};
    final private Type type;
    final private String name;
    final private boolean primary;
    
    /**
     * Create a control of given type
     * @param t control type
     * @param name control name
     * @param primary color to be showed
     */
    protected Control(Type t, String name, Boolean primary) {
        type = t;
        this.name = name;
        this.primary = primary;
        this.put("name", name);
        this.put("primary", primary);
        this.put("parameters", this.getRequestParameter());
        switch(this.type) {
            case AJAX:
                this.put("type", "AJAX");
                this.put("URL", this.getRequestURL());
                this.put("AJAXMessages", this.getAjax());
                break;
            case REDIRECT:
                this.put("type", "REDIRECT");
                this.put("URL", this.getRedirectURL());
                break;
        }
    }
    
    public String getName() {
        return this.name;
    }
    
    public Type getType() {
        return type;
    }
    
    public boolean isAjax() {
        return type == Type.AJAX;
    }
    
    public boolean isRedirect() {
        return type == Type.REDIRECT;
    }
    
    /**
     * get ajax configuration list
     * @return 
     */
    public JSONArray getAjax() {
        if(!isAjax()) {
            throw new AssertionError("Must be AJAX");
        }
        return null;
    }
    
    /**
     * get ajax url request
     * @return 
     */
    public String getRequestURL() {
        if(!isAjax()) {
            throw new AssertionError("Must be AJAX");
        }
        return null;
    }
    
    /**
     * get request parameter
     * @return 
     */
    public abstract JSONObject getRequestParameter();
    
    /**
     * get redirect url request
     * @return 
     */
    public String getRedirectURL() {
        if(!isRedirect()) {
            throw new AssertionError("Must be REDIRECT");
        }
        return null;
    }

    public boolean isPrimary() {
        return primary;
    }
    

    /**
     * represents a message to be given on a given state and if the answer
     * is supposed to be succeful
     */
    public static class AjaxMessage extends JSONObject {
        
        public AjaxMessage(Integer status, String message, Boolean successful) {
            this.put("message", message);
            this.put("status", status);
            this.put("successful", successful);
        }
    }
}
