package model.json.notification;

import java.util.ResourceBundle;
import model.Rate;
import model.json.notification.Control.Type;
import org.json.simple.JSONObject;

public class AnswerNotification extends Notification {

    public AnswerNotification(ResourceBundle bundle, Rate r) {
        super(
            bundle.getString("titleNotificationAnswer"),
            new StringBuilder(bundle.getString("msgNotificationTheOwnerOf")).append(" ").append(r.getRestaurant().getName()).append(" ").append(bundle.getString("msgNotificationAnsweredAReview")).toString(),
            r.getModified(),

            new Control(Type.REDIRECT, bundle.getString("btnNotificationView"), true) {

                @Override
                public String getRedirectURL() {
                    return "/accept_rate_answer";
                }

                @Override
                public JSONObject getRequestParameter() {
                    JSONObject ret = new JSONObject();
                    ret.put("user_id", r.getUser().getId());
                    ret.put("restaurant_id", r.getRestaurant().getId());
                    ret.put("day", r.getDay().toString());
                    return ret;
                }
            }
        );
    }
}
