package model.json.notification;

import java.util.ResourceBundle;
import model.Picture;
import org.json.simple.JSONObject;

public class ImageReportedNotification extends Notification {
    public ImageReportedNotification(ResourceBundle bundle, Picture p) {
        super(
            bundle.getString("titleNotificationReportedImage"),
            new StringBuilder(bundle.getString("msgNotificationTheOwnerOf")).append(" ").append(p.getRestaurant().getName()).append(" ").append(bundle.getString("msgNotificationReportedAnImage")).toString(),
            p.getModified(),

            new Control(Control.Type.REDIRECT, bundle.getString("btnNotificationView"), true) {

                @Override
                public String getRedirectURL() {
                    return "/show_reported_image";
                }

                @Override
                public JSONObject getRequestParameter() {
                    JSONObject ret = new JSONObject();
                    ret.put("id", p.getId());
                    return ret;
                }
            }
        );
    }
}
