package model.json.notification;

import java.util.Arrays;
import java.util.ResourceBundle;
import model.Restaurant;
import model.json.notification.Control.Type;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class ClaimNotification extends Notification {

    public ClaimNotification(ResourceBundle bundle, Restaurant r) {
        super(
            bundle.getString("titleNotificationClaim"),
            new StringBuilder(bundle.getString("msgNotificationTheUser")).append(" ").append(r.getOwner().getName()).append(" ").append(r.getOwner().getSurname()).append(" ").append(bundle.getString("msgNotificationClaimedRestaurant")).append(" ").append(r.getName()).toString(),
            r.getModified(),

            new Control(Type.AJAX, bundle.getString("btnNotificationClaimRefuse"), false) {
                @Override
                public JSONArray getAjax() {
                    JSONArray ret = new JSONArray();
                    ret.addAll(Arrays.asList(
                        new Control.AjaxMessage(200, bundle.getString("msgNotificationClaimRefused"), true),
                        new Control.AjaxMessage(403, bundle.getString("msgNotificationClaimNotRefused"), false),
                        new Control.AjaxMessage(500, bundle.getString("msgNotificationGenericError"), false)
                    ));
                    return ret;
                }

                @Override
                public String getRequestURL() {
                    return "/refuse_claim";
                }

                @Override
                public JSONObject getRequestParameter() {
                    JSONObject ret = new JSONObject();
                    ret.put("id", r.getId());
                    return ret;
                }
            },
            new Control(Type.AJAX, bundle.getString("btnNotificationClaimAccept"), true) {
                @Override
                public JSONArray getAjax() {
                    JSONArray ret = new JSONArray();
                    ret.addAll(Arrays.asList(
                        new AjaxMessage(200, bundle.getString("msgNotificationClaimAccepted"), true),
                        new AjaxMessage(403, bundle.getString("msgNotificationClaimNotAccepted"), false),
                        new AjaxMessage(500, bundle.getString("msgNotificationGenericError"), false)
                    ));
                    return ret;
                }

                @Override
                public String getRequestURL() {
                    return "/accept_claim";
                }

                @Override
                public JSONObject getRequestParameter() {
                    JSONObject ret = new JSONObject();
                    ret.put("id", r.getId());
                    return ret;
                }
            }
        );
    }
}
