package model.json;

import org.json.simple.JSONObject;

public class User extends JSONObject {

    public void setId(long id) {
        this.put("id", id);
    }

    public void setEmail(String email) {
        //do not let personal information leak
        throw new AssertionError("Should not be called: personal informations should not leak");
    }

    public void setName(String name) {
        this.put("name", name);
    }

    public void setSurname(String surname) {
        this.put("surname", surname);
    }

    public void setAvatar(String avatar) {
        //do not let internal rappresentation leak
        throw new AssertionError("Should not be called: internal rappresentation should not leak");
    }

    public void setAdmin(boolean isAdmin) {
        this.put("isAdmin", isAdmin);
    }
}
