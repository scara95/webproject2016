package model;

import java.sql.Timestamp;

public class Picture {
    private long id;
    private boolean reportable;
    private Restaurant restaurant;
    private Timestamp modified;

    public Picture() {

    }

    public Picture(long id, boolean reportable) {
        this.id = id;
        this.reportable = reportable;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isReportable() {
        return reportable;
    }

    public void setReportable(boolean reportable) {
        this.reportable = reportable;
    }

    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

}
