package model;

import java.sql.Timestamp;
import java.util.Date;

public class Rate {
    User user;
    Restaurant restaurant;
    Date day;
    int rate;
    String review;
    String ownerAnsewr;
    User answerApprover;
    long up;
    long down;
    Timestamp modified;

    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }


    public long getUp() {
        return up;
    }

    public void setUp(long up) {
        this.up = up;
    }

    public long getDown() {
        return down;
    }

    public void setDown(long down) {
        this.down = down;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getOwnerAnswer() {
        return ownerAnsewr;
    }

    public void setOwnerAnswer(String ownerAnsewr) {
        this.ownerAnsewr = ownerAnsewr;
    }

    public User getAnswerApprover() {
        return answerApprover;
    }

    public void setAnswerApprover(User answerApprover) {
        this.answerApprover = answerApprover;
    }

}
