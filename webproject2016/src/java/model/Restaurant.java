package model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import org.json.simple.JSONObject;

public class Restaurant implements Serializable {
    public enum Price {LOW, MID, HIGH};

    long id;
    String name;
    String descr;
    String site;
    String city;
    double lon;
    double lat;
    Price price;
    String timetable;
    User owner;
    User ownerapprover;
    List<String> types;
    Timestamp modified;
    Integer classification;
    Double rate;
    long ratesCount;

    public long getRatesCount() {
        return ratesCount;
    }

    public void setRatesCount(long rates_count) {
        this.ratesCount = rates_count;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTimetable() {
        return timetable;
    }

    public void setTimetable(String timetable) {
        this.timetable = timetable;
    }

    public Integer getClassification() {
        return classification;
    }

    public void setClassification(Integer classification) {
        this.classification = classification;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public User getOwnerapprover() {
        return ownerapprover;
    }

    public void setOwnerapprover(User ownerapprover) {
        this.ownerapprover = ownerapprover;
    }
    
    public JSONObject toJSON(ResourceBundle bundle) {
        model.json.Restaurant ret = new model.json.Restaurant();
        ret.setClassification(classification);
        ret.setDescr(descr);
        ret.setId(id);
        ret.setLat(lat);
        ret.setLon(lon);
        ret.setName(name);
        ret.setOwner(owner);
        ret.setOwnerapprover(owner);
        ret.setPrice(price);
        ret.setRate(rate);
        ret.setSite(site);
        List<String> types_ret = new ArrayList<>();
        for(String t:types) {
            types_ret.add(bundle.getString(t));
        }
        ret.setTypes(types_ret);
        ret.setTimetable(timetable);
        ret.setCity(city);
        ret.setRatesCount(ratesCount);
        return ret;
    }
}
