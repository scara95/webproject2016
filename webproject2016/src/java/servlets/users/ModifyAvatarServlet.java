package servlets.users;

import dbmanager.DBManager;
import exceptions.StateForbidden;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.User;
import support.parametercheckdispatch.FormatException;
import support.parametercheckdispatch.ParameterCheckDispatch;
import support.parametercheckdispatch.parametergetter.multipart.DefaultExtensionChangeFileRenamePolicy;
import support.parametercheckdispatch.parametergetter.multipart.MultipartJPGConverterParameterGetterFarm;
import support.utility.StateUtility;

/**
 * Change avatar for the current user, go back to previous page
 */
public class ModifyAvatarServlet extends HttpServlet {
    DBManager dbmanager;
    ParameterCheckDispatch pcd;
    
    @Override
    public void init() throws ServletException {
        dbmanager = (DBManager) super.getServletContext().getAttribute("dbmanager");
        List<String> mustNotBeNull = Arrays.asList("inputFile");
        List<String> canBeNull = Arrays.asList();
        Map<String, ParameterCheckDispatch.ParamTypes> typesMapping = new HashMap<>();
        typesMapping.put("inputFile", ParameterCheckDispatch.ParamTypes.FILE);
        pcd = new ParameterCheckDispatch(mustNotBeNull, canBeNull, typesMapping, new MultipartJPGConverterParameterGetterFarm(getServletContext().getRealPath("/WEB-INF/avatars"), new DefaultExtensionChangeFileRenamePolicy("jpg"))) {
            @Override
            protected void mustNotBeNullSomeNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                mustNotBeNullAllNull(parms, request, response);
            }

            @Override
            protected void mustNotBeNullAllNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                throw new StateForbidden();
            }

            @Override
            protected void mustNotBeNullAllSet(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                User user = StateUtility.getUser(request);
                File avatar = (File)parms.get("inputFile");
                try {
                    dbmanager.users.modifyAvatar(user, avatar.getName());
                    user.setAvatar(avatar.getName());
                } catch (SQLException ex) {
                    throw new ServletException(ex);
                }
                //funziona, ma no, ci penso dopo
                if(user.getAvatar()!=null) {
                    File old = new File(getServletContext().getRealPath("/WEB-INF/avatars/"+user.getAvatar()));
                    old.delete();
                }
                response.sendRedirect(StateUtility.untrack(request));
            }
            
        };
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            try {
                StateUtility.contextInit(dbmanager, request);
            } catch (SQLException ex) {
                throw new ServletException(ex);
            }
            pcd.check(request, response);
        } catch (FormatException ex) {
            throw new StateForbidden();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
