package servlets.registration;

import dbmanager.DBManager;
import exceptions.StateForbidden;
import java.io.IOException;
import java.sql.SQLException;
import java.util.UUID;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.User;
import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import support.parametercheckdispatch.FormatException;
import support.parametercheckdispatch.parametergetter.multipart.MultipartJPGConverterParameterGetterFarm;
import support.parametercheckdispatch.parametergetter.multipart.DefaultExtensionChangeFileRenamePolicy;
import support.parametercheckdispatch.ParameterCheckDispatch;
import support.utility.StateUtility;

/**
 * Complete the registration process gathering data for profile.
 * Returns to previous page after completion.
 */
public class CompleteRegistrationServlet extends HttpServlet {

    DBManager dbmanager;
    ParameterCheckDispatch pcd;

    @Override
    public void init() throws ServletException {
        dbmanager = (DBManager) super.getServletContext().getAttribute("dbmanager");
        List<String> mustNotBeNull = Arrays.asList("user", "name", "surname");
        List<String> canBeNull = Arrays.asList("avatar");
        Map<String, ParameterCheckDispatch.ParamTypes> typesMapping = new HashMap<>();
        typesMapping.put("user", ParameterCheckDispatch.ParamTypes.STRING);
        typesMapping.put("name", ParameterCheckDispatch.ParamTypes.STRING);
        typesMapping.put("surname", ParameterCheckDispatch.ParamTypes.STRING);
        typesMapping.put("avatar", ParameterCheckDispatch.ParamTypes.FILE);
        pcd = new ParameterCheckDispatch(mustNotBeNull, canBeNull, typesMapping, new MultipartJPGConverterParameterGetterFarm(getServletContext().getRealPath("/WEB-INF/avatars"), new DefaultExtensionChangeFileRenamePolicy("jpg"))) {
            @Override
            protected void mustNotBeNullSomeNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                mustNotBeNullAllNull(parms, request, response);
            }

            @Override
            protected void mustNotBeNullAllNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                throw new StateForbidden();
            }

            @Override
            protected void mustNotBeNullAllSet(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                UUID uuid = UUID.fromString((String)parms.get("user"));
                String name = (String)parms.get("name");
                String surname = (String)parms.get("surname");
                File avatar = (File)parms.get("avatar");
                
                User user;
                try {
                    user = dbmanager.users.completeRegistration(uuid, name, surname, avatar);
                } catch (SQLException ex) {
                    throw new ServletException(ex);
                }
                if (user == null) {
                    throw new StateForbidden();
                } else {
                    HttpSession session = request.getSession();
                    session.setAttribute("user", user);
                    request.setAttribute("header", StateUtility.getBundle(request).getString("titleWelcome"));
                    request.setAttribute("message", StateUtility.getBundle(request).getString("msgCompletedRegistration"));
                    RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/success.jsp");
                    rd.forward(request, response);
                }
            }
            
        };
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            try {
                StateUtility.contextInit(dbmanager, request);
            } catch (SQLException ex) {
                throw new ServletException(ex);
            }
            pcd.check(request, response);
        } catch (FormatException ex) {
            throw new StateForbidden();
        }
        
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
