package servlets.registration;

import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ResourceBundle;
import java.util.UUID;
import support.mailing.GMailMailSender;

/**
 * Send email for verification
 */
public class VerificationMailSender extends GMailMailSender {

    public VerificationMailSender(String email, String password) {
        super(email, password);
    }

    @Override
    protected void compose(ResourceBundle bundle, PrintWriter subject, PrintWriter body, Object data) {
        Data d = (Data) data;
        subject.print(bundle.getString("titleConfirmRegistration") + " " + bundle.getString("siteName"));
        try {
            body.println(bundle.getString("msgConfirmRegistration") + " " + d.getPath() + "/verify?uuid=" + URLEncoder.encode(d.user.toString(), StandardCharsets.UTF_8.name()));
            body.println(bundle.getString("msgConfirmRegistration2"));
        } catch (UnsupportedEncodingException ex) {
            //!!panic, condizione impossibile
        }

    }

    public static class Data {

        final private String path;
        final private UUID user;

        public Data(String path, UUID user) {
            this.path = path;
            this.user = user;
        }

        public String getPath() {
            return path;
        }

        public UUID getUser() {
            return user;
        }
    }
}
