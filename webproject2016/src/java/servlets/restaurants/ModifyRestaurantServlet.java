package servlets.restaurants;

import dbmanager.DBManager;
import exceptions.StateForbidden;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Restaurant;
import org.json.simple.JSONArray;
import org.json.simple.JSONValue;
import support.parametercheckdispatch.FormatException;
import support.parametercheckdispatch.ParameterCheckDispatch;
import support.parametercheckdispatch.parametergetter.HttpRequestParameterGetterFarm;
import support.parametercheckdispatch.parametergetter.slashstyleurl.LongSlashStyleParameterGetterFarm;
import support.utility.StateUtility;

/**
 * Modify restaurant data
 */
public class ModifyRestaurantServlet extends HttpServlet {
DBManager dbmanager;
    ParameterCheckDispatch pcd;

    @Override
    public void init() throws ServletException {
        dbmanager = (DBManager) super.getServletContext().getAttribute("dbmanager");
        List<String> mustNotBeNull = Arrays.asList("id", "name", "descr", "price", "timetable");
        List<String> canBeNull = Arrays.asList("site", "types");
        Map<String, ParameterCheckDispatch.ParamTypes> typesMapping = new HashMap<>();
        typesMapping.put("name", ParameterCheckDispatch.ParamTypes.STRING);
        typesMapping.put("descr", ParameterCheckDispatch.ParamTypes.STRING);
        typesMapping.put("site", ParameterCheckDispatch.ParamTypes.STRING);
        typesMapping.put("id", ParameterCheckDispatch.ParamTypes.LONG);
        typesMapping.put("price", ParameterCheckDispatch.ParamTypes.STRING);
        typesMapping.put("types", ParameterCheckDispatch.ParamTypes.STRING);
        typesMapping.put("timetable", ParameterCheckDispatch.ParamTypes.STRING);
        
        pcd = new ParameterCheckDispatch(mustNotBeNull, canBeNull, typesMapping, new LongSlashStyleParameterGetterFarm("id", new HttpRequestParameterGetterFarm())) {
            @Override
            protected void mustNotBeNullSomeNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                mustNotBeNullAllNull(parms, request, response);
            }

            @Override
            protected void mustNotBeNullAllNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                throw new StateForbidden();
            }

            @Override
            protected void mustNotBeNullAllSet(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                try {
                    List<String> types = null;
                    if(parms.get("types")!=null) {
                        JSONArray ts = (JSONArray)((JSONArray)JSONValue.parse((String)parms.get("types"))).get(0);
                        if(ts.size() > 0) {
                            types = new ArrayList<>();
                            for(Object t:ts) {
                                types.add((String)t);
                            }
                        }
                    }
                    dbmanager.restaurants.modify(
                            StateUtility.getUser(request),
                            (Long)parms.get("id"),
                            (String)parms.get("name"),
                            (String)parms.get("descr"),
                            (String)parms.get("site"),
                            Restaurant.Price.valueOf((String)parms.get("price")),
                            types,
                            (String)parms.get("timetable"));
                    response.sendRedirect(request.getContextPath()+"/restaurant/"+Long.toString((Long)parms.get("id")));
                } catch (SQLException ex) {
                    throw new ServletException(ex);
                }
            }

        };
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            pcd.check(request, response);
        } catch (FormatException ex) {
            throw new StateForbidden();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
