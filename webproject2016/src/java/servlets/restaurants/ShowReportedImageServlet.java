package servlets.restaurants;

import dbmanager.DBManager;
import exceptions.StateForbidden;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Picture;
import model.Restaurant;
import support.parametercheckdispatch.FormatException;
import support.parametercheckdispatch.ParameterCheckDispatch;
import support.parametercheckdispatch.parametergetter.HttpRequestParameterGetterFarm;
import support.parametercheckdispatch.parametergetter.slashstyleurl.SlashStyleParameterGetterFarm;
import support.utility.StateUtility;

/**
 * Display a reported image to an admin
 */
public class ShowReportedImageServlet extends HttpServlet {
    DBManager dbmanager;
    ParameterCheckDispatch pcd;

    public void init() throws ServletException {
        dbmanager = (DBManager) super.getServletContext().getAttribute("dbmanager");
        List<String> mustNotBeNull = Arrays.asList("id", "action");
        List<String> canBeNull = Arrays.asList();
        Map<String, ParameterCheckDispatch.ParamTypes> typesMapping = new HashMap<>();
        typesMapping.put("id", ParameterCheckDispatch.ParamTypes.LONG);
        typesMapping.put("action", ParameterCheckDispatch.ParamTypes.STRING);
        pcd = new ParameterCheckDispatch(mustNotBeNull, canBeNull, typesMapping, new SlashStyleParameterGetterFarm("action", "(keep|delete)", new HttpRequestParameterGetterFarm())) {
            @Override
            protected void mustNotBeNullSomeNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                Long id = (Long)parms.get("id");
                if(id == null) {
                    throw new StateForbidden();
                }
                else {
                    try {
                        Picture p = dbmanager.pictures.getById((Long)parms.get("id"));
                        if(p==null) {
                            throw new StateForbidden();
                        }
                        else {
                            Restaurant r = dbmanager.restaurants.getById(p.getRestaurant().getId());
                            request.setAttribute("restaurant", r);
                            request.setAttribute("img", p);
                            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/reported_picture.jsp");
                            rd.forward(request, response);
                        }
                    } catch (SQLException ex) {
                        throw new ServletException(ex);
                    }
                }
            }

            @Override
            protected void mustNotBeNullAllNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                throw new StateForbidden();
            }

            @Override
            protected void mustNotBeNullAllSet(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                try {
                    if(dbmanager.pictures.reported((long)parms.get("id"), StateUtility.getUser(request), "keep".equals(parms.get("action")))) {
                        response.setStatus(200);
                    }
                    else {
                        response.sendError(403);
                    }
                } catch (SQLException ex) {
                    throw new StateForbidden(ex);
                }
            }

        };
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            try {
                StateUtility.contextInit(dbmanager, request);
            } catch (SQLException ex) {
                throw new ServletException(ex);
            }
            pcd.check(request, response);
        } catch (FormatException ex) {
            throw new StateForbidden(ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
