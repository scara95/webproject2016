package servlets.restaurants;

import dbmanager.DBManager;
import exceptions.StateForbidden;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import support.parametercheckdispatch.FormatException;
import support.parametercheckdispatch.ParameterCheckDispatch;
import support.parametercheckdispatch.parametergetter.multipart.DefaultExtensionChangeFileRenamePolicy;
import support.parametercheckdispatch.parametergetter.slashstyleurl.LongSlashStyleParameterGetterFarm;
import support.parametercheckdispatch.parametergetter.multipart.MultipartJPGConverterParameterGetterFarm;
import support.utility.StateUtility;

/**
 * Add a picture to a restaurand and go back to previous page
 */
public class AddRestaurantImageServlet extends HttpServlet {
    DBManager dbmanager;
    ParameterCheckDispatch pcd;

    @Override
    public void init() throws ServletException {
        dbmanager = (DBManager) super.getServletContext().getAttribute("dbmanager");
        List<String> mustNotBeNull = Arrays.asList("id", "inputFile");
        List<String> canBeNull = Arrays.asList();
        Map<String, ParameterCheckDispatch.ParamTypes> typesMapping = new HashMap<>();
        typesMapping.put("id", ParameterCheckDispatch.ParamTypes.LONG);
        typesMapping.put("inputFile", ParameterCheckDispatch.ParamTypes.FILE);
        pcd = new ParameterCheckDispatch(mustNotBeNull, canBeNull, typesMapping, new LongSlashStyleParameterGetterFarm("id", new MultipartJPGConverterParameterGetterFarm(getServletContext().getRealPath("/WEB-INF/pictures"), new DefaultExtensionChangeFileRenamePolicy("jpg")))) {
            @Override
            protected void mustNotBeNullSomeNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                File image = (File)parms.get("inputFile");
                if(image!=null) {
                    image.delete();
                }
                response.sendRedirect(StateUtility.untrack(request));
            }

            @Override
            protected void mustNotBeNullAllNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                throw new StateForbidden();
            }

            @Override
            protected void mustNotBeNullAllSet(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                try {
                    dbmanager.restaurants.addPicture((long)parms.get("id"), (File)parms.get("inputFile"));
                } catch (SQLException ex) {
                    throw new ServletException(ex);
                }
                response.sendRedirect(StateUtility.untrack(request));
            }

        };
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            try {
                StateUtility.contextInit(dbmanager, request);
            } catch (SQLException ex) {
                throw new ServletException(ex);
            }
            pcd.check(request, response);
        } catch (FormatException ex) {
            throw new StateForbidden();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
