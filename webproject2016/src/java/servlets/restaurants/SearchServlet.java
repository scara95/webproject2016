package servlets.restaurants;

import dbmanager.DBManager;
import exceptions.StateForbidden;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import support.parametercheckdispatch.FormatException;
import support.parametercheckdispatch.ParameterCheckDispatch;
import support.utility.StateUtility;
import support.utility.parametergetter.SearchParameterGetter;

/**
 * Serve search page.
 */
public class SearchServlet extends HttpServlet {
    DBManager dbmanager;
    ParameterCheckDispatch pcd;

    @Override
    public void init() throws ServletException {
        dbmanager = (DBManager) super.getServletContext().getAttribute("dbmanager");
        SearchParameterGetter getter = new SearchParameterGetter();
        pcd = new ParameterCheckDispatch(getter.getMustNotBeNull(), getter.getCanBeNull(), getter.getTypes(), getter) {
            @Override
            protected void mustNotBeNullSomeNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                request.setAttribute("toInit", false);
                request.getRequestDispatcher("/WEB-INF/jsp/search_json.jsp").forward(request, response);
            }

            @Override
            protected void mustNotBeNullAllNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                request.setAttribute("toInit", false);
                request.getRequestDispatcher("/WEB-INF/jsp/search_json.jsp").forward(request, response);
            }

            @Override
            protected void mustNotBeNullAllSet(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                request.setAttribute("toInit", true);
                String path = request.getContextPath()+"/search_json"+request.getPathInfo();
                String query = request.getQueryString();
                if(query != null) {
                    path += "?"+query;
                }
                request.setAttribute("initRequest", path);
                request.getRequestDispatcher("/WEB-INF/jsp/search_json.jsp").forward(request, response);
            }
        };
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        StateUtility.track(request);
        try {
            try {
                StateUtility.contextInit(dbmanager, request);
                request.setAttribute("cookTypes", dbmanager.restaurants.getTypes());
            } catch (SQLException ex) {
                throw new ServletException(ex);
            }
            pcd.check(request, response);
        } catch (FormatException ex) {
            throw new StateForbidden(ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
