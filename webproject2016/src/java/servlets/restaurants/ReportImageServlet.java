package servlets.restaurants;

import dbmanager.DBManager;
import exceptions.StateForbidden;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import support.parametercheckdispatch.FormatException;
import support.parametercheckdispatch.ParameterCheckDispatch;
import support.parametercheckdispatch.parametergetter.HttpRequestParameterGetterFarm;
import support.parametercheckdispatch.parametergetter.slashstyleurl.SlashStyleParameterGetterFarm;
import support.utility.StateUtility;

/**
 * Ajax service to accept or refuse an uploaded image.
 * Answer with 200/403
 */
public class ReportImageServlet extends HttpServlet {
    DBManager dbmanager;
    ParameterCheckDispatch pcd;

    @Override
    public void init() throws ServletException {
        dbmanager = (DBManager) super.getServletContext().getAttribute("dbmanager");
        List<String> mustNotBeNull = Arrays.asList("id", "action");
        List<String> canBeNull = Arrays.asList();
        Map<String, ParameterCheckDispatch.ParamTypes> typesMapping = new HashMap<>();
        typesMapping.put("id", ParameterCheckDispatch.ParamTypes.LONG);
        typesMapping.put("action", ParameterCheckDispatch.ParamTypes.STRING);
        pcd = new ParameterCheckDispatch(mustNotBeNull, canBeNull, typesMapping, new SlashStyleParameterGetterFarm("action", "(report|accept)", new HttpRequestParameterGetterFarm())) {
            @Override
            protected void mustNotBeNullSomeNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                throw new AssertionError("Impossible happened, go take some coffee");
            }

            @Override
            protected void mustNotBeNullAllNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                throw new StateForbidden();
            }

            @Override
            protected void mustNotBeNullAllSet(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                try {
                    if(dbmanager.pictures.report((long)parms.get("id"), StateUtility.getUser(request), "accept".equals(parms.get("action")))) {
                        response.setStatus(200);
                    }
                    else {
                        response.sendError(403);
                    }
                } catch (SQLException ex) {
                    throw new StateForbidden(ex);
                }
            }

        };
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            pcd.check(request, response);
        } catch (FormatException ex) {
            throw new StateForbidden(ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
