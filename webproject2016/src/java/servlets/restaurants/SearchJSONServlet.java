package servlets.restaurants;

import dbmanager.DBManager;
import exceptions.StateForbidden;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import support.parametercheckdispatch.FormatException;
import support.parametercheckdispatch.ParameterCheckDispatch;
import support.utility.StateUtility;
import support.utility.parametergetter.SearchParameterGetter;

/**
 * JSON search service.
 * Return an array of results
 */
public class SearchJSONServlet extends HttpServlet {
    DBManager dbmanager;
    ParameterCheckDispatch pcd;

    public void init() throws ServletException {
        dbmanager = (DBManager) super.getServletContext().getAttribute("dbmanager");
        SearchParameterGetter getter = new SearchParameterGetter();
        pcd = new ParameterCheckDispatch(getter.getMustNotBeNull(), getter.getCanBeNull(), getter.getTypes(), getter) {
            @Override
            protected void mustNotBeNullSomeNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                String price = (String)parms.get("price");
                if(price == null) {
                    getRestaurants((String)parms.get("type"), (String)parms.get("args"), "ALL", (String)parms.get("typelist"), (String)parms.get("name"), request, response);
                }
            }

            @Override
            protected void mustNotBeNullAllNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                throw new StateForbidden();
            }

            @Override
            protected void mustNotBeNullAllSet(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                getRestaurants((String)parms.get("type"), (String)parms.get("args"), (String)parms.get("price"), (String)parms.get("typelist"), (String)parms.get("name"), request, response);
            }

            final private void getRestaurants(String type, String args, String price, String typelist, String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                try {
                    response.setContentType("application/json;charset=UTF-8");
                    dbmanager.restaurants.searchJSON(type, args, price, typelist, name, StateUtility.getBundle(request)).writeJSONString(response.getWriter());
                } catch (SQLException ex) {
                    throw new ServletException(ex);
                }
            }
        };
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            pcd.check(request, response);
        } catch (FormatException ex) {
            throw new StateForbidden(ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
