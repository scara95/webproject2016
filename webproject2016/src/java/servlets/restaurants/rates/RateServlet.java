package servlets.restaurants.rates;

import dbmanager.DBManager;
import exceptions.KeyDuplication;
import exceptions.StateForbidden;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.User;
import support.parametercheckdispatch.FormatException;
import support.parametercheckdispatch.ParameterCheckDispatch;
import support.parametercheckdispatch.parametergetter.HttpRequestParameterGetterFarm;
import support.parametercheckdispatch.parametergetter.slashstyleurl.LongSlashStyleParameterGetterFarm;
import support.utility.StateUtility;

/**
 * Add a review to a restaurant. User is implicitly the logged in user.
 * Redirect to previous page.
 */
public class RateServlet extends HttpServlet {
    DBManager dbmanager;
    ParameterCheckDispatch pcd;

    @Override
    public void init() throws ServletException {
        dbmanager = (DBManager)super.getServletContext().getAttribute("dbmanager");
        List<String> mustNotBeNull = Arrays.asList("restaurant-id", "rate-text", "rate-value");
        List<String> canBeNull = Arrays.asList();
        Map<String, ParameterCheckDispatch.ParamTypes> typesMapping = new HashMap<>();
        typesMapping.put("restaurant-id", ParameterCheckDispatch.ParamTypes.LONG);
        typesMapping.put("rate-value", ParameterCheckDispatch.ParamTypes.LONG);
        typesMapping.put("rate-text", ParameterCheckDispatch.ParamTypes.STRING);
         pcd = new ParameterCheckDispatch(mustNotBeNull, canBeNull, typesMapping, new LongSlashStyleParameterGetterFarm("restaurant-id", new HttpRequestParameterGetterFarm())) {
            @Override
            protected void mustNotBeNullSomeNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                throw new StateForbidden();
            }

            @Override
            protected void mustNotBeNullAllNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                throw new StateForbidden();
            }

            @Override
            protected void mustNotBeNullAllSet(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                HttpSession session = request.getSession();
                User user = (User)session.getAttribute("user");
                try {
                    dbmanager.rates.add(user.getId(), (long)parms.get("restaurant-id"), (int)(long)parms.get("rate-value"), (String)parms.get("rate-text"));
                } catch (SQLException ex) {
                    if(ex.getSQLState().startsWith("23")) {
                        throw new KeyDuplication("Hai già inserito una recensione oggi!");
                    }
                    else {
                        throw new ServletException(ex);
                    }
                }
                response.sendRedirect(StateUtility.untrack(request));
            }

         };
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            try {
                StateUtility.contextInit(dbmanager, request);
            } catch (SQLException ex) {
                throw new ServletException(ex);
            }
            pcd.check(request, response);
        } catch (FormatException ex) {
            throw new StateForbidden();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
