package servlets.restaurants.rates;

import dbmanager.DBManager;
import exceptions.StateForbidden;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import support.parametercheckdispatch.FormatException;
import support.parametercheckdispatch.ParameterCheckDispatch;
import support.utility.StateUtility;

/**
 *
 * Ajax service to add an answer to a review.
 * Must be owner.
 * Answer with 200/403/500
 */
public class AnswerRateServlet extends HttpServlet {
    DBManager dbmanager;
    ParameterCheckDispatch pcd;

    @Override
    public void init() throws ServletException {
        dbmanager = (DBManager)super.getServletContext().getAttribute("dbmanager");
        List<String> mustNotBeNull = Arrays.asList("restaurant_id", "user_id", "day", "answer");
        List<String> canBeNull = Arrays.asList();
        Map<String, ParameterCheckDispatch.ParamTypes> typesMapping = new HashMap<>();
        typesMapping.put("restaurant_id", ParameterCheckDispatch.ParamTypes.LONG);
        typesMapping.put("user_id", ParameterCheckDispatch.ParamTypes.LONG);
        typesMapping.put("day", ParameterCheckDispatch.ParamTypes.STRING);
        typesMapping.put("answer", ParameterCheckDispatch.ParamTypes.STRING);
         pcd = new ParameterCheckDispatch(mustNotBeNull, canBeNull, typesMapping) {
            @Override
            protected void mustNotBeNullSomeNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                mustNotBeNullAllNull(parms, request, response);
            }

            @Override
            protected void mustNotBeNullAllNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                response.sendError(500);
            }

            @Override
            protected void mustNotBeNullAllSet(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                String s_day = (String)parms.get("day");
                Date day;
                try {
                    day = new SimpleDateFormat("yyyy-MM-dd").parse(s_day);
                } catch (ParseException ex) {
                    throw new ServletException(ex);
                }
                try {
                    if(dbmanager.rates.answer(StateUtility.getUser(request), (long)parms.get("user_id"), (long)parms.get("restaurant_id"), day, (String)parms.get("answer"))) {
                        response.setStatus(200);
                    }
                    else {
                        response.sendError(403);
                    }
                } catch (SQLException ex) {
                    throw new StateForbidden(ex);
                }
            }

         };
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            pcd.check(request, response);
        } catch (FormatException ex) {
            throw new StateForbidden(ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
