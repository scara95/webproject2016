package servlets.restaurants.rates;

import dbmanager.DBManager;
import exceptions.StateForbidden;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Rate;
import support.parametercheckdispatch.FormatException;
import support.parametercheckdispatch.ParameterCheckDispatch;
import support.utility.StateUtility;

/**
 * Ajax service to accept or refuse an answer to a review.
 * Must be admin.
 * Serve page if accept parameter is null.
 */
public class AcceptRateAnswer extends HttpServlet {
    DBManager dbmanager;
    ParameterCheckDispatch pcd;

    @Override
    public void init() throws ServletException {
        dbmanager = (DBManager)super.getServletContext().getAttribute("dbmanager");
        List<String> mustNotBeNull = Arrays.asList("restaurant_id", "user_id", "day");
        List<String> canBeNull = Arrays.asList("accept");
        Map<String, ParameterCheckDispatch.ParamTypes> typesMapping = new HashMap<>();
        typesMapping.put("restaurant_id", ParameterCheckDispatch.ParamTypes.LONG);
        typesMapping.put("user_id", ParameterCheckDispatch.ParamTypes.LONG);
        typesMapping.put("day", ParameterCheckDispatch.ParamTypes.STRING);
        typesMapping.put("accept", ParameterCheckDispatch.ParamTypes.STRING);
         pcd = new ParameterCheckDispatch(mustNotBeNull, canBeNull, typesMapping) {
            @Override
            protected void mustNotBeNullSomeNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                mustNotBeNullAllNull(parms, request, response);
            }

            @Override
            protected void mustNotBeNullAllNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                throw new StateForbidden();
            }

            @Override
            protected void mustNotBeNullAllSet(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                String accept = (String)parms.get("accept");Date day;
                try {
                    day = new SimpleDateFormat("yyyy-MM-dd").parse((String)parms.get("day"));
                } catch (ParseException ex) {
                    throw new ServletException(ex);
                }
                boolean success = false;
                if(accept == null) {
                    try {
                        Rate r = dbmanager.rates.getById((long)parms.get("user_id"), (long)parms.get("restaurant_id"), day);
                        request.setAttribute("restaurant", r.getRestaurant());
                        request.setAttribute("rate", r);
                        request.getRequestDispatcher("/WEB-INF/jsp/accept_rate_answer.jsp").forward(request, response);
                    } catch (SQLException ex) {
                        throw new StateForbidden(ex);
                    }
                }
                else {
                    if("accept".equals(accept)) {
                        try {
                            success = dbmanager.rates.acceptAnswer(StateUtility.getUser(request), (long)parms.get("user_id"), (long)parms.get("restaurant_id"), day);
                        } catch (SQLException ex) {
                            throw new ServletException(ex);
                        }
                    }
                    else if("refuse".equals(accept)) {
                        try {
                            success = dbmanager.rates.refuseAnswer(StateUtility.getUser(request), (long)parms.get("user_id"), (long)parms.get("restaurant_id"), day);
                        } catch (SQLException ex) {
                            throw new ServletException(ex);
                        }
                    }
                    if(success) {
                        response.setStatus(200);
                    }
                    else {
                        response.sendError(403);
                    }
                }
            }

         };
    }
    /**
     * Processes requests for both HTTP GET and POST
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            try {
                StateUtility.contextInit(dbmanager, request);
            } catch (SQLException ex) {
                throw new ServletException(ex);
            }
            pcd.check(request, response);
        } catch (FormatException ex) {
            throw new StateForbidden(ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
