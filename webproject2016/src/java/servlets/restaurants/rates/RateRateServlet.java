package servlets.restaurants.rates;

import dbmanager.DBManager;
import exceptions.StateForbidden;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.User;
import support.parametercheckdispatch.FormatException;
import support.parametercheckdispatch.ParameterCheckDispatch;

/**
 * Ajax service to rate a user review
 * Answer with 400/200 based on success
 */
public class RateRateServlet extends HttpServlet {
    DBManager dbmanager;
    ParameterCheckDispatch pcd;

    @Override
    public void init() throws ServletException {
        dbmanager = (DBManager)super.getServletContext().getAttribute("dbmanager");
        List<String> mustNotBeNull = Arrays.asList("rate_user", "rate_restaurant", "rate_day", "what");
        List<String> canBeNull = Arrays.asList();
        Map<String, ParameterCheckDispatch.ParamTypes> typesMapping = new HashMap<>();
        typesMapping.put("rate_user", ParameterCheckDispatch.ParamTypes.LONG);
        typesMapping.put("rate_restaurant", ParameterCheckDispatch.ParamTypes.LONG);
        typesMapping.put("rate_day", ParameterCheckDispatch.ParamTypes.STRING);
        typesMapping.put("what", ParameterCheckDispatch.ParamTypes.STRING);
        pcd = new ParameterCheckDispatch(mustNotBeNull, canBeNull, typesMapping) {
            @Override
            protected void mustNotBeNullSomeNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
               response.sendError(400);
            }

            @Override
            protected void mustNotBeNullAllNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                response.sendError(400);
            }

            @Override
            protected void mustNotBeNullAllSet(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                HttpSession session = request.getSession();
                User user = (User) session.getAttribute("user");
                String s_day = (String)parms.get("rate_day");
                Date day;
                try {
                    day = new SimpleDateFormat("yyyy-MM-dd").parse(s_day);
                } catch (ParseException ex) {
                    throw new ServletException(ex);
                }
                String s_rate = (String)parms.get("what");
                boolean rate;
                switch (s_rate) {
                    case "up":
                        rate = true;
                        break;
                    case "down":
                        rate = false;
                        break;
                    default:
                        response.sendError(400);
                        return;
                }
                try {
                    dbmanager.rates.rate(user.getId(), (long)parms.get("rate_user"), (long)parms.get("rate_restaurant"), day, rate);
                    response.setStatus(204);
                } catch (SQLException ex) {
                    response.sendError(400);
                }
            }
        };
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            pcd.check(request, response);
        } catch (FormatException ex) {
            throw new StateForbidden();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
