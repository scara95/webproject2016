package servlets;

import dbmanager.DBManager;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import support.utility.StateUtility;

/**
 * Serve ntification page setting needed context
 */
public class NotificationsPageServlet extends HttpServlet {
    DBManager dbmanager;
    
    @Override
    public void init() throws ServletException {
        dbmanager = (DBManager) super.getServletContext().getAttribute("dbmanager");
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        StateUtility.track(request);
        try {
            StateUtility.contextInit(dbmanager, request);
        } catch (SQLException ex) {
                throw new ServletException(ex);
        }
        if(StateUtility.logged(request)) {
            try {
                request.setAttribute("notificationsList", dbmanager.users.getNotifications(StateUtility.getBundle(request), StateUtility.getUser(request)));
            } catch (SQLException ex) {
            }
        }
        else {
            request.setAttribute("notificationsList", new JSONArray());
        }
        request.getRequestDispatcher("/WEB-INF/jsp/all_notifications.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
