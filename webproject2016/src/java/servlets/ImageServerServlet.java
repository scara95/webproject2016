package servlets;

import dbmanager.DBManager;
import exceptions.StateForbidden;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.User;
import support.parametercheckdispatch.FormatException;
import support.parametercheckdispatch.ParameterCheckDispatch;
import support.parametercheckdispatch.parametergetter.slashstyleurl.PrefixSlashStyleParameterGetterFarm;
import javax.servlet.RequestDispatcher;

/**
 * Serve avatars and pictures based on their id
 */
public class ImageServerServlet extends HttpServlet {
    DBManager dbmanager;
    ParameterCheckDispatch pcd;
    
    @Override
    public void init() throws ServletException {
        dbmanager = (DBManager) super.getServletContext().getAttribute("dbmanager");
        List<String> mustNotBeNull = Arrays.asList("id", "prefix");
        List<String> canBeNull = Arrays.asList();
        Map<String, ParameterCheckDispatch.ParamTypes> typesMapping = new HashMap<>();
        typesMapping.put("id", ParameterCheckDispatch.ParamTypes.LONG);
        typesMapping.put("prefix", ParameterCheckDispatch.ParamTypes.STRING);
        pcd = new ParameterCheckDispatch(mustNotBeNull, canBeNull, typesMapping, new PrefixSlashStyleParameterGetterFarm("id", "prefix", new String[]{"/avatar", "/picture"}, "([1-9][0-9]*).jpg", null)) {
            @Override
            protected void mustNotBeNullSomeNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                throw new AssertionError("Go take some coffee! Impossible happened!");
            }

            @Override
            protected void mustNotBeNullAllNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                response.sendError(404);
            }

            @Override
            protected void mustNotBeNullAllSet(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                String prefix = (String) parms.get("prefix");
                if(prefix.equals("/avatar")) {
                    try {
                        User user = dbmanager.users.getById((long)parms.get("id"));
                        if(user == null) {
                            response.sendError(404);
                        } else {
                            String avatar = user.getAvatar();
                            if(avatar == null) {
                                response.sendError(404);
                            } else {
                                RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/avatars/"+avatar);
                                rd.forward(request, response);
                            }
                        }
                    } catch (SQLException ex) {
                        throw new ServletException(ex);
                    }
                }
                else if(prefix.equals("/picture")) {
                    try {
                        String picture = dbmanager.pictures.getFilenameById((long)parms.get("id"));
                        if(picture == null) {
                            response.sendError(404);
                        } else {
                            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/pictures/"+picture);
                            rd.forward(request, response);
                        }
                    } catch (SQLException ex) {
                        throw new ServletException(ex);
                    }
                }
            }
            
        };
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            pcd.check(request, response);
        } catch (FormatException ex) {
            throw new StateForbidden(ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
