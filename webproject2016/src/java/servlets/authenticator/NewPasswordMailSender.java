package servlets.authenticator;

import java.io.PrintWriter;
import java.util.ResourceBundle;
import support.mailing.GMailMailSender;

/**
 * Mailing service for new password
 */
public class NewPasswordMailSender extends GMailMailSender {

    public NewPasswordMailSender(String email, String password) {
        super(email, password);
    }

    @Override
    protected void compose(ResourceBundle bundle, PrintWriter subject, PrintWriter body, Object data) {
        Data d = (Data) data;
        subject.print(bundle.getString("titleNewPassword") + " " + bundle.getString("siteName"));
        body.println(bundle.getString("msgNewPassword"));
        body.println(bundle.getString("msgNewPassword2") + " " + d.getPassword());
    }

    public static class Data {

        final private String password;

        public String getPassword() {
            return password;
        }

        public Data(String password) {
            this.password = password;
        }

    }
}
