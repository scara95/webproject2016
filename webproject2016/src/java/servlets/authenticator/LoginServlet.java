package servlets.authenticator;

import dbmanager.DBManager;
import exceptions.StateForbidden;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.User;
import support.parametercheckdispatch.FormatException;
import support.parametercheckdispatch.ParameterCheckDispatch;
import support.utility.StateUtility;

/**
 * Check for user and password and log in if successful.
 * email and password can be passed by get or post. Obvious security practice
 * is to pass them as post
 */
public class LoginServlet extends HttpServlet {
    DBManager dbmanager;
    ParameterCheckDispatch pcd;
    @Override
    public void init() throws ServletException {
        dbmanager = (DBManager)super.getServletContext().getAttribute("dbmanager");
        List<String> mustNotBeNull = Arrays.asList("email", "password");
        List<String> canBeNull = Arrays.asList();
        Map<String, ParameterCheckDispatch.ParamTypes> typesMapping = new HashMap<>();
        typesMapping.put("email", ParameterCheckDispatch.ParamTypes.STRING);
        typesMapping.put("password", ParameterCheckDispatch.ParamTypes.STRING);
        pcd = new ParameterCheckDispatch(mustNotBeNull, canBeNull, typesMapping) {
            @Override
            protected void mustNotBeNullSomeNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                request.setAttribute("error", true);
                RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
                rd.forward(request, response);
            }

            @Override
            protected void mustNotBeNullAllNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
                rd.forward(request, response);
            }

            @Override
            protected void mustNotBeNullAllSet(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                User user = null;
                String email = (String) parms.get("email");
                String password = (String) parms.get("password");
                try {
                    user = dbmanager.users.authenticate(email, password);

                } catch (SQLException ex) {
                    throw new ServletException(ex);
                }
                if(user == null) {
                    request.setAttribute("error", true);
                    RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
                    rd.forward(request, response);
                }
                else {
                    HttpSession session = request.getSession();
                    session.setAttribute("user", user);
                    request.setAttribute("header", StateUtility.getBundle(request).getString("login"));
                    request.setAttribute("message", StateUtility.getBundle(request).getString("msgLoggedIn"));
                    RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/success.jsp");
                    rd.forward(request, response);
                }
            }  
        };
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(StateUtility.logged(request)) {
            response.sendRedirect(StateUtility.untrack(request));
        } else {
            try {
                try {
                    StateUtility.contextInit(dbmanager, request);
                } catch (SQLException ex) {
                    throw new ServletException(ex);
                }
                pcd.check(request, response);
            } catch (FormatException ex) {
                throw new StateForbidden();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
