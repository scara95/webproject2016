package support.authenticators;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/**
 * Implements PBKDF2WithHmacSHA512 encryption with random salt and selectable iterations
 */
public final class PBKDF2WithHmacSHA512 extends Authenticator
{
    public static final String PREFIX = "01";
    
    private static final int SALT_LENGTH = 14;
    private final int iterations;

    public PBKDF2WithHmacSHA512(String[] parameters) {
        super(parameters);
        iterations = Integer.parseInt(parameters[0], 10);
        if(iterations <= 0) {
            throw new AssertionError("iterations must be possitive");
        }
    }

    @Override
    public String encrypt(char[] password) {
        byte[] salt = new byte[SALT_LENGTH];
        byte[] key;
        try {
            SecureRandom.getInstance("SHA1PRNG").nextBytes(salt);
        } catch (NoSuchAlgorithmException ex) {
            throw new AssertionError("backend PRNG algorithm must be present");
        }
        key = hash(password, iterations, salt);
        byte[] hash = new byte[salt.length+key.length];
        System.arraycopy(salt, 0, hash, 0,           salt.length);
        System.arraycopy(key,  0, hash, salt.length, key.length );
        return PREFIX+":"+String.format("%010d", iterations)+":"+Base64.getEncoder().encodeToString(hash);
    }
    
    @Override
    public boolean authenticate(char[] password, String encrypted) {
        byte[] hash = Base64.getDecoder().decode(encrypted);
        byte[] salt = Arrays.copyOfRange(hash, 0, SALT_LENGTH);
        byte[] key = Arrays.copyOfRange(hash, SALT_LENGTH, hash.length);
        return Arrays.equals(key, hash(password, iterations, salt));
    }
    

    private byte[] hash(char[] password, int iterations, byte[] salt) {
        KeySpec spec = new PBEKeySpec(password, salt, iterations, 512);
        try {
          SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
          return f.generateSecret(spec).getEncoded();
        }
        catch (NoSuchAlgorithmException ex) {
            throw new AssertionError("backend algorithm must be present");
        }
        catch (InvalidKeySpecException ex) {
          throw new IllegalStateException("Invalid SecretKeyFactory", ex);
        }
    }

}
