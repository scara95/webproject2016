package support.authenticators;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;


/**
 * Implement a simple fixed salt SHA512 hashing
 * Defaults to no salt
 */
public class SaltedSHA512 extends Authenticator {
    public final static String PREFIX = "02";
    
    private static char[] salt = new char[0];
    
    /**
     * Set globally used salt
     * @param aSalt salt
     */
    public static void setSalt(char[] aSalt) {
        salt = aSalt;
    }
    
    /**
     * Get currently used global salt
     * @return salt
     */
    public static char[] getSalt() {
        return salt;
    }
    
    public SaltedSHA512(String[] parameters) {
        super(parameters);
    }

    @Override
    public String encrypt(char[] password) {
        try {
            return PREFIX+":"+Base64.getEncoder().encodeToString(hash(password, salt));
        } catch (NoSuchAlgorithmException ex) {
            throw new AssertionError("backend algorithm must be present");
        }
    }

    @Override
    public boolean authenticate(char[] password, String encrypted) {
        try {
            return Arrays.equals(hash(password, salt), Base64.getDecoder().decode(encrypted));
        } catch (NoSuchAlgorithmException ex) {
            throw new AssertionError("backend algorithm must be present");
        }
    }
    
    private byte[] hash(char[] password, char[] salt) throws NoSuchAlgorithmException {
        char[] salted = new char[salt.length+password.length];
        System.arraycopy(salt, 0, salted, 0, salt.length);
        System.arraycopy(password, 0, salted, salt.length, password.length);
        return MessageDigest.getInstance("SHA-512").digest(new String(salted).getBytes());
    }
    
    
}
