package support.authenticators;

/**
 * Authenticator interface
 */
public abstract class Authenticator {
    /**
     * Construct 
     * @param parameters parameters to specific authenticator
     */
    public Authenticator(String[] parameters) {
        
    }
    
    /**
     * Return an encrypted token for the given password to be used on
     * authentication
     * @param password password to be encrypted
     * @return encrypted token
     */
    public abstract String encrypt(char[] password);
    
    /**
     * Authenticate a password given a generated token
     * @param password password to autenticate
     * @param encrypted token
     * @return t/f success/fail
     */
    public abstract boolean authenticate(char[] password, String encrypted);
    
    /**
     * encrypt(password.toCharArray()) equivalent
     * @param password password to be encrypted
     * @return encrypted token
     */
    public String encrypt(String password) {
        return encrypt(password.toCharArray());
    }
    
    /**
     * authenticate(password.toCharArray(), encrypted) equivalent
     * @param password password to autenticate
     * @param encrypted token
     * @return t/f success/fail
     */
    public boolean authenticate(String password, String encrypted) {
        return authenticate(password.toCharArray(), encrypted);
    }
    
}
