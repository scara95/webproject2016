package support.authenticators;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Permit authenticator registration and selection from token
 */
public class AuthenticatorFactory {
    static Map<String, Class<?>> authenticators = new HashMap<>();
    
    /**
     * Register a class from class name using his PREFIX field as selector
     * @param auth class name
     */
    public static void register(String auth) {
        Class c;
        try {
            c = Class.forName(auth);
        } catch (ClassNotFoundException ex) {
            throw new AssertionError("Auth system: " + auth + " is supposed to be present");
        }
        register(c);
    }
    
    /**
     * Register a class from class object using his PREFIX field as selector
     * @param c class object
     */
    public static void register(Class<?> c) {
        String auth = c.getCanonicalName();
        try {
            AuthenticatorFactory.register((String)c.getDeclaredField("PREFIX").get(null), c);
        } catch (NoSuchFieldException ex) {
            throw new AssertionError("Not a valid auth system: " + auth);
        } catch (SecurityException ex) {
            throw new AssertionError("You should be in a non secured environment");
        } catch (IllegalAccessException | IllegalArgumentException ex) {
            throw new AssertionError("PREFIX is supposed to be final static public");
        }
    }
    
    /**
     * Register a given class with a custom prefix
     * @param prefix prefix
     * @param auth class object
     */
    public static void register(String prefix, Class<?> auth) {
        authenticators.put(prefix, auth);
    }
    
    /**
     * Select authenticator based on generated token
     * @param token generated token
     * @return Authenticator instance
     */
    public static Authenticator getAuthenticator(String token) {
        String[] tokens = token.split(":");
        return getAuthenticator(tokens);
    }
    
    /**
     * Select authenticator and authenticate given a token
     * @param password password to autenticate
     * @param token token
     * @return t/f success/fail
     */
    public static boolean authenticate(String password, String token) {
        String[] tokens = token.split(":");
        return getAuthenticator(Arrays.copyOfRange(tokens, 0, tokens.length-1)).authenticate(password, tokens[tokens.length-1]);
    }
    
    /**
     * Return and autenthicator given a selector+parameters array
     * @param selparms {selector, parameter1, ..., parameterN}
     * @return 
     */
    private static Authenticator getAuthenticator(String[] selparms) {
        if(!authenticators.containsKey(selparms[0])) {
            throw new IllegalArgumentException("No authenticator found");
        }
        Class<?> c = authenticators.get(selparms[0]);
        try {
            return (Authenticator)c.getConstructor(String[].class).newInstance((Object)Arrays.copyOfRange(selparms, 1, selparms.length));
        } catch (NoSuchMethodException ex) {
            throw new AssertionError("There should be a constructor");
        } catch (SecurityException ex) {
            throw new AssertionError("You should be in a non secured environment");
        } catch (InvocationTargetException | InstantiationException | IllegalAccessException | IllegalArgumentException ex) {
            throw new AssertionError("Can not instantiate authenticator");
        }
    }
    
}
