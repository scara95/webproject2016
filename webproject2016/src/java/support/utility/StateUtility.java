package support.utility;

import dbmanager.DBManager;
import java.sql.SQLException;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ResourceBundle;
import model.User;

/**
 * Help managing session attributes and state
 */
public class StateUtility {
    
    /**
     * Check if a user is currently logged
     * @param request
     * @return t/f
     */
    public static boolean logged(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        return session != null && session.getAttribute("user")!=null;
    }
    
    /**
     * Get the currently logged user
     * @param request
     * @return user object or null
     */
    public static User getUser(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        return session == null ? null : (User)session.getAttribute("user");
    }
    
    /**
     * track a url for backtracking
     * @param request 
     */
    public static void track(HttpServletRequest request) {
        HttpSession session = request.getSession();
        String query = request.getQueryString();
        String uri = request.getRequestURI();
        if(uri == null) {
            uri = request.getContextPath();
        }
        if(query != null) {
            uri = uri + "?" + query;
        }
        session.setAttribute("support.utility.StateUtility.track", uri);
    }
    
    /**
     * Return the last tracked url
     * @param request
     * @return last tracked url
    */
    public static String untrack(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if(session == null) {
            return request.getContextPath();
        }
        String uri = (String)session.getAttribute("support.utility.StateUtility.track");
        return uri == null ? request.getContextPath() : uri;
    }
    
    /**
     * initialize some request attribute needed for most pages
     * @param dbmanager
     * @param request
     * @throws SQLException 
     */
    public static void contextInit(DBManager dbmanager, HttpServletRequest request) throws SQLException {
        boolean state = false;
        if(logged(request)) {
            User user = getUser(request);
            state = dbmanager.users.hasRestaurants(user) || user.isAdmin();
            request.setAttribute("ownedRestaurants", dbmanager.users.getRestaurants(user));
        }
        request.setAttribute("notifications", state);
        request.setAttribute("lang", getLocale(request).getLanguage());
    }
    
    /**
     * Return current selected locale or default
     * @param request
     * @return current locale
     */
    public static Locale getLocale(HttpServletRequest request) {
        HttpSession session = null;
        if(request != null) session = request.getSession(false);
        Locale locale = null;
        if(session != null) {
            locale = (Locale)session.getAttribute("locale");
        }
        return locale == null ? Locale.getDefault() : locale;
    }
    
    /**
     * Set current locale
     * @param request
     * @param locale 
     */
    public static void setLocale(HttpServletRequest request, Locale locale)  {
        HttpSession session = request.getSession();
        session.setAttribute("locale", locale);
    }
    
    /**
     * Get a translation bundle based on currently selected locale
     * @param request
     * @return 
     */
    public static ResourceBundle getBundle(HttpServletRequest request) {
        return ResourceBundle.getBundle("resources.messages", getLocale(request));
    }
}
