package support.utility.parametergetter;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import support.parametercheckdispatch.FormatException;
import support.parametercheckdispatch.ParameterCheckDispatch;
import support.parametercheckdispatch.parametergetter.HttpRequestParameterGetterFarm;
import support.parametercheckdispatch.parametergetter.ParameterGetter;
import support.parametercheckdispatch.parametergetter.ParameterGetterFarm;
import support.parametercheckdispatch.parametergetter.slashstyleurl.MultipatternPrefixSlashStyleParameterGetterFarm;
import support.parametercheckdispatch.parametergetter.slashstyleurl.MultipatternPrefixSlashStyleParameterGetterFarm.NP;

/**
 * Parameter getter for search url
 */
public final class SearchParameterGetter implements ParameterGetterFarm {
    private final MultipatternPrefixSlashStyleParameterGetterFarm getter;
    private final List<String> mustNotBeNull = Arrays.asList("type", "price");
    private final List<String> canBeNull = Arrays.asList("typelist", "args", "name");
    private final Map<String, ParameterCheckDispatch.ParamTypes> types = new HashMap<>();
    
    public SearchParameterGetter() {
        types.put("type", ParameterCheckDispatch.ParamTypes.STRING);
        types.put("args", ParameterCheckDispatch.ParamTypes.STRING);
        types.put("price", ParameterCheckDispatch.ParamTypes.STRING);
        types.put("typelist", ParameterCheckDispatch.ParamTypes.STRING);
        types.put("name", ParameterCheckDispatch.ParamTypes.STRING);
        Map<String, List<NP>> matchers = new HashMap<>();
        NP price = new NP("price", "(LOW|MID|HIGH|ALL)");
        NP string_arg = new NP("args", "(.+)");
        matchers.put("/", Arrays.asList(price));
        matchers.put("/state", Arrays.asList(string_arg, price));
        matchers.put("/province", Arrays.asList(string_arg, price));
        matchers.put("/city", Arrays.asList(string_arg, price));
        matchers.put("/near", Arrays.asList(new NP("args", "(-?[0-9]+\\.[0-9]+,-?[0-9]+\\.[0-9]+,[0-9]+(\\.[0-9]+)?)"), price));
        matchers.put("/address", Arrays.asList(new NP("args", "(-?[0-9]+\\.[0-9]+,-?[0-9]+\\.[0-9]+)"), price));
        getter = new MultipatternPrefixSlashStyleParameterGetterFarm("type", matchers, "typelist", new HttpRequestParameterGetterFarm());
    }

    public List<String> getMustNotBeNull() {
        return mustNotBeNull;
    }

    public List<String> getCanBeNull() {
        return canBeNull;
    }

    public Map<String, ParameterCheckDispatch.ParamTypes> getTypes() {
        return types;
    }

    @Override
    public ParameterGetter create(HttpServletRequest request) throws IOException, FormatException {
        return getter.create(request);
    }
    
}
