package support.parametercheckdispatch;

import support.parametercheckdispatch.parametergetter.ParameterGetter;
import support.parametercheckdispatch.parametergetter.HttpRequestParameterGetterFarm;
import java.io.File;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import support.parametercheckdispatch.parametergetter.ParameterGetterFarm;

/**
 * Declaratively check for parameters and dispatch based on given parameters
 */
public abstract class ParameterCheckDispatch {
    public enum ParamTypes {LONG, DOUBLE, STRING, FILE};
    final List<String> mustNotBeNull;
    final List<String> canBeNull;
    final Map<String, ParamTypes> typesMapping;
    final ParameterGetterFarm getterFarm;
    
    /**
     * Constructor, the getter farm is implicity supposed to be HttpRequestParameterGetterFactory
     * @param mustNotBeNull list of parameter that you want to be given
     * @param canBeNull list of optional parameters
     * @param typesMapping type mapping for parameters
     */
    public ParameterCheckDispatch(List<String> mustNotBeNull, List<String> canBeNull, Map<String, ParamTypes> typesMapping) {
        this(mustNotBeNull, canBeNull, typesMapping, new HttpRequestParameterGetterFarm());
    }
    
    /**
     * Constructor
     * @param mustNotBeNull list of parameter that you want to be given
     * @param canBeNull list of optional parameters
     * @param typesMapping type mapping for parameters
     * @param getterFarm parameter getter farm
     */
    public ParameterCheckDispatch(List<String> mustNotBeNull, List<String> canBeNull, Map<String, ParamTypes> typesMapping, ParameterGetterFarm getterFarm) {
        this.mustNotBeNull = mustNotBeNull;
        this.canBeNull = canBeNull;
        this.typesMapping = typesMapping;
        this.getterFarm = getterFarm;
        //check that all given parameters are typed
        boolean wellTyped = true;
        for(String mnbn:mustNotBeNull) {
            if(!wellTyped) {
                break;
            }
            wellTyped = wellTyped && typesMapping.containsKey(mnbn);
        }
        for(String cbn:canBeNull) {
            if(!wellTyped) {
                break;
            }
            wellTyped = wellTyped && typesMapping.containsKey(cbn);
        }
        if(!wellTyped) {
            throw new AssertionError("There must be a type for every parameter.");
        }
    }
    
    //Check for parameters on given request
    public void check(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, FormatException {
        //create the parameter getter
        ParameterGetter getter  = getterFarm.create(request);
        //get parameters and convert them to the right type building a map
        boolean some_null = false;
        boolean all_null = true;
        HashMap<String, Object> parms = new HashMap<>();
        for(String mnbn:mustNotBeNull) {
            boolean is_null = false;
            if(typesMapping.get(mnbn) != ParamTypes.FILE) {
                String ps = getter.getParameter(mnbn);
                if(ps==null) {
                    is_null = some_null = true;
                    parms.put(mnbn, null);
                } else {
                    Object p = null;
                    switch(typesMapping.get(mnbn)) {
                        case LONG:
                            p = Long.parseLong(ps);
                            break;
                        case DOUBLE:
                            p = Double.parseDouble(ps);
                            break;
                        case STRING:
                            p = ps;
                            break;
                    }
                    parms.put(mnbn, p);
                }
            }
            else {
                File f = getter.getFile(mnbn);
                parms.put(mnbn, f);
                if(f==null) {
                    is_null = some_null = true;
                }
            }
            all_null = all_null && is_null;
        }
        for(String cbn:canBeNull) {
            if(typesMapping.get(cbn) != ParamTypes.FILE) {
                String ps = getter.getParameter(cbn);
                if(ps==null) {
                    parms.put(cbn, null);
                } else {
                    Object p = null;
                    switch(typesMapping.get(cbn)) {
                        case LONG:
                            p = Long.parseLong(ps);
                            break;
                        case DOUBLE:
                            p = Double.parseDouble(ps);
                            break;
                        case STRING:
                            p = ps;
                            break;
                    }
                    parms.put(cbn, p);
                }
            }
            else {
                File f = getter.getFile(cbn);
                parms.put(cbn, f);
            }
        }
        //dispatch on virtual methods based on selected parameters
        if(all_null) {
            mustNotBeNullAllNull(parms, request, response);
        }
        else if(some_null) {
            mustNotBeNullSomeNull(parms, request, response);
        }
        else {
            mustNotBeNullAllSet(parms, request, response);
        }
    }
    
    //called when there are more then one mustNotBeNull parameters and some of them is null but not all
    protected abstract void mustNotBeNullSomeNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
    //called when all mustNotBeNull parameters are null
    protected abstract void mustNotBeNullAllNull(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
    //called when all mustNotBeNull parameters are not null
    protected abstract void mustNotBeNullAllSet(Map<String, Object> parms, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;   
}
