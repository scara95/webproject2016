package support.parametercheckdispatch;

/**
 * Error in request format or parameter initialization
 */
public class FormatException extends Exception {
}
