package support.parametercheckdispatch.parametergetter.slashstyleurl;


import java.util.UUID;
import support.parametercheckdispatch.parametergetter.ParameterGetterFarm;

/**
 * Unprefixed, single pattern getter
 */
public class SlashStyleParameterGetterFarm extends PrefixSlashStyleParameterGetterFarm {
    public SlashStyleParameterGetterFarm(String name, String pattern, ParameterGetterFarm chain) {
        super(name, UUID.randomUUID().toString(), new String[]{"/"}, pattern, chain);
    }
}
