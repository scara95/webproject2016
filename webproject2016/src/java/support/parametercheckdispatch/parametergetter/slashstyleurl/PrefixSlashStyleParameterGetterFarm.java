
package support.parametercheckdispatch.parametergetter.slashstyleurl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import support.parametercheckdispatch.FormatException;
import support.parametercheckdispatch.parametergetter.slashstyleurl.MultipatternPrefixSlashStyleParameterGetterFarm.NP;
import support.parametercheckdispatch.parametergetter.ParameterGetter;
import support.parametercheckdispatch.parametergetter.ParameterGetterFarm;

/**
 * Single pattern for all prefixes
 */
public class PrefixSlashStyleParameterGetterFarm implements ParameterGetterFarm {
    final MultipatternPrefixSlashStyleParameterGetterFarm getter;
    
    public PrefixSlashStyleParameterGetterFarm(String name, String prefixName, String[] prefixes, String pattern, ParameterGetterFarm chain) {
        List<NP> patterns = new ArrayList<>();
        patterns.add(new NP(name, pattern));
        Map<String, List<NP>> matchers = new HashMap<>();
        for(String prefix:prefixes) {
            matchers.put(prefix, patterns);
        }
        getter = new MultipatternPrefixSlashStyleParameterGetterFarm(prefixName, matchers, null, chain);
    }

    @Override
    public ParameterGetter create(HttpServletRequest request) throws IOException, FormatException {
        return getter.create(request);
    }
    
}
