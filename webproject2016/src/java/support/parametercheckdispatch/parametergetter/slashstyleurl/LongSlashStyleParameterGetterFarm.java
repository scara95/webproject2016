package support.parametercheckdispatch.parametergetter.slashstyleurl;

import support.parametercheckdispatch.parametergetter.ParameterGetterFarm;

/**
 * Append a [1..] long to the url as parameter
 */
public class LongSlashStyleParameterGetterFarm extends SlashStyleParameterGetterFarm {
    
    public LongSlashStyleParameterGetterFarm(String name, ParameterGetterFarm chain) {
        super(name, "([1-9][0-9]*)", chain);
    }
    
}
