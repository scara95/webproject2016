package support.parametercheckdispatch.parametergetter.slashstyleurl;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import support.parametercheckdispatch.FormatException;
import support.parametercheckdispatch.parametergetter.ParameterGetter;
import support.parametercheckdispatch.parametergetter.ParameterGetterFarm;

/**
 * Build a parameter getter that can match multiple prefix and patterns after
 * the base url with optional fallback on other methods
 */
public class MultipatternPrefixSlashStyleParameterGetterFarm implements ParameterGetterFarm {

    @Override
    public ParameterGetter create(HttpServletRequest request) throws IOException, FormatException {
        return new MultipatternPrefixRestParameterGetter(request);
    }
    
    /**
     * Represent a named parameter with the pattern to match it
     */
    public static final class NP implements Serializable {
        private String name;
        private String pattern;
        private Pattern pt;
        
        public NP() {
            
        }
        
        public NP(String name, String pattern) {
            this.name = name;
            this.pattern = pattern;
            this.pt = Pattern.compile(pattern);
        }

        public final String getPattern() {
            return pattern;
        }

        public final void setPattern(String pattern) {
            this.pattern = pattern;
        }

        public final String getName() {
            return name;
        }

        public final void setName(String name) {
            this.name = name;
        }
        
        public final Matcher getMatcher(String toMatch) {
            return pt.matcher(toMatch);
        }
        
    }  
    
    private final ParameterGetterFarm chainFarm;
    private final List<String> prefixes;
    private final Map<String, List<NP>> matchers;
    private final String prefixName;
    private final String over;
    
    /**
     * Contructor
     * @param prefixName parameter name to be used for prefix
     * @param matchers map of prefix-ordered list of matchers
     * @param over name of parameter for the over, unmatched parts
     * @param chainFarm Farm for chain in case parameter is not part of the url
     */
    public MultipatternPrefixSlashStyleParameterGetterFarm(String prefixName, Map<String, List<NP>> matchers, String over, ParameterGetterFarm chainFarm) {
        this.chainFarm = chainFarm;
        this.matchers = matchers;
        this.prefixes = new ArrayList<>();
        this.prefixes.addAll(this.matchers.keySet());
        this.prefixName = prefixName;
        this.over = over;
        //Sort in descending order: start from longest prefix
        Collections.sort(prefixes, Collections.reverseOrder());        
    }

    public class MultipatternPrefixRestParameterGetter implements ParameterGetter {
        private final Map<String, String> parameters;
        private final ParameterGetter chain;
        private String prefix;
        
        public MultipatternPrefixRestParameterGetter(HttpServletRequest request) throws IOException, FormatException {
            this.parameters = new HashMap<>();
            if(chainFarm != null) {
                this.chain = chainFarm.create(request);
            }
            else {
                this.chain = null;
            }
            String query = request.getPathInfo();
            if(query == null) {
                query = "/";
            }
            else {
                query = query.replaceFirst("/*$", "");
            }
            boolean found = false;
            for(String prefix:prefixes) {
                if(query.startsWith(prefix)) {
                    found = true;
                    this.prefix = prefix;
                    break;
                }
            }
            if(!found) throw new FormatException();
            this.parameters.clear();
            query = query.substring(this.prefix.length()).replaceFirst("^/*", "");
            if(query.equals("")) {
                return;
            }
            String[] toMatch = query.split("/");
            List<NP> matchers1 = matchers.get(this.prefix);
            int toCheck = Math.min(toMatch.length, matchers1.size());
            for(int i = 0; i < toCheck; ++i) {
                Matcher m = matchers1.get(i).getMatcher(toMatch[i]);
                if(m.matches()) {
                    this.parameters.put(matchers1.get(i).getName(), m.group(1));
                }
                else {
                    throw new FormatException();
                }
            }
            if(toMatch.length > matchers1.size()) {
                if(over != null) {
                    this.parameters.put(over, String.join("/", Arrays.copyOfRange(toMatch, toCheck, toMatch.length)));
                }
                else {
                    throw new FormatException();
                }
            }
        }

        @Override
        public String getParameter(String name) throws FormatException {
            if(prefixName.equals(name)) {
                return this.prefix;
            }
            else if(this.parameters.containsKey(name)) {
                return this.parameters.get(name);
            }
            else if(chain != null) {
                return chain.getParameter(name);
            }
            else {
                return null;
            }
        }

        @Override
        public File getFile(String name) throws IOException, FormatException {
            if(chain != null) {
                return chain.getFile(name);
            } else {
                return null;
            }
        }
    }
    
}
