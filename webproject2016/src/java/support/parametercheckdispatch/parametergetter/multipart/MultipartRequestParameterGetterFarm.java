/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package support.parametercheckdispatch.parametergetter.multipart;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;
import java.io.File;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import support.parametercheckdispatch.FormatException;
import support.parametercheckdispatch.parametergetter.ParameterGetter;
import support.parametercheckdispatch.parametergetter.ParameterGetterFarm;

/**
 * Unfiltered mutipart parameter getter
 */
public class MultipartRequestParameterGetterFarm implements ParameterGetterFarm {
    MultipartRequest multi;
    String path;

    public MultipartRequestParameterGetterFarm(String path) {
        this.path = path;
    }

    @Override
    public ParameterGetter create(HttpServletRequest request) throws IOException, FormatException {
        return new MultipartRequestParameterGetter(request);
    }
        
        
        public class MultipartRequestParameterGetter implements ParameterGetter {
            public MultipartRequestParameterGetter(HttpServletRequest request) throws IOException {
                multi
                        = new MultipartRequest(request, path, 10 * 1024 * 1024,
                                "ISO-8859-1", new DefaultFileRenamePolicy());
            }

            @Override
            public String getParameter(String name) {
                return multi.getParameter(name);
            }

            @Override
            public File getFile(String name) {
                return multi.getFile(name);
            }
        }
    }
