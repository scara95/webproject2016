package support.parametercheckdispatch.parametergetter.multipart;

import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.FileRenamePolicy;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.ParamPart;
import com.oreilly.servlet.multipart.Part;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import support.parametercheckdispatch.FormatException;
import support.parametercheckdispatch.parametergetter.ParameterGetter;
import support.parametercheckdispatch.parametergetter.ParameterGetterFarm;

/**
 * Parameter getter for multipart form requests with data filtering
 * hard limit setted on 2MB
 */
public abstract class MultipartFilteredRequestParameterGetterFarm implements ParameterGetterFarm {
    final private String path;
    final private FileRenamePolicy rename_policy;

    /**
     * Constructor
     * @param path path to save uploaded files
     * @param rename_policy rename policy for files
     */
    public MultipartFilteredRequestParameterGetterFarm(String path, FileRenamePolicy rename_policy) {
        this.path = path;
        this.rename_policy = rename_policy;
    }
    
    @Override
    public ParameterGetter create(HttpServletRequest request) throws IOException, FormatException {
        return new MultipartFilteredRequestParameterGetter(request);
    }

    /**
     * Mapping function to be called on file before saving, can be used process
     * file on upload
     * @param is
     * @param os
     * @throws IOException
     * @throws FormatException 
     */
    protected abstract void filter(InputStream is, OutputStream os) throws IOException, FormatException;
    
    /**
     * Multipart parameter getter implementation
     */
    public class MultipartFilteredRequestParameterGetter implements ParameterGetter {
        final private Map<String, String> params;
        final private Map<String, File> files;
        
        /**
         * push style constructor
         * @param request
         * @throws IOException
         * @throws FormatException 
         */
        public MultipartFilteredRequestParameterGetter(HttpServletRequest request) throws IOException, FormatException {
            params = new HashMap<>();
            files = new HashMap<>();
            MultipartParser p = new MultipartParser(request, 2*1024*1024);
            Part next;
            //get parts and process them
            while((next = p.readNextPart()) != null) {
                //if it's a file preprocess it and save it for later use
                if(next.isFile()) {
                    FilePart fp = (FilePart)next;
                    String fn = fp.getFileName();
                    if(fn!=null) {
                        File hd = new File(path, fn);
                        hd = rename_policy.rename(hd);
                        OutputStream os = new BufferedOutputStream(new FileOutputStream(hd));
                        filter(fp.getInputStream(), os);
                        files.put(fp.getName(), hd);
                    }
                    else {
                        files.put(fp.getName(), null);
                    }
                }
                else {
                    ParamPart pp = (ParamPart) next;
                    String value = pp.getStringValue();
                    if(!value.equals("")) {
                        params.put(pp.getName(), value);
                    }
                    else {
                        params.put(pp.getName(), null);
                    }
                }
            }
        }
        @Override
        public String getParameter(String name) {
            return params.get(name);
        }

        @Override
        public File getFile(String name) {
            return files.get(name);
        }
    }

}
