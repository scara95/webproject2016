package support.parametercheckdispatch.parametergetter.multipart;

import com.oreilly.servlet.multipart.FileRenamePolicy;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.imageio.ImageIO;
import support.parametercheckdispatch.FormatException;

/**
 * Multipart getter farm with jpg automatic conversion on upload
 */
public class MultipartJPGConverterParameterGetterFarm extends MultipartFilteredRequestParameterGetterFarm {

    public MultipartJPGConverterParameterGetterFarm(String path, FileRenamePolicy rename_policy) {
        super(path, rename_policy);
    }


    @Override
    protected void filter(InputStream inputStream, OutputStream os) throws IOException, FormatException {
        BufferedImage im = ImageIO.read(inputStream);
        if(!ImageIO.write(im, "jpg", os)) {
            throw new FormatException();
        }   
    }
    
}
