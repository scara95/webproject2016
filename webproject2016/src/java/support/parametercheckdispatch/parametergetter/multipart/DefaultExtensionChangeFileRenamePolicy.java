package support.parametercheckdispatch.parametergetter.multipart;

import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;
import java.io.File;

/**
 * Change filename to the given extension and take care of getting an unique name
 */
public class DefaultExtensionChangeFileRenamePolicy extends DefaultFileRenamePolicy {
    String ext;
    public DefaultExtensionChangeFileRenamePolicy(String ext) {
        this.ext = ext;
    }
    
    @Override
    public File rename(File file) {
        String fn = file.getName();
        fn = fn.substring(0, fn.lastIndexOf("."))+"."+ext;
        return super.rename(new File(file.getParent(), fn));
    }
    
}
