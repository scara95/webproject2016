package support.parametercheckdispatch.parametergetter;

import java.io.File;
import java.io.IOException;
import support.parametercheckdispatch.FormatException;

/**
 * Parameter getter interface
 */
public interface ParameterGetter {
    /**
     * Get the string rappresentation for the given parameter name
     * @param name parameter name
     * @return parameter value
     * @throws FormatException 
     */
    String getParameter(String name) throws FormatException;
    
    /**
     * Get file object for the given parameter.
     * Can throw an unsupported exception
     * @param name parameter name
     * @return file object value
     * @throws IOException
     * @throws FormatException 
     */
    File getFile(String name) throws IOException, FormatException;
}
