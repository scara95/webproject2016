package support.parametercheckdispatch.parametergetter;

import java.io.File;
import javax.servlet.http.HttpServletRequest;

/**
 * Parameter getter farm for classic servlet get/post parameters
 */
public class HttpRequestParameterGetterFarm implements ParameterGetter, ParameterGetterFarm {
    HttpServletRequest request;
    
    public HttpRequestParameterGetterFarm() {
    }
    
    @Override
    public String getParameter(String name) {
        return request.getParameter(name);
    }

    @Override
    public File getFile(String name) {
        throw new UnsupportedOperationException("Not supported.");
    }

    private HttpRequestParameterGetterFarm(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public ParameterGetter create(HttpServletRequest request) {
        return new HttpRequestParameterGetterFarm(request);
    }
}
