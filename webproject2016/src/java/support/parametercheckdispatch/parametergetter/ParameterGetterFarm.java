package support.parametercheckdispatch.parametergetter;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import support.parametercheckdispatch.FormatException;

/**
 * ParameterGetterFarm interface, get a ParameterGetter from given request
 */
public interface ParameterGetterFarm {
    /**
     * Contruct a ParameterGetter from the given request
     * @param request
     * @return ParameterGetter object
     * @throws IOException
     * @throws FormatException 
     */
    ParameterGetter create(HttpServletRequest request)throws IOException, FormatException;
}
