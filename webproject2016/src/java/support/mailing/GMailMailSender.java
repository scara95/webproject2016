package support.mailing;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Properties;
import java.util.ResourceBundle;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Synchronous GMail mailing service
 */
public abstract class GMailMailSender {

    final private String USER;
    final private String PASSWORD;
    
    /**
     * Constructor
     * @param email email address
     * @param password account password
     */
    public GMailMailSender(String email, String password) {
        USER = email;
        PASSWORD = password;
    }
    
    /**
     * Compose a message
     * @param bundle translation bundle
     * @param subject mail subject
     * @param body mail body
     * @param data optional extra data
     */
    protected abstract void compose(ResourceBundle bundle, PrintWriter subject, PrintWriter body, Object data);
    
    /**
     * Send an email to the given addres
     * @param bundle translation bundle
     * @param to address of reciver
     * @param data optional data to be passed on to compose
     * @throws MessagingException
     * @throws UnsupportedEncodingException 
     */
    final public void send(ResourceBundle bundle, String to, Object data) throws MessagingException, UnsupportedEncodingException {

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(USER, PASSWORD);
            }
        });

        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(USER));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
        ByteArrayOutputStream subject = new ByteArrayOutputStream();
        ByteArrayOutputStream body = new ByteArrayOutputStream();
        PrintWriter  spw = new PrintWriter(subject);
        PrintWriter  bpw = new PrintWriter(body);
        this.compose(bundle, spw, bpw, data);
        spw.flush();
        bpw.flush();
        message.setSubject(subject.toString());
        message.setText(body.toString());
        Transport.send(message);
    }
}
