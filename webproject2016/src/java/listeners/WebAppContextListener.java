package listeners;

import dbmanager.DBManager;
import java.sql.SQLException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import support.authenticators.AuthenticatorFactory;
import support.authenticators.SaltedSHA512;

/**
 * Define globals needed and initialize the auth system
 */
public class WebAppContextListener implements ServletContextListener {
    Timer timer;
    DBManager manager;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        String dburl = sce.getServletContext().getInitParameter("dburl");
        String defaultAuthenticator = sce.getServletContext().getInitParameter("defaultAuthenticator");
        String salt = sce.getServletContext().getInitParameter("salt");
        //register supported authenticators
        String[] auths = sce.getServletContext().getInitParameter("supportedAuthenticators").split(";");
        SaltedSHA512.setSalt(salt.toCharArray());
        for(String auth:auths) {
            AuthenticatorFactory.register(auth);
        }
        
        try {
            //initialize dbmanager
            manager = new DBManager(dburl, defaultAuthenticator);

            //set dbmanager attribute
            sce.getServletContext().setAttribute("dbmanager", manager);
            //set up registration cleanup task to clean unfinished registration
            timer = new Timer(true);
            timer.scheduleAtFixedRate(new CleanDBTask(manager), 0, 30*60);

        } catch (SQLException ex) {

            Logger.getLogger(getClass().getName()).severe(ex.toString());

            throw new RuntimeException(ex);

        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        //cleanup manager and cleanup task
        timer.cancel();
        manager.shutdown();
    }

    private static class CleanDBTask extends TimerTask {
        
        DBManager manager;
        
        public CleanDBTask(DBManager manager) {
            this.manager = manager;
        }

        @Override
        public void run() {
            try {
                manager.users.cleanRegistrations();
            } catch (SQLException ex) {
                Logger.getLogger(WebAppContextListener.class.getName()).log(Level.WARNING, null, ex);
            }
        }
    }
}
