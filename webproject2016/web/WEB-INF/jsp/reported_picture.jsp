<%--
    Document   : reportedPicture
    Created on : 14-nov-2016, 17.40.13
    Author     : Enrico
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>

    <head>
        <%@include file="head.jsp" %>
        <title><fmt:message key="titleReportedPicture" /> - <fmt:message key="siteName" /></title>
    </head>
    <body>
        <%@include file="menu.jsp" %>
        <c:set var="owner" value="${requestScope.restaurant.getOwner()}" />

        <div class="container">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <h2>
                        <c:out value="${requestScope.restaurant.getName()}" />
                        <small>
                            <fmt:message key="msgOwnedBy" /> <a href="<%=request.getContextPath()%>/profile/${owner.getId()}"><c:out value="${owner.getName()} ${owner.getSurname()}" /></a>
                        </small>
                    </h2>
                    <p>
                        <i class="material-icons rating" rating="${requestScope.restaurant.getRate()}" data-content="star star star star star" data-toggle="tooltip" data-placement="right" data-original-title="">star star star star star</i>
                    </p>
                    <h4>
                        <fmt:message key="msgPrice" /> <i class="material-icons rating-money" rating="${requestScope.restaurant.getPrice()}" data-content="attach_money attach_money attach_money" data-toggle="tooltip" data-placement="right" data-original-title="">attach_money attach_money attach_money</i>
                    </h4>
                    <p>
                        <c:out value="${requestScope.restaurant.getDescr()}" />
                    </p>
                    <c:if test="${requestScope.restaurant.getSite()!=null}">
                        <p>
                            <a href="${requestScope.restaurant.getSite()}" target="_blank">${requestScope.restaurant.getSite()}</a>
                        </p>
                    </c:if>
                    <div class="btn-group">
                        <c:forEach var="type" items="${requestScope.restaurant.getTypes()}">
                            <a class="btn btn-raised btn-default -fw-btn-fake"><fmt:message key="${type}" /></a>
                        </c:forEach>
                    </div>
                    <br>
                    <div>
                        <div id="map" class="col-md-8 col-sm-12" lat="${requestScope.restaurant.getLat()}" lon="${requestScope.restaurant.getLon()}"></div>
                        <div class="col-md-4 col-sm-12">
                            <div class="col-xs-6 col-md-12" style="padding:0;">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            <i class="material-icons" style="font-size:15px;">schedule</i> <fmt:message key="timetable" />
                                        </h3>
                                    </div>
                                    <div class="panel-body" id="orari"></div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-md-12" style="text-align:center;" id="QRcode">
                                <img src="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <img class="center-block" id="reported-image" src="<%=request.getContextPath()%>/static/picture/${requestScope.img.getId()}.jpg"/>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-xs-12 col-sm-offset-2" id="pulsantoloni">
                            <button class="btn btn-raised" action="delete"><fmt:message key="btnDelete" /></button>
                            <button class="btn btn-raised btn-primary pull-right" action="keep"><fmt:message key="btnAccept" /></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="<%=request.getContextPath() + "/js/starsAndDollarsPlugin.js"%>"></script>
        <script>
            $(window).on("load", function () {
                initStarsAndDollarsPlugin({
                    strings: {
                        msgRating: "<fmt:message key="msgRating" />",
                        msgReputation: "<fmt:message key="msgReputation" />"
                    }
                });
            });
        </script>
        <script src="<%=request.getContextPath() + "/js/acceptReportedPicturePlugin.js"%>"></script>
        <script>
            $(window).on("load", function() {
                initAcceptReportedPicturePlugin({
                    baseURL: "<%=request.getContextPath()%>",
                    imgId: ${requestScope.img.getId()},
                    strings: {
                        alertSuccess: "<fmt:message key="alertSuccess" />",
                        alertError: "<fmt:message key="alertError" />",
                        msgImageRejected: "<fmt:message key="msgImageRejected" />",
                        msgImageNotRejected: "<fmt:message key="msgImageNotRejected" />",
                        msgImageAccepted: "<fmt:message key="msgImageAccepted" />",
                        msgImageNotAccepted: "<fmt:message key="msgImageNotAccepted" />"
                    }
                });
            });
        </script>
        <script src="<%=request.getContextPath() + "/js/orariDisplayPlugin.js"%>"></script>
        <script>
            var orari;
            $(window).on("load", function () {
                orari = initOrariDisplayPlugin({
                    selector: "#orari", 
                    orari: ${requestScope.restaurant.getTimetable()},
                    strings: {
                        days: ["<fmt:message key="day0" />", "<fmt:message key="day1" />", "<fmt:message key="day2" />", 
                    "<fmt:message key="day3" />", "<fmt:message key="day4" />", "<fmt:message key="day5" />", "<fmt:message key="day6" />"]
                    }
                });
            });
        </script>
        <script src="<%=request.getContextPath() + "/js/mapQRDisplayerPlugin.js"%>"></script>
        <script>
            $(window).on("load", function () {
                initMapQRDisplayerPlugin({
                    baseURL: "<%=request.getContextPath()%>",
                    lat: "<c:out value="${requestScope.restaurant.getLat()}" />",
                    lng: "<c:out value="${requestScope.restaurant.getLon()}" />",
                    restaurantName: "<c:out value="${requestScope.restaurant.getName()}" />",
                    orari: orari,
                    strings: {
                        msgQRName: "<fmt:message key="msgQRName" />",
                        msgQRAddress: "<fmt:message key="msgQRAddress" />",
                        msgQRTimetable: "<fmt:message key="msgQRTimetable" />"
                    }
                });
            });
        </script>
    </body>
</html>
