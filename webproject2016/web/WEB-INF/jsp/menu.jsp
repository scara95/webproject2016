<%--
    Document   : menu
    Created on : 9-nov-2016, 22.54.11
    Author     : nicol&meis
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <!-- responsive -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapserino" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/webproject2016"><img src="<%=request.getContextPath() + "/img/reFood.png"%>"></a>
        </div>
        <!-- /responsive button -->
        <div id="collapserino" class="navbar-collapse collapse navbar-responsive-collapse">
            <ul class="nav navbar-nav navbar-right">
                <c:choose>
                    <c:when test="${sessionScope.user != null}">
                        <c:if test="${requestScope.notifications}">
                            <li class="dropdown">
                                <a href="" data-target="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <div class="notifica" id="notifications-number"></div>
                                    <i class="material-icons" id="notifications-icon">notifications_none</i>
                                    <b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu dd-notifiche" id="notifications">
                                    <li><a href="<%=request.getContextPath()%>/all_notifications"><fmt:message key="allNotifications" /></a></li>
                                </ul>
                            </li>
                        </c:if>
                        <li class="dropdown">
                            <a href="" data-target="#" class="dropdown-toggle" data-toggle="dropdown">
                                <c:out value="${sessionScope.user.getName()} ${sessionScope.user.getSurname()}"/>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<%=request.getContextPath()%>/profile/${sessionScope.user.getId()}"><fmt:message key="profile" /></a></li>
                                <li><a href="<%=request.getContextPath() + "/add_restaurant"%>"><fmt:message key="addRestaurant" /></a></li>
                                    <c:set var="rs" value="${requestScope.ownedRestaurants}" />
                                    <c:if test="${not empty rs}">
                                    <li class="divider"></li>
                                        <c:forEach var="restaurant" items="${rs}">
                                        <li><a href="<%=request.getContextPath()%>/restaurant/${restaurant.getId()}">${restaurant.getName()}</a></li>
                                        </c:forEach>
                                    </c:if>
                                <li class="divider"></li>
                                <li><a href="<%=request.getContextPath() + "/logout"%>"><fmt:message key="exit" /></a></li>
                            </ul>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li><a href="<%=request.getContextPath() + "/login"%>"><fmt:message key="login" /></a></li>
                        <li><a href="<%=request.getContextPath() + "/register"%>"><fmt:message key="signin" /></a></li>
                    </c:otherwise>
                </c:choose>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <c:choose>
                            <c:when test="${requestScope.lang == 'en'}">
                                <img src="<%=request.getContextPath() + "/img/flag_eng.png"%>" style="height:17px;">
                            </c:when>
                            <c:otherwise>
                                <img src="<%=request.getContextPath() + "/img/flag_ita.jpg"%>" style="height:17px;">
                            </c:otherwise>
                        </c:choose>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" style="min-width:inherit;">
                        <li><a href="<%=request.getContextPath() + "/language?lang=it_IT"%>"><img src="<%=request.getContextPath() + "/img/flag_ita.jpg"%>" style="height:17px;"></a></li>
                        <li><a href="<%=request.getContextPath() + "/language?lang=en"%>"><img src="<%=request.getContextPath() + "/img/flag_eng.png"%>" style="height:17px;"></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

<c:if test="${requestScope.notifications}">
    <script>
        function renderNotifications(ns) {
        if (ns.length > 0) {
        $("#notifications-number").text(ns.length);
                $("#notifications-icon").text("notifications");
        } else {
        $("#notifications-number").empty();
                $("#notifications-icon").text("notifications_none");
        }
        var nsc = $("#notifications");
                nsc.empty();
                $.each(ns, function (index, n) {
                var title = $("<h4></h4>");
                        title.text(n.name);
                        var message = $("<p></p>");
                        message.text(n.message);
                        var header = $("<a></a>");
                        header.append(title);
                        header.append(message);
                        var body = $("<div></div>");
                        $.each(n.controls, function (index, c) {
                        var button = $("<button></button>");
                                button.addClass("btn");
                                button.addClass("btn-raised");
                                if (c.primary) {
                        button.addClass("btn-primary");
                        }
                        button.text(c.name);
                                if (c.type === "AJAX") {
                        button.click(function () {
                        var requestURL = c.URL;
                                var requestParameters = c.parameters;
                                var callbacks = {};
                                var messages = c.AJAXMessages;
                                $.each(messages, function (i, message) {
                                callbacks[message.status] = function () {
                                if (message.successful) {
                                $.alert("success", "<fmt:message key="alertSuccess" />", message.message);
                                } else {
                                $.alert("danger", "<fmt:message key="alertError" />", message.message);
                                }
                                $.getJSON("<%=request.getContextPath()%>/notifications", {}, renderNotifications);
                                };
                                });
                                $.ajax({
                                method: "POST",
                                        url: "<%=request.getContextPath()%>" + requestURL,
                                        data: requestParameters,
                                        statusCode: callbacks
                                });
                        });
                        } else if (c.type === "REDIRECT") {
                        button.click(function () {
                        var requestURL = "<%=request.getContextPath()%>" + c.URL;
                                var requestParameters = c.parameters;
                                $.redirectPost(requestURL, requestParameters);
                        });
                        }
                        body.append(button);
                        });
                        var notification = $("<li></li>");
                        notification.append(header);
                        notification.append(body);
                        nsc.append(notification);
                });
                if (ns.length > 0) {
        nsc.append($("<li class=\"divider\"></li>"));
        }
        nsc.append($("<li><a href=\"<%=request.getContextPath()%>/all_notifications\"><fmt:message key="allNotifications" /></a></li>"));
        }

        (function worker() {
        $.getJSON("<%=request.getContextPath()%>/notifications", {}, renderNotifications);
                setTimeout(worker, 15000);
        })();
    </script>
</c:if>