<%--
    Document   : restaurant
    Created on : 29-nov-2016, 22.03.13
    Author     : Enrico
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>

    <head>
        <%@include file="head.jsp" %>
        <title><fmt:message key="titlePrivacy"/> - <fmt:message key="siteName" /></title>
    </head>

    <body>
        <%@include file="menu.jsp" %>
        <div class="container">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="panel-title"><fmt:message key="titlePrivacy" /></div>
                </div>
                <div class="panel-body">
                    <p><fmt:message key="privacy1" /></p>
                    <p><fmt:message key="privacy2" /></p>
                    <p><fmt:message key="privacy3" /></p>
                    <h5><fmt:message key="privacyTitle1" /></h5>
                    <p><fmt:message key="privacy4" /></p>
                    <p><fmt:message key="privacy5" /></p>
                    <ul>
                        <li><a href="#coll"><fmt:message key="privacyLink1" /></a></li>
                        <li><a href="#use"><fmt:message key="privacyLink2" /></a></li>
                        <li><a href="#shar"><fmt:message key="privacyLink3" /></a></li>
                        <li><a href="#dura"><fmt:message key="privacyLink4" /></a></li>
                        <li><a href="#choi"><fmt:message key="privacyLink5" /></a></li>
                        <li><a href="#prot"><fmt:message key="privacyLink6" /></a></li>
                        <li><a href="#kids"><fmt:message key="privacyLink7" /></a></li>
                        <li><a href="#exte"><fmt:message key="privacyLink8" /></a></li>
                        <li><a href="#visit"><fmt:message key="privacyLink9" /></a></li>
                        <li><a href="#chgs"><fmt:message key="privacyLink10" /></a></li>
                        <li><a href="#cont"><fmt:message key="privacyLink11" /></a></li>
                    </ul>
                    <h2 id="coll"><fmt:message key="privacyLink1" /></h2>
                    <h4><fmt:message key="privacyTitle2" /></h4>
                    <p><fmt:message key="privacy6" /></p>
                    <p><fmt:message key="privacy7" /></p>
                    <p><fmt:message key="privacy8" /></p>
                    <p><fmt:message key="privacy9" /></p>
                    <h4><fmt:message key="privacyTitle3" /></h4>
                    <p><fmt:message key="privacy10" /></p>
                    <h4><fmt:message key="privacyTitle4" /></h4>
                    <p><fmt:message key="privacy11" /></p>
                    <h4><fmt:message key="privacyTitle5" /></h4>
                    <p><fmt:message key="privacy12" /></p>
                    <h4><fmt:message key="privacyTitle6" /></h4>
                    <p><fmt:message key="privacy13" /></p>
                    <h4><fmt:message key="privacyTitle7" /></h4>
                    <p><fmt:message key="privacy14" /></p>
                    <p><fmt:message key="privacy15" /></p>
                    <h2 id="use"><fmt:message key="privacyLink2" /></h2>
                    <h4><fmt:message key="privacyTitle8" /></h4>
                    <p><fmt:message key="privacy16" /></p>
                    <ul>
                        <li><fmt:message key="privacy17" /></li>
                        <li><fmt:message key="privacy18" /></li>
                        <li><fmt:message key="privacy19" /></li>
                        <li><fmt:message key="privacy20" /></li>
                        <li><fmt:message key="privacy21" /></li>
                        <li><fmt:message key="privacy22" /></li>
                        <li><fmt:message key="privacy23" /></li>
                        <li><fmt:message key="privacy24" /></li>
                        <li><fmt:message key="privacy25" /></li>
                        <li><fmt:message key="privacy26" /></li>
                    </ul>
                    <p><fmt:message key="privacy27" /></p>
                    <h5><fmt:message key="privacyTitle9" /></h5>
                    <p><fmt:message key="privacy28" /></p>
                    <h2 id="shar"><fmt:message key="privacyLink3" /></h2>
                    <p><fmt:message key="privacy29" /></p>
                    <h2 id="dura"><fmt:message key="privacyLink4" /></h2>
                    <p><fmt:message key="privacy30" /></p>
                    <p><fmt:message key="privacy31" /></p>
                    <h2 id="choi"><fmt:message key="privacyLink5" /></h2>
                    <ul>
                        <li><fmt:message key="privacy32" /></li>
                        <li><fmt:message key="privacy33" /></li>
                        <li><fmt:message key="privacy34" /></li>
                    </ul>
                    <h2 id="prot"><fmt:message key="privacyLink6" /></h2>
                    <p><fmt:message key="privacy35" /></p>
                    <h2 id="kids"><fmt:message key="privacyLink7" /></h2>
                    <p><fmt:message key="privacy36" /></p>
                    <h2 id="exte"><fmt:message key="privacyLink8" /></h2>
                    <p><fmt:message key="privacy37" /></p>
                    <h2 id="visit"><fmt:message key="privacyLink9" /></h2>
                    <p><fmt:message key="privacy38" /></p>
                    <p><fmt:message key="privacy39" /></p>
                    <h2 id="chgs"><fmt:message key="privacyLink10" /></h2>
                    <p><fmt:message key="privacy40" /></p>
                    <h2 id="cont"><fmt:message key="privacyLink11" /></h2>
                    <p><fmt:message key="privacy41" /></p>
                    <p><i>FazioneWaifus</i></p>
                    <p><i>Via Ancilla Ora Marighetto, 102</i></p>
                    <p><i>Trento</i></p>
                    <p><i>Email: <a href="mailto:fazionewaifus@gmail.com">fazionewaifus@gmail.com</a></i></p>
                    <p>&copy;2016 FazioneWaifus LLC. <fmt:message key="allRightsReserved" /></p>
                    <p><fmt:message key="privacy42" /></p>
                </div>
            </div>
        </div>

    </body>
</html>