<%--
    Document   : restaurant
    Created on : 14-nov-2016, 17.40.13
    Author     : Enrico
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>

    <head>
        <%@include file="head.jsp" %>
        <title><fmt:message key="titleRestaurant" /> - <fmt:message key="siteName" /></title>
    </head>
    <body>
        <%@include file="menu.jsp" %>
        <c:set var="logged" value="${sessionScope.user!=null}"></c:set>
        <c:set var="isOwner" value="${logged && (sessionScope.user.getId() == requestScope.restaurant.getOwner().getId()) && (requestScope.restaurant.getOwnerapprover() != null)}" />
        <c:set var="owner" value="${requestScope.restaurant.getOwner()}" />

        <div class="container">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <c:if test="${isOwner}">
                        <div class="modal fade" id="editRestaurant" tabindex="-1" role="dialog" aria-labelledby="editRestaurantTitle">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form class="form-horizontal" method="POST" action="<%=request.getContextPath()%>/modify_restaurant/${requestScope.restaurant.getId()}">
                                        <fieldset>
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="editRestaurantTitle"><fmt:message key="titleModalEditRestaurant" /></h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label class="control-label col-sm-2" for="name"><fmt:message key="fieldName" /></label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" id="name" name="name" value="<c:out value="${requestScope.restaurant.getName()}" />" autofocus required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-2" for="descr"><fmt:message key="fieldDescription" /></label>
                                                    <div class="col-sm-10">
                                                        <textarea class="form-control" id="descr" name="descr" required><c:out value="${requestScope.restaurant.getDescr()}" /></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputSite" class="col-xs-2 control-label"><fmt:message key="fieldWebsite" /></label>

                                                    <div class="col-xs-10">
                                                        <input type="text" name="site" class="form-control" id="inputSite" placeholder="<fmt:message key="fieldName" />" pattern="(https?://)?(\w+\.)+[a-zA-Z/]+" value="<c:out value="${requestScope.restaurant.getSite()}" />">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-xs-2 control-label"><fmt:message key="fieldPrice" /></label>
                                                    <c:set var="price" value="${requestScope.restaurant.getPrice()}" />
                                                    <div class="col-xs-10">
                                                        <div class="radio radio-primary">
                                                            <label>
                                                                <input type="radio" name="price" id="optionsRadios1" value="LOW"<c:if test="${price == 'LOW'}"> checked</c:if>>
                                                                <fmt:message key="fieldPriceLow" />
                                                            </label>
                                                        </div>
                                                        <div class="radio radio-primary">
                                                            <label>
                                                                <input type="radio" name="price" id="optionsRadios2" value="MID"<c:if test="${price == 'MID'}"> checked</c:if>>
                                                                <fmt:message key="fieldPriceMedium" />
                                                            </label>
                                                        </div>
                                                        <div class="radio radio-primary">
                                                            <label>
                                                                <input type="radio" name="price" id="optionsRadios3" value="HIGH"<c:if test="${price == 'HIGH'}"> checked</c:if>>
                                                                <fmt:message key="fieldPriceHigh" />
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-xs-12" id="orariSelector">
                                                        <input type="hidden" name="timetable" value='${requestScope.restaurant.getTimetable()}'>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message key="btnDismiss" /></button>
                                                <button type="submit" class="btn btn-primary"><fmt:message key="btnConfirm" /></button>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="pull-right">
                            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#editRestaurant">
                                <fmt:message key="btnEditRestaurant" />
                            </button>
                        </div>
                    </c:if>
                    <h2>
                        <div class="-fw-circle" data-toggle="tooltip" data-placement="top" data-original-title="<fmt:message key="msgInTheClassificationOf" /> <c:out value="${requestScope.restaurant.getCity()}"></c:out>">
                            <b>#${restaurant.getClassification()}</b>
                        </div>
                        <c:out value="${requestScope.restaurant.getName()}" />
                        <small>
                            <c:choose>
                                <c:when test="${(owner.getId() != null) && (requestScope.restaurant.getOwnerapprover() != null)}">
                                    <fmt:message key="msgOwnedBy" /> <a href="<%=request.getContextPath()%>/profile/${owner.getId()}"><c:out value="${owner.getName()} ${owner.getSurname()}" /></a>
                                </c:when>
                                <c:when test="${owner.getId() != null}">
                                    <fmt:message key="msgClaimedBy" /> <a href="<%=request.getContextPath()%>/profile/${owner.getId()}"><c:out value="${owner.getName()} ${owner.getSurname()}" /></a>
                                </c:when>
                                <c:otherwise>
                                    <c:if test="${logged}">
                                        <fmt:message key="msgReclame" /> <a id="restaurantClaimer" style="cursor:pointer;"><fmt:message key="linkReclame" /></a>
                                    </c:if>
                                </c:otherwise>
                            </c:choose>
                        </small>
                    </h2>
                    <p>
                        <i class="material-icons rating" rating="${requestScope.restaurant.getRate()}" data-content="star star star star star" data-toggle="tooltip" data-placement="right" data-original-title="">star star star star star</i>
                    </p>
                    <h4>
                        <fmt:message key="msgPrice" /> <i class="material-icons rating-money" rating="${requestScope.restaurant.getPrice()}" data-content="attach_money attach_money attach_money">attach_money attach_money attach_money</i>
                    </h4>
                    <p>
                        <c:out value="${requestScope.restaurant.getDescr()}" />
                    </p>
                    <c:if test="${requestScope.restaurant.getSite()!=null}">
                        <p>
                            <a href="${requestScope.restaurant.getSite()}" target="_blank">${requestScope.restaurant.getSite()}</a>
                        </p>
                    </c:if>
                    <div class="btn-group">
                        <c:forEach var="type" items="${requestScope.restaurant.getTypes()}">
                            <a class="btn btn-default btn-raised -fw-btn-fake"><fmt:message key="${type}" /></a>
                        </c:forEach>
                    </div>
                    <br>
                    <div>
                        <div id="map" class="col-md-8 col-sm-12" lat="${requestScope.restaurant.getLat()}" lon="${requestScope.restaurant.getLon()}"></div>
                        <div class="col-md-4 col-sm-12">
                            <div class="col-xs-6 col-md-12" style="padding:0;">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            <i class="material-icons" style="font-size:15px;">schedule</i> <fmt:message key="timetable" />
                                        </h3>
                                    </div>
                                    <div class="panel-body" id="orari"></div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-md-12" style="text-align:center;" id="QRcode">
                                <img src="">
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top:5px;">
                        <div class="col-md-offset-2 col-md-8 gallery">
                            <c:forEach var="img" items="${requestScope.imgs}">
                                <div class="col-sm-4">
                                    <a href="<%=request.getContextPath()%>/static/picture/${img.getId()}.jpg" data-toggle="lightbox" data-gallery="example-gallery">
                                        <div class="img-gallery" src="<%=request.getContextPath()%>/static/picture/${img.getId()}.jpg"></div>
                                    </a>
                                    <c:if test="${isOwner && img.isReportable()}">
                                        <div class="report-img" img="${img.getId()}">
                                            <a class="btn btn-raised btn-xs" action="report"><fmt:message key="btnReport" /></a>
                                            <a class="btn btn-raised btn-primary btn-xs" action="accept"><fmt:message key="btnAccept" /></a>
                                        </div>
                                    </c:if>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                    <c:if test="${logged}">
                        <div class="row">
                            <div class="modal fade" id="newRate" tabindex="-1" role="dialog" aria-labelledby="rateTitle">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <form class="form-horizontal" method="POST" action="<%=request.getContextPath()%>/rate/${requestScope.restaurant.getId()}">
                                            <fieldset>
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="rateTitle"><fmt:message key="titleModalNewRate" /> <c:out value="${sessionScope.user.getName()} ${sessionScope.user.getSurname()}"></c:out></h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="rate-text"><fmt:message key="fieldRateText" /></label>
                                                        <div class="col-sm-10">
                                                            <textarea class="form-control" id="rate-text" name="rate-text" autofocus required></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="rate-value" class="col-sm-2 control-label"><fmt:message key="fieldRate" /></label>
                                                        <div class="col-sm-10">
                                                            <i class="material-icons" id="newRateStars" data-content="star star star star star">
                                                                <a>star</a>
                                                                <a>star</a>
                                                                <a>star</a>
                                                                <a>star</a>
                                                                <a>star</a>
                                                            </i>
                                                            <input type="hidden" name="rate-value" id="newRateStarsInput" value="0">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message key="btnDismiss" /></button>
                                                    <button type="submit" class="btn btn-primary"><fmt:message key="btnConfirm" /></button>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="newImage" tabindex="-1" role="dialog" aria-labelledby="imgTitle">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="<%=request.getContextPath()%>/add_restaurant_image/${requestScope.restaurant.getId()}">
                                            <fieldset>
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="imgTitle"><fmt:message key="titleModalNewImage" /></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="inputFile" class="col-sm-2 control-label"><fmt:message key="fieldImage" /></label>
                                                        <div class="col-sm-10">
                                                            <input type="text" readonly="" class="form-control" placeholder="<fmt:message key="msgImage" />">
                                                            <input type="file" id="inputFile" name="inputFile" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message key="btnDismiss" /></button>
                                                    <button type="submit" class="btn btn-primary"><fmt:message key="btnConfirm" /></button>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-offset-2 col-md-3 col-sm-6">
                                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#newImage">
                                    <fmt:message key="btnNewImage" />
                                </button>
                            </div>
                            <c:if test="${!requestScope.hasRatedToday}">
                                <div class="col-md-offset-2 col-md-3 col-sm-6">
                                    <button type="button" class="btn btn-primary btn-lg -fw-pull-right" data-toggle="modal" data-target="#newRate">
                                        <fmt:message key="btnNewRate" />
                                    </button>
                                </div>
                            </c:if>
                        </div>
                    </c:if>
                    <div class="recensioni row">
                        <div class="col-md-12">
                            <div class="bs-component">
                                <div class="list-group">
                                    <c:forEach var="rate" items="${requestScope.rates}">
                                        <div class="list-group-item">
                                            <div class="row-picture immaginina">
                                                <c:set var="user" value="${rate.getUser()}"></c:set>
                                                <c:choose>
                                                    <c:when test="${user.getAvatar()!=null}">
                                                        <div src="<%=request.getContextPath()%>/static/avatar/${user.getId()}.jpg"></div>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <i class="material-icons">person</i>
                                                    </c:otherwise>
                                                </c:choose>
                                            </div>
                                            <div class="row-content">
                                                <div class="row">
                                                    <div class="col-md-10 col-sm-9">
                                                        <a href="<%=request.getContextPath()%>/profile/${rate.getUser().getId()}">
                                                            <h4 class="list-group-item-heading">
                                                                <c:out value="${rate.getUser().getName()} ${rate.getUser().getSurname()}" />
                                                            </h4>
                                                        </a>
                                                        <p>
                                                            <i class="material-icons rating" rating="${rate.getRate()}" data-content="star star star star star" data-toggle="tooltip" data-placement="right" data-original-title="">star star star star star</i>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-2 col-sm-3">
                                                        <div class="thumbs -fw-pull-right" rate_user="${rate.getUser().getId()}" rate_restaurant="${rate.getRestaurant().getId()}" rate_day="${rate.getDay()}">
                                                            <c:if test="${logged}">
                                                                <c:set var="rated" value="${applicationScope.dbmanager.users.hasRatedRate(sessionScope.user, rate)}"></c:set>
                                                                <c:if test="${rated}">
                                                                    <c:set var="vote" value="${applicationScope.dbmanager.users.getRateRate(sessionScope.user, rate)}"></c:set>
                                                                </c:if>
                                                            </c:if>
                                                            <div>${rate.getDay()}</div>
                                                            <span class="thumb-up-num">${rate.getUp()}</span>
                                                            <i class="material-icons
                                                               <c:choose>
                                                                   <c:when test="${logged && rated && vote}">
                                                                       thumb-up-voted
                                                                   </c:when>
                                                                   <c:when test="${logged}">
                                                                       thumb-up-clickable
                                                                   </c:when>
                                                               </c:choose>
                                                               ">thumb_up</i>
                                                            <i class="material-icons
                                                               <c:choose>
                                                                   <c:when test="${logged && rated && !vote}">
                                                                       thumb-down-voted
                                                                   </c:when>
                                                                   <c:when test="${logged}">
                                                                       thumb-down-clickable
                                                                   </c:when>
                                                               </c:choose>
                                                               ">thumb_down</i>
                                                            <span class="thumb-down-num">${rate.getDown()}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <p class="list-group-item-text"><c:out value="${rate.getReview()}" /></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <c:set var="ownerAnswer" value="${rate.getOwnerAnswer()}"></c:set>
                                            <c:choose>
                                                <c:when test="${ownerAnswer != null && rate.getAnswerApprover() != null}">
                                                    <div class="list-group-item col-xs-offset-1" style="margin-top:10px;">
                                                        <div class="row-picture immaginina">
                                                            <c:choose>
                                                                <c:when test="${owner.getAvatar()!=null}">
                                                                    <div src="<%=request.getContextPath()%>/static/avatar/${owner.getId()}.jpg"></div>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <i class="material-icons">person</i>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </div>
                                                        <div class="row-content">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <a href="<%=request.getContextPath()%>/profile/${owner.getId()}">
                                                                        <h4 class="list-group-item-heading">
                                                                            <c:out value="${owner.getName()} ${owner.getSurname()}" /> (<fmt:message key="msgOwner" />)
                                                                        </h4>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <p class="list-group-item-text"><c:out value="${ownerAnswer}" /></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:when>
                                                <c:when test="${isOwner && ownerAnswer == null}">
                                                    <div class="modal fade" id="newAnswer" tabindex="-1" role="dialog" aria-labelledby="newAnswerTitle">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <form class="form-horizontal">
                                                                    <fieldset>
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                            <h4 class="modal-title" id="newAnswerTitle"><fmt:message key="titleModalNewAnswer" /></h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="form-group">
                                                                                <label for="textArea" class="col-sm-2 control-label"><fmt:message key="fieldAnswer" /></label>
                                                                                <div class="col-sm-10">
                                                                                    <textarea class="form-control" rows="3" id="textArea" autofocus></textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message key="btnDismiss" /></button>
                                                                            <button type="button" id="newAnswerBtn" class="btn btn-primary" rate_d="${rate.getDay()}" rate_r="${requestScope.restaurant.getId()}" rate_u="${rate.getUser().getId()}"><fmt:message key="btnConfirm" /></button>
                                                                        </div>
                                                                    </fieldset>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button type="button" class="btn btn-primary btn-lg -fw-pull-right" data-toggle="modal" data-target="#newAnswer" rate_d="${rate.getDay()}" rate_r="${requestScope.restaurant.getId()}" rate_u="${rate.getUser().getId()}" esterno>
                                                        <fmt:message key="btnNewAnswer" />
                                                    </button>
                                                </c:when>
                                            </c:choose>
                                        </div>
                                        <div class="list-group-separator"></div>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="<%=request.getContextPath() + "/js/starsAndDollarsPlugin.js"%>"></script>
        <script>
            $(window).on("load", function () {
                initStarsAndDollarsPlugin({
                    strings: {
                        msgRating: "<fmt:message key="msgRating" />",
                        msgReputation: "<fmt:message key="msgReputation" />"
                    }
                });
            });
        </script>
        <script src="<%=request.getContextPath() + "/js/restaurantPicturePlugin.js"%>"></script>
        <script src="<%=request.getContextPath() + "/js/propicPlugin.js"%>"></script>
        <script>
            $(window).on("load", function () {
                initPropicPlugin({selector: ".immaginina > div"});
            });
        </script>
        <script src="<%=request.getContextPath() + "/js/centerCenterPlugin.js"%>"></script>
        <script>
            $(window).on("load", function () {
                initCenterCenterPlugin({selector: ".-fw-circle *"});
            });
        </script>
        <script src="<%=request.getContextPath() + "/js/orariDisplayPlugin.js"%>"></script>
        <script>
            var orari;
            $(window).on("load", function () {
                orari = initOrariDisplayPlugin({
                    selector: "#orari",
                    orari: ${requestScope.restaurant.getTimetable()},
                    strings: {
                        days: ["<fmt:message key="day0" />", "<fmt:message key="day1" />", "<fmt:message key="day2" />",
                            "<fmt:message key="day3" />", "<fmt:message key="day4" />", "<fmt:message key="day5" />", "<fmt:message key="day6" />"]
                    }
                });
            });
        </script>
        <script src="<%=request.getContextPath() + "/js/mapQRDisplayerPlugin.js"%>"></script>
        <script>
            $(window).on("load", function () {
                initMapQRDisplayerPlugin({
                    baseURL: "<%=request.getContextPath()%>",
                    lat: "<c:out value="${requestScope.restaurant.getLat()}" />",
                    lng: "<c:out value="${requestScope.restaurant.getLon()}" />",
                    restaurantName: "<c:out value="${requestScope.restaurant.getName()}" />",
                    orari: orari,
                    strings: {
                        msgQRName: "<fmt:message key="msgQRName" />",
                        msgQRAddress: "<fmt:message key="msgQRAddress" />",
                        msgQRTimetable: "<fmt:message key="msgQRTimetable" />"
                    }
                });
            });
        </script>
        <c:if test="${logged}">
            <script src="<%=request.getContextPath() + "/js/newRateStarsPlugin.js"%>"></script>
            <script src="<%=request.getContextPath() + "/js/thumbsPlugin.js"%>"></script>
            <script>
            $(window).on("load", function () {
                initThumbsPlugin({
                    baseURL: "<%=request.getContextPath()%>",
                    strings: {
                        alertError: "<fmt:message key="alertError" />",
                        msgRetry: "<fmt:message key="msgRetry" />",
                        msgGenericError: "<fmt:message key="msgGenericError" />"
                    }
                });
            });
            </script>
            <script src="<%=request.getContextPath() + "/js/restaurantClaimerPlugin.js"%>"></script>
            <script>
            $(window).on("load", function () {
                initRestaurantClaimerPlugin({
                    baseURL: "<%=request.getContextPath()%>",
                    fullname: "<c:out value="${sessionScope.user.getName()} ${sessionScope.user.getSurname()}" />",
                    restaurantId: ${requestScope.restaurant.getId()},
                    userId: ${sessionScope.user.getId()},
                    strings: {
                        alertSuccess: "<fmt:message key="alertSuccess" />",
                        alertError: "<fmt:message key="alertError" />",
                        msgRetry: "<fmt:message key="msgRetry" />",
                        msgGenericError: "<fmt:message key="msgGenericError" />",
                        msgRestaurantClaimed: "<fmt:message key="msgRestaurantClaimed" />",
                        msgRestaurantAlreadyClaimed: "<fmt:message key="msgRestaurantAlreadyClaimed" />",
                        msgClaimedBy: "<fmt:message key="msgClaimedBy" />"
                    }
                });
            });
            </script>
            <script src="<%=request.getContextPath() + "/js/fileSizeCheckPlugin.js"%>"></script>
            <script>
            $(window).on("load", function () {
                initFileSizeCheckPlugin({
                    selector: "#newImage form",
                    strings: {
                        alertError: "<fmt:message key="alertError" />",
                        msgImageNotUploaded: "<fmt:message key="msgImageNotUploaded" />",
                        msgRetry: "<fmt:message key="msgRetry" />"
                    }
                });
            });
            </script>
        </c:if>
        <c:if test="${isOwner}">
            <script src="<%=request.getContextPath() + "/js/reportImgPlugin.js"%>"></script>
            <script>
            $(window).on("load", function () {
                initReportImgPlugin({
                    baseURL: "<%=request.getContextPath()%>",
                    strings: {
                        alertSuccess: "<fmt:message key="alertSuccess" />",
                        alertError: "<fmt:message key="alertError" />",
                        msgRetry: "<fmt:message key="msgRetry" />",
                        msgGenericError: "<fmt:message key="msgGenericError" />",
                        msgImageReported: "<fmt:message key="msgImageReported" />",
                        msgImageAccepted: "<fmt:message key="msgImageAccepted" />"
                    }
                });
            });
            </script>
            <script src="<%=request.getContextPath() + "/js/newAnswerPlugin.js"%>"></script>
            <script>
            $(window).on("load", function () {
                initNewAnswerPlugin({
                    baseURL: "<%=request.getContextPath()%>",
                    strings: {
                        alertSuccess: "<fmt:message key="alertSuccess" />",
                        alertError: "<fmt:message key="alertError" />",
                        msgNewAnswerEmpty: "<fmt:message key="msgNewAnswerEmpty" />",
                        msgNewAnswerSent: "<fmt:message key="msgNewAnswerSent" />",
                        msgGenericError: "<fmt:message key="msgGenericError" />",
                        msgRetry: "<fmt:message key="msgRetry" />"
                    }
                });
            });
            </script>
            <script src="<%=request.getContextPath() + "/js/modelDate.js"%>"></script>
            <script src="<%=request.getContextPath() + "/js/orariInserimentoPlugin.js"%>"></script>
            <script>
            $(window).on("load", function () {
                initOrariInserimentoPlugin({
                    strings: {
                        alertError: "<fmt:message key="alertError" />",
                        msgSameDate: "<fmt:message key="msgSameDate" />",
                        msgDateConflict: "<fmt:message key="msgDateConflict" />",
                        msgRetry: "<fmt:message key="msgRetry" />",
                        msgInsertDate: "<fmt:message key="msgInsertDate" />",
                        msgDayOverflow: "<fmt:message key="msgDayOverflow" />",
                        msgOpensAt: "<fmt:message key="msgOpensAt" />",
                        msgClosesAt: "<fmt:message key="msgClosesAt" />",
                        days: ["<fmt:message key="day0" />", "<fmt:message key="day1" />", "<fmt:message key="day2" />",
                            "<fmt:message key="day3" />", "<fmt:message key="day4" />", "<fmt:message key="day5" />", "<fmt:message key="day6" />"]
                    }
                });
            });
            </script>
            
            <script src="<%=request.getContextPath() + "/js/detectEnterKeyPlugin.js"%>"></script>
            <script>
            $(window).on("load", function () {
                var selectorFields = [];
                var selectorBtn = [];
                $("#orariSelector input[type=time]").each(function (e) {
                    selectorFields.push("#orariSelector [name=" + $(this).attr("name") + "]");
                    selectorBtn.push("#" + $(this).parent().parent().find(".btn-fab").attr("id"));
                });
                initDetectEnterKeyPlugin({
                    selectorFields: selectorFields,
                    selectorBtn: selectorBtn
                });
            });
            </script>
        </c:if>
    </body>
</html>
