<%-- 
    Document   : login
    Created on : 9-nov-2016, 11.21.26
    Author     : nicol
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="support.utility.StateUtility" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="head.jsp" %>
        <title><fmt:message key="titleLogin" /> - <fmt:message key="siteName" /></title>
    </head>
    <body>
        <%@include file="menu.jsp" %>

        <div class="container">
            <div class="modal fade" id="forgotPwd" tabindex="-1" role="dialog" aria-labelledby="forgotPwdTitle">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <form class="form-horizontal">
                            <fieldset>
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="forgotPwdTitle"><fmt:message key="titleModalForgottenPw" /></h4>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="inputEmailForgotten" class="col-sm-2 control-label"><fmt:message key="fieldEmailForgottenPw" /></label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="inputEmailForgotten" name="inputEmailForgotten" placeholder="<fmt:message key="fieldEmail" />" autofocus required>
                                        </div>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message key="btnDismiss" /></button>
                                    <button type="button" id="forgotPwdBtn" class="btn btn-primary"><fmt:message key="btnConfirm" /></button>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="panel-title"><fmt:message key="titleLogin" /></div>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" action="login" method="POST">
                        <fieldset>
                            <c:if test="${requestScope.error == true}">
                                <div class="col-sm-offset-2">
                                    <p class="text-danger"><fmt:message key="msgMismatch" /> <fmt:message key="msgRetry" /></p>
                                </div>
                            </c:if>
                            <div class="form-group <c:if test="${requestScope.error == true}">
                                 has-error
                                </c:if>">
                                <label for="inputEmail" class="col-xs-2 control-label"><fmt:message key="fieldEmail" /></label>

                                <div class="col-xs-10">
                                    <input type="email" name="email" class="form-control" id="inputEmail" placeholder="<fmt:message key="fieldEmail" />" required>
                                </div>
                            </div>
                            <div class="form-group <c:if test="${requestScope.error == true}">
                                 has-error
                                </c:if>">
                                <label for="inputPassword" class="col-xs-2 control-label"><fmt:message key="fieldPassword" /></label>

                                <div class="col-xs-10">
                                    <input type="password" name="password" class="form-control" id="inputPassword" placeholder="<fmt:message key="fieldPassword" />" required>
                                </div>
                            </div>
                            <div class="col-sm-offset-2">
                                <small><a style="cursor:pointer;" data-toggle="modal" data-target="#forgotPwd"><fmt:message key="linkForgottenPassword" /></a></small>
                            </div>
                            <div class="col-sm-offset-2">
                                <small><fmt:message key="linkNotSignedIn" /> <a href="<%=request.getContextPath() + "/register"%>"><fmt:message key="signin" /></a></small>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <div class="pull-right">
                                        <a href="<%=StateUtility.untrack(request)%>" class="btn btn-raised btn-default"><fmt:message key="btnDismiss" /></a> 
                                        <button type="submit" class="btn btn-raised btn-primary"><fmt:message key="btnLogin" /></button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <script src="<%=request.getContextPath() + "/js/forgottenPasswordPlugin.js"%>"></script>
        <script>
            $(window).on("load", function () {
                initForgottenPasswordPlugin({
                    baseURL: "<%=request.getContextPath()%>",
                    strings: {
                        alertSuccess: "<fmt:message key="alertSuccess" />",
                        alertError: "<fmt:message key="alertError" />",
                        msgEmailMissing: "<fmt:message key="msgEmailMissing" />",
                        msgPasswordRecovered: "<fmt:message key="msgPasswordRecovered" />",
                        msgRetry: "<fmt:message key="msgRetry" />",
                        msgMailNotFound: "<fmt:message key="msgMailNotFound" />",
                        magGenericError: "<fmt:message key="magGenericError" />"
                    }
                });
            });
        </script>
        <script src="<%=request.getContextPath() + "/js/detectEnterKeyPlugin.js"%>"></script>
        <script>
            $(window).on("load", function () {
                initDetectEnterKeyPlugin({
                    selectorFields: ["#forgotPwd input"],
                    selectorBtn: ["#forgotPwdBtn"]
                });
            });
        </script>
    </body>
</html>
