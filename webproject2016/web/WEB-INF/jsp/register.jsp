<%--
    Document   : register
    Created on : 9-nov-2016, 22.54.11
    Author     : nicol
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="support.utility.StateUtility" %>
<!doctype html>
<html>

    <head>
        <%@include file="head.jsp" %>
        <title><fmt:message key="titleSignIn" /> - <fmt:message key="siteName" /></title>
    </head>

    <body>
        <%@include file="menu.jsp" %>

        <div class="container">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="panel-title"><fmt:message key="titleSignIn" /></div>
                </div>
                <div class="panel-body">
                    <c:if test="${requestScope.error}">
                        <div class="col-sm-offset-2">
                            <p class="text-danger"><fmt:message key="msgAlreadyIn" /> <fmt:message key="msgRetry" /></p>
                        </div>
                    </c:if>
                    <form class="form-horizontal" id="registerForm" action="register" method="POST">
                        <fieldset>
                            <div class="form-group
                                 <c:if test="${requestScope.error == true}">
                                     has-error
                                 </c:if>
                                 ">
                                <label for="inputEmail" class="col-xs-2 control-label"><fmt:message key="fieldEmail" /></label>

                                <div class="col-xs-10">
                                    <input type="email" name="email" class="form-control" id="inputEmail" placeholder="<fmt:message key="fieldEmail" />" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword1" class="col-xs-2 control-label"><fmt:message key="fieldPassword" /></label>

                                <div class="col-xs-10">
                                    <input type="password" name="password" class="form-control" id="inputPassword1" placeholder="<fmt:message key="fieldPassword" />" required>
                                    <div class="help-block" id="pwdStrength"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword2" class="col-xs-2 control-label"><fmt:message key="fieldPasswordRepeat" /></label>

                                <div class="col-xs-10">
                                    <input type="password" class="form-control" id="inputPassword2" placeholder="<fmt:message key="fieldPassword" />" required>
                                </div>
                            </div>
                            <div class="col-sm-offset-2">
                                <fmt:message key="msgPrivacyAndToS" /> <a href="<%=request.getContextPath() + "/privacy"%>" target="_blank"><fmt:message key="linkPrivacy" /></a> 
                                <fmt:message key="msgAndThe" /> <a href="<%=request.getContextPath() + "/toc"%>" target="_blank"><fmt:message key="linkToS" /></a><br>
                                <small><fmt:message key="linkAlreadySignedIn" /> <a href="<%=request.getContextPath() + "/login"%>"><fmt:message key="linkLogin" /></a></small>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <div class="pull-right">
                                        <a href="<%=StateUtility.untrack(request)%>" class="btn btn-raised btn-default"><fmt:message key="btnDismiss" /></a>
                                        <button type="button" id="registerBtn" class="btn btn-raised btn-primary"><fmt:message key="btnSignIn" /></button>
                                        <button type="submit" id="registerSubmit" style="display:none"></button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <script src="<%=request.getContextPath() + "/js/passwordCheckPlugin.js"%>"></script>
        <script>
            $(window).on("load", function () {
                initPasswordCheckPlugin({
                    strings: {
                        msgPasswordCheckTooWeak: "<fmt:message key="msgPasswordCheckTooWeak" />",
                        msgPasswordCheckWeak: "<fmt:message key="msgPasswordCheckWeak" />",
                        msgPasswordCheckMedium: "<fmt:message key="msgPasswordCheckMedium" />",
                        msgPasswordCheckStrong: "<fmt:message key="msgPasswordCheckStrong" />"
                    }
                });
            });
        </script>
        <script src="<%=request.getContextPath() + "/js/registrationControlPlugin.js"%>"></script>
        <script>
            $(window).on("load", function () {
                initRegistrationControlPlugin({
                    strings: {
                        alertError: "<fmt:message key="alertError" />",
                        msgPasswordNotMatch: "<fmt:message key="msgPasswordNotMatch" />",
                        msgRetry: "<fmt:message key="msgRetry" />",
                        msgPasswordWeak: "<fmt:message key="msgPasswordWeak" />"
                    }
                });
            });
        </script>
    </body>
</html>
