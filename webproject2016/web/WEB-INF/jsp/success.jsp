<%-- 
    Document   : success
    Created on : 10-nov-2016, 11.38.57
    Author     : nicol
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="support.utility.StateUtility" %>
<!doctype html>
<html>

    <head>
        <%@include file="head.jsp" %>
        <title><fmt:message key="titleSuccess" /> - <fmt:message key="siteName" /></title>
    </head>

    <body>
        <%@include file="menu.jsp" %>

        <div class="container">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="panel-title"><c:out value="${requestScope.header}"/></div>
                </div>
                <div class="panel-body">
                    <div>
                        <p><c:out value="${requestScope.message}"/></p>
                        <p><small><fmt:message key="msgSuccess1" /> <a href="<%=StateUtility.untrack(request)%>"><fmt:message key="linkSuccess" /></a> <fmt:message key="msgSuccess2" /></small></p>
                    </div>
                </div>
            </div>
        </div>
        <script>
            setTimeout(function(){$.redirect("<c:out value="${StateUtility.untrack(pageContext.request)}"/>");}, 1500);
        </script>
    </body>
</html>
