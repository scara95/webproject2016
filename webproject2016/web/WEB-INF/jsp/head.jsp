<%-- 
    Document   : head
    Created on : 9-nov-2016, 11.21.26
    Author     : nicol
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="support.utility.StateUtility" %>
<fmt:setLocale value="${StateUtility.getLocale(pageContext.request)}" />

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="<fmt:message key="metaKeywords" />">
<meta name="description" content="<fmt:message key="metaDescription" />">

<!-- Favicon -->
<link rel="icon" href="<%=request.getContextPath() + "/img/favicon.png"%>" type="image/png" />

<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- Material Design fonts -->
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">

<!-- Bootstrap -->
<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<link href="https://maxcdn.bootstrapcdn.com/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
<script src="https://maxcdn.bootstrapcdn.com/js/ie10-viewport-bug-workaround.js"></script>

<!-- Bootstrap Material Design -->
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() + "/bootstrap_md/dist/css/bootstrap-material-design.css"%>">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() + "/bootstrap_md/dist/css/ripples.min.css"%>">
<script src="<%=request.getContextPath() + "/bootstrap_md/dist/js/material.js"%>"></script>
<script src="<%=request.getContextPath() + "/bootstrap_md/dist/js/ripples.min.js"%>"></script>

<!-- Dropdown material design -->
<script src="https://cdn.rawgit.com/FezVrasta/dropdown.js/master/jquery.dropdown.js"></script>

<!-- Accordion material design -->
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() + "/accordion_md/css/style.css"%>">
<script src="<%=request.getContextPath() + "/accordion_md/js/index.js"%>"></script>

<!-- Tokenfield for TypeAhead Tags -->
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() + "/tokenfield/css/bootstrap-tokenfield.min.css"%>">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() + "/tokenfield/css/tokenfield-typeahead.min.css"%>">
<script src="<%=request.getContextPath() + "/tokenfield/bootstrap-tokenfield.min.js"%>"></script>
<script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.min.js"></script>

<!-- Photogallery -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/4.0.1/ekko-lightbox.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/4.0.1/ekko-lightbox.min.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- GMaps -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA_Xa5O4PLHCnlWcHfnRwlZYJwEoF4j-kw&libraries=places&language=<c:out value="${requestScope.lang}"/>"></script>

<!-- JQuery utility extensions -->
<script src="<%=request.getContextPath() + "/js/JQueryExtensions.js"%>"></script>

<!-- URIfier e sender di Nicola -->
<script src="<%=request.getContextPath() + "/js/URIfier.js"%>"></script>
<script src="<%=request.getContextPath() + "/js/sender.js"%>"></script>

<!-- To refactor -->
<script>
    $(window).on("load", function () {
        // Init material design
        $.material.init();

        // Init lightbox
        $(document).on('click', '[data-toggle="lightbox"]', function (event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });
        
        /* Init tooltips */
        $('[data-toggle="tooltip"]').tooltip();

        /* Autofocus fields if present */
        $('.modal').on('shown.bs.modal', function () {
            $(this).find('[autofocus]').focus();
        });
    });
    if (!Array.mergeSort) {
        Array.prototype.mergeSort = function (compare) {

          var length = this.length,
            middle = Math.floor(length / 2);

          // define default comparison function if none is defined
          if (!compare) {
            compare = function (left, right) {
              if (left  <  right) {
                return -1;
              } else if (left === right) {
                return 0;
              } else {
                return 1;
              }
            };
          }

          if (length < 2) {
            return this;
          }

          function merge(left, right, compare) {

            var result = [];

            while (left.length > 0 || right.length > 0) {
              if (left.length > 0 && right.length > 0) {
                if (compare(left[0], right[0]) <= 0) {
                  result.push(left[0]);
                  left = left.slice(1);
                } else {
                  result.push(right[0]);
                  right = right.slice(1);
                }
              } else if (left.length > 0) {
                result.push(left[0]);
                left = left.slice(1);
              } else if (right.length > 0) {
                result.push(right[0]);
                right = right.slice(1);
              }
            }
            return result;
          }

          return merge(
            this.slice(0, middle).mergeSort(compare),
            this.slice(middle, length).mergeSort(compare),
            compare
          );
        };
    }
</script>

<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() + "/css/styles.css"%>">