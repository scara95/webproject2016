<%-- 
    Document   : search_form
    Created on : 9-dic-2016, 19.06.12
    Author     : Enrico
--%>
<div class="row">
    <form class="form-horizontal" id="search">
        <div class="panel panel-primary col-md-offset-2 col-md-8">
            <div class="panel-body">
                <div class="input-group">
                    <div class="form-group" style="margin:0; padding:0;">
                        <input type="text" name="name" id="name" class="form-control" placeholder="<fmt:message key="fieldSearch" />" autocomplete="off">
                        <ul class="dropdown-menu" id="autoCompleter"></ul>
                    </div>
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="submit" id="searchBtn"><i class="material-icons">search</i></button>
                    </span>
                    <span class="input-group-addon"><i class="material-icons" id="expander">keyboard_arrow_down</i></span>
                </div>
            </div>
        </div>
        <div class="panel panel-primary col-md-offset-2 col-md-8" id="advSearch">
            <div class="panel-body">
                <div class="form-group">
                    <label for="near" class="col-xs-2 control-label"><fmt:message key="fieldSearchNear" /></label>

                    <div class="col-xs-5">
                        <select id="nearType" name="nearType" class="form-control">
                            <option value="none" selected><fmt:message key="fieldSearchNearTypeNoFilter" /></option>
                            <option value="city"><fmt:message key="fieldSearchNearTypeCity" /></option>
                            <option value="province"><fmt:message key="fieldSearchNearTypeProvince" /></option>
                            <option value="state"><fmt:message key="fieldSearchNearTypeState" /></option>
                            <option value="address"><fmt:message key="fieldSearchNearTypeAddress" /></option>
                            <option value="near"><fmt:message key="fieldSearchNearTypeNearTo" /></option>
                        </select>
                    </div>
                    <div class="col-xs-5">
                        <input type="text" class="form-control" id="near" name="near" disabled>
                    </div>
                </div>
                <div class="form-group" id="fgRadius">
                    <label for="radius" class="col-xs-2 control-label"><fmt:message key="fieldSearchRadius" /></label>
                    <div class="col-xs-10 input-group">
                        <input type="number" class="form-control" id="radius" name="radius" value="5" min="1" step="1">
                        <span class="input-group-addon">km</span>
                    </div>
                </div>
                <div class="form-group" id="fgAdvSearchMap">
                    <input id="pac-input" class="controls" type="hidden" placeholder="<fmt:message key="fieldAddress" />">
                    <div id="advSearchMap"></div>
                    <input type="hidden" name="state" id="inputState">
                    <input type="hidden" name="province" id="inputProvince">
                    <input type="hidden" name="city" id="inputCity">
                    <input type="hidden" name="lat" id="inputLat">
                    <input type="hidden" name="lon" id="inputLon">
                </div>
                <div class="form-group">
                    <label for="price" class="col-xs-2 control-label"><fmt:message key="fieldPrice" /></label>

                    <div class="col-xs-10">
                        <select id="price" name="price" class="form-control">
                            <option value="ALL" selected><fmt:message key="fieldPriceNoFilter" /></option>
                            <option value="LOW"><fmt:message key="fieldPriceLow" /></option>
                            <option value="MID"><fmt:message key="fieldPriceMedium" /></option>
                            <option value="HIGH"><fmt:message key="fieldPriceHigh" /></option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="selectTypes" class="col-xs-2 control-label"><fmt:message key="fieldTypes" /></label>
                    <div class="col-xs-10">
                        <div class="btn-group" id="typesButtons1">
                            <a data-target="listTypes1" class="btn btn-default btn-raised dropdown-toggle" data-toggle="dropdown">
                                <fmt:message key="msgChooseTypes" />
                                <span class="caret"></span>
                            </a>
                            <ul id="listTypes1" class="dropdown-menu"></ul>
                        </div>
                        <input type="hidden" name="types" id="types" value="[[]]">
                    </div>
                    <div class="col-xs-offset-2 col-xs-10">
                        <button type='button' class='btn btn-raised btn-default' id="moreTypes"><fmt:message key="msgChooseMoreTypes" /></button>
                    </div>
                </div>
                
            </div>
        </div>
    </form>
</div>
<script src="<%=request.getContextPath() + "/js/searchAutocompletePlugin.js"%>"></script>
<script>
    $(window).on("load", function () {
        initSearchAutocompletePlugin({
            baseURL: "<%=request.getContextPath()%>",
            strings: {
                msgAutocompleteRestaurants: "<fmt:message key="msgAutocompleteRestaurants" />",
                msgAutocompleteDescriptions: "<fmt:message key="msgAutocompleteDescriptions" />"
            }
        });
    });
</script>
<script src="<%=request.getContextPath() + "/js/searchMapPlugin.js"%>"></script>
<script>
    $(window).on("load", function () {
        initSearchMapPlugin({
            strings: {
                fieldSearchNearTypeMe: "<fmt:message key="fieldSearchNearTypeMe" />"
            }
        });
    });
</script>
<script src="<%=request.getContextPath() + "/js/searchPlugin.js"%>"></script>
<script>
    $(window).on("load", function () {
        initSearchPlugin({
            baseURL: "<%=request.getContextPath()%>",
            strings: {
                alertError: "<fmt:message key="alertError" />",
                msgSearchMissingAddress: "<fmt:message key="msgSearchMissingAddress" />"
            }
        });
    });
</script>
<script src="<%=request.getContextPath() + "/js/typesComboboxPlugin.js"%>"></script>
<script>
    $(window).on("load", function () {
        var types = [];
    <c:forEach var="type" items="${requestScope.cookTypes}">
        types.push(["<fmt:message key="${type}" />", "${type}"]);
    </c:forEach>
        initTypesComboboxPlugin({
            types: types, 
            level: 1,
            strings: {msgChooseTypes: "<fmt:message key="msgChooseTypes" />"}
        });
    });
</script>