<%--
    Document   : restaurant
    Created on : 29-nov-2016, 22.03.13
    Author     : Enrico
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>

    <head>
        <%@include file="head.jsp" %>
        <title><fmt:message key="titleToc" /> - <fmt:message key="siteName" /></title>
    </head>

    <body>
        <%@include file="menu.jsp" %>
        <div class="container">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="panel-title"><fmt:message key="titleToc" /></div>
                </div>
                <div class="panel-body">
                    <h2><fmt:message key="toc1" /></h2>
                    <p><fmt:message key="toc2" /></p>
                    <p><fmt:message key="toc3" /></p>
                    <p><fmt:message key="toc4" /></p>
                    <p><fmt:message key="toc5" /></p>
                    <p><fmt:message key="toc6" /></p>
                    <p><fmt:message key="toc7" /></p>
                    <p><fmt:message key="toc8" /></p>
                    <p><fmt:message key="toc9" /></p>
                </div>
            </div>
        </div>

    </body>
</html>