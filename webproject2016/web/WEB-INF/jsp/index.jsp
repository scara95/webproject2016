<%--
    Document   : index
    Created on : 9-nov-2016, 14.38.17
    Author     : nicola
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>

    <head>
        <%@include file="head.jsp" %>
        <title><fmt:message key="siteName" /></title>
    </head>

    <body>
        <%@include file="menu.jsp" %>

        <div class="container">
            <%@include file="search_form.jsp" %>
        </div>
    </body>
</html>
