<%-- 
    Document   : StateForbidden
    Created on : 13-nov-2016, 18.49.34
    Author     : nicol
--%>

<%@page isErrorPage="true" contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="support.utility.StateUtility" %>
<!doctype html>
<html>

    <head>
        <%@include file="head.jsp" %>
        <title><fmt:message key="titleStateForbidden" /> - <fmt:message key="siteName" /></title>
    </head>

    <body>
        <%@include file="menu.jsp" %>

        <div class="container">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="panel-title"><fmt:message key="titleStateForbidden" /></div>
                </div>
                <div class="panel-body">
                    <div>
                        <p><fmt:message key="msgForbidden1" /><br><fmt:message key="msgForbidden2" /></p>
                        <p><a href="<%=StateUtility.untrack(request)%>"><fmt:message key="linkContinue" /></a></p>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
