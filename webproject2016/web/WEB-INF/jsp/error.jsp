<%-- 
    Document   : error
    Created on : 13-nov-2016, 18.26.42
    Author     : nicol
--%>

<%@page isErrorPage="true" contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="support.utility.StateUtility" %>
<!doctype html>
<html>

    <head>
        <%@include file="head.jsp" %>
        <title><fmt:message key="titleError" /> - <fmt:message key="siteName" /></title>
    </head>

    <body>
        <%@include file="menu.jsp" %>

        <div class="container">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="panel-title"><fmt:message key="titleError" /></div>
                </div>
                <div class="panel-body">
                    <div>
                        <p><c:out value="${pageContext.exception.message}"/></p>
                        <p><a href="<%=StateUtility.untrack(request)%>"><fmt:message key="linkContinue" /></a></p>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

