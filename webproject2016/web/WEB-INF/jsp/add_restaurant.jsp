<%--
    Document   : add_restaurant
    Created on : 11-nov-2016, 9.11.03
    Author     : nicol
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="support.utility.StateUtility" %>
<!doctype html>
<html>

    <head>
        <%@include file="head.jsp" %>
        <title><fmt:message key="titleAddRestaurant" /> - <fmt:message key="siteName" /></title>
    </head>

    <body>
        <%@include file="menu.jsp" %>

        <div class="container">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="panel-title"><fmt:message key="titleAddRestaurant" /></div>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="add_restaurant" id="addRestaurant">
                        <fieldset>
                            <div class="form-group">
                                <label for="inputName" class="col-xs-2 control-label"><fmt:message key="fieldName" /></label>

                                <div class="col-xs-10">
                                    <input type="text" name="name" class="form-control" id="inputName" placeholder="<fmt:message key="fieldName" />" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="textArea" class="col-xs-2 control-label"><fmt:message key="fieldDescription" /></label>

                                <div class="col-xs-10">
                                    <textarea class="form-control" rows="3" id="textArea" placeholder="<fmt:message key="fieldDescription" />" name="descr" required></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputSite" class="col-xs-2 control-label"><fmt:message key="fieldWebsite" /></label>

                                <div class="col-xs-10">
                                    <input type="text" name="site" class="form-control" id="inputSite" placeholder="<fmt:message key="fieldWebsite" />" pattern="(https?://)?(\w+\.)+[a-zA-Z/]+">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="selectTypes" class="col-xs-2 control-label"><fmt:message key="fieldTypes" /></label>
                                <div class="col-xs-10">
                                    <div class="btn-group" id="typesButtons1">
                                        <a data-target="listTypes1" class="btn btn-default btn-raised dropdown-toggle" data-toggle="dropdown">
                                            <fmt:message key="msgChooseTypes" />
                                            <span class="caret"></span>
                                        </a>
                                        <ul id="listTypes1" class="dropdown-menu"></ul>
                                    </div>
                                    <input type="hidden" name="types" id="types" value="[[]]">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-2 control-label"><fmt:message key="fieldPrice" /></label>

                                <div class="col-xs-10">
                                    <div class="radio radio-primary">
                                        <label>
                                            <input type="radio" name="price" id="optionsRadios1" value="LOW">
                                            <fmt:message key="fieldPriceLow" />
                                        </label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <label>
                                            <input type="radio" name="price" id="optionsRadios2" value="MID">
                                            <fmt:message key="fieldPriceMedium" />
                                        </label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <label>
                                            <input type="radio" name="price" id="optionsRadios3" value="HIGH">
                                            <fmt:message key="fieldPriceHigh" />
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-1 control-label"><fmt:message key="timetable" /></label>

                                <div class="col-xs-11" id="orariSelector">
                                    <input type="hidden" name="timetable" value="[]">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="pac-input" class="col-xs-2 control-label"><fmt:message key="fieldAddress" /></label>
                                <div class="col-xs-10">
                                    <input id="pac-input" class="form-control" type="text" placeholder="<fmt:message key="fieldAddress" />">
                                    <div id="map"></div>
                                    <input type="hidden" name="state" id="inputState">
                                    <input type="hidden" name="province" id="inputProvince">
                                    <input type="hidden" name="city" id="inputCity">
                                    <input type="hidden" name="lat" id="inputLat" required>
                                    <input type="hidden" name="lon" id="inputLon" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <div class="pull-right">
                                        <a href="<%=StateUtility.untrack(request)%>" class="btn btn-raised btn-default"><fmt:message key="btnDismiss" /></a>
                                        <button type="submit" class="btn btn-raised btn-primary"><fmt:message key="btnFinishAdd" /></button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <script src="<%=request.getContextPath() + "/js/modelDate.js"%>"></script>
        <script src="<%=request.getContextPath() + "/js/orariInserimentoPlugin.js"%>"></script>
        <script>
            $(window).on("load", function () {
                initOrariInserimentoPlugin({
                    strings: {
                        alertError: "<fmt:message key="alertError" />",
                        msgSameDate: "<fmt:message key="msgSameDate" />",
                        msgDateConflict: "<fmt:message key="msgDateConflict" />",
                        msgRetry: "<fmt:message key="msgRetry" />",
                        msgInsertDate: "<fmt:message key="msgInsertDate" />",
                        msgDayOverflow: "<fmt:message key="msgDayOverflow" />",
                        msgOpensAt: "<fmt:message key="msgOpensAt" />",
                        msgClosesAt: "<fmt:message key="msgClosesAt" />",
                        days: ["<fmt:message key="day0" />", "<fmt:message key="day1" />", "<fmt:message key="day2" />",
                            "<fmt:message key="day3" />", "<fmt:message key="day4" />", "<fmt:message key="day5" />", "<fmt:message key="day6" />"]
                    }
                });
            });
        </script>
        <script src="<%=request.getContextPath() + "/js/typesComboboxPlugin.js"%>"></script>
        <script>
            $(window).on("load", function () {
                var types = [];
            <c:forEach var="type" items="${requestScope.types}">
                types.push(["<fmt:message key="${type}" />", "${type}"]);
            </c:forEach>
                initTypesComboboxPlugin({
                    types: types,
                    level: 1,
                    strings: {msgChooseTypes: "<fmt:message key="msgChooseTypes" />"}
                });
            });
        </script>
        <script src="<%=request.getContextPath() + "/js/detectEnterKeyPlugin.js"%>"></script>
        <script>
            $(window).on("load", function () {
                var selectorFields = [];
                var selectorBtn = [];
                $("#orariSelector input[type=time]").each(function (e) {
                    selectorFields.push("#orariSelector [name=" + $(this).attr("name") + "]");
                    selectorBtn.push("#" + $(this).parent().parent().find(".btn-fab").attr("id"));
                });
                initDetectEnterKeyPlugin({
                    selectorFields: selectorFields,
                    selectorBtn: selectorBtn
                });
            });
        </script>
        <script src="<%=request.getContextPath() + "/js/newRestaurantMapPlugin.js"%>"></script>
        <script src="<%=request.getContextPath() + "/js/newRestaurantFormControlPlugin.js"%>"></script>
        <script>
            $(window).on("load", function () {
                initNewRestaurantFormControlPlugin({
                    strings: {
                        alertError: "<fmt:message key="alertError" />",
                        msgNewRestaurantMissingType: "<fmt:message key="msgNewRestaurantMissingType" />",
                        msgNewRestaurantMissingPrice: "<fmt:message key="msgNewRestaurantMissingPrice" />",
                        msgNewRestaurantMissingAddress: "<fmt:message key="msgNewRestaurantMissingAddress" />",
                        msgNewRestaurantMissingDescription: "<fmt:message key="msgNewRestaurantMissingDescription" />",
                        msgNewRestaurantPendingTimetable: "<fmt:message key="msgNewRestaurantPendingTimetable" />"
                    }
                });
            });
        </script>
    </body>
</html>