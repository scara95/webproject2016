<%--
    Document   : all_notifications
    Created on : 9-nov-2016, 22.54.11
    Author     : meis
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="support.utility.StateUtility" %>
<!doctype html>
<html>

    <head>
        <%@include file="head.jsp" %>
        <title><fmt:message key="titleAllNotifications" /> - <fmt:message key="siteName" /></title>
    </head>

    <body>
        <c:set var="logged" value="${sessionScope.user!=null}"></c:set>

        <%@include file="menu.jsp" %>

        <div class="container">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="panel-title"><fmt:message key="titleAllNotifications" /></div>
                </div>
                <div class="panel-body">
                    
                    
                </div>
            </div>
        </div>
    </body>
    <c:if test="${logged}">
        <script src="<%=request.getContextPath() + "/js/notificationsDisplayPlugin.js"%>"></script>
        <script>
            $(window).on("load", function () {
                initNotificationsDisplayPlugin({
                    baseURL: "<%=request.getContextPath()%>",
                    selector: ".container > .panel > .panel-body",
                    content: ${requestScope.notificationsList},
                    strings: {
                        alertSuccess: "<fmt:message key="alertSuccess" />",
                        alertError: "<fmt:message key="alertError" />",
                        msgNoNotifications: "<fmt:message key="msgNoNotifications" />"
                    }
                });
            });
        </script>
    </c:if>
</html>
