<%-- 
    Document   : access_error
    Created on : 13-nov-2016, 18.26.42
    Author     : nicol
--%>

<%@page isErrorPage="true" contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="support.utility.StateUtility" %>
<!doctype html>
<html>

    <head>
        <%@include file="head.jsp" %>
        <title><fmt:message key="titleAccessError" /> - <fmt:message key="siteName" /></title>
    </head>

    <body>
        <%@include file="menu.jsp" %>

        <div class="container">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="panel-title"><fmt:message key="titleAccessError" /></div>
                </div>
                <div class="panel-body">
                    <div>
                        <p>
                            <fmt:message key="msgProtected1" /><br><fmt:message key="msgProtected2" /><br>
                            <a href="<%=request.getContextPath() + "/login"%>"><fmt:message key="login" /></a><fmt:message key="msgAndRetry" />
                        </p>
                        <p><a href="<%=StateUtility.untrack(request)%>"><fmt:message key="linkContinue" /></a></p>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

