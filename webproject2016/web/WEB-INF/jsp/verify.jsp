<%-- 
    Document   : verify
    Created on : 10-nov-2016, 9.46.29
    Author     : nicol
--%>

<%@page import="java.util.UUID"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>

    <head>
        <%@include file="head.jsp" %>
        <title><fmt:message key="titleVerify" /> - <fmt:message key="siteName" /></title>
    </head>

    <body>
        <%@include file="menu.jsp" %>

        <div class="container">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="panel-title"><fmt:message key="titleVerify" /></div>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" action="complete_registration" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="user" value="<%=((UUID) request.getAttribute("user")).toString()%>">
                        <fieldset>
                            <div class="form-group">
                                <label for="inputName" class="col-xs-2 control-label"><fmt:message key="fieldName" /></label>

                                <div class="col-xs-10">
                                    <input type="text" name="name" class="form-control" id="inputName" placeholder="<fmt:message key="fieldName" />" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputSurname" class="col-xs-2 control-label"><fmt:message key="fieldSurname" /></label>

                                <div class="col-xs-10">
                                    <input type="text" name="surname" class="form-control" id="inputSurname" placeholder="<fmt:message key="fieldSurname" />" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputFile" class="col-xs-2 control-label"><fmt:message key="fieldAvatar" /></label>

                                <div class="col-xs-10">
                                    <input type="text" readonly="" class="form-control" placeholder="<fmt:message key="msgImage" />">
                                    <input type="file" id="inputFile" name="avatar">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <button type="submit" class="btn btn-raised btn-primary"><fmt:message key="btnCompleteSignIn" /></button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <script src="<%=request.getContextPath() + "/js/fileSizeCheckPlugin.js"%>"></script>
        <script>
            $(window).on("load", function () {
                initFileSizeCheckPlugin({
                    selector: "form",
                    strings: {
                        alertError: "<fmt:message key="alertError" />",
                        msgImageNotUploaded: "<fmt:message key="msgImageNotUploaded" />",
                        msgRetry: "<fmt:message key="msgRetry" />"
                    }
                });
            });
        </script>
    </body>
</html>
