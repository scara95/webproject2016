<%--
    Document   : search_json
    Created on : 15-gen-2017, 13.02.42
    Author     : nicol
--%>

<%--
    Document   : results
    Created on : 30-nov-2016, 0.29.19
    Author     : nicol
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>
    <head>
        <%@include file="head.jsp" %>
        <title><fmt:message key="titleResults" /> - <fmt:message key="siteName" /></title>
    </head>
    <body>
        <%@include file="menu.jsp" %>
        <div class="container">
            <%@include file="search_form.jsp" %>
            <div class="row" id="searchSorting">
                <div class="panel panel-primary col-md-offset-2 col-md-8">
                    <div class="panel-body">
                        <a class="active" data-criterium="byClassification"><fmt:message key="fieldSortByClassification" /> <i class="material-icons">keyboard_arrow_up</i></a>
                        <a data-criterium="byRate"><fmt:message key="fieldSortByRating" /></a>
                        <a data-criterium="byName"><fmt:message key="fieldSortByAlphabet" /></a>
                        <a data-criterium="byPrice"><fmt:message key="fieldSortByPrice" /></a>
                        <a data-criterium="byCity"><fmt:message key="fieldSortByCity" /></a>
                    </div>
                </div>
            </div>
        </div>
        <script src="<%=request.getContextPath() + "/js/searchSortingModel.js"%>"></script>
        <script src="<%=request.getContextPath() + "/js/searchSortingFunctions.js"%>"></script>
        <script src="<%=request.getContextPath() + "/js/searchSortingPlugin.js"%>"></script>
        <script>
            $(window).on("load", function () {
                initSearchSortingPlugin({
                    baseURL: "<%=request.getContextPath()%>",
                    strings: {
                        msgShowingResultsFrom: "<fmt:message key="msgShowingResultsFrom" />",
                        msgShowingResultsTo: "<fmt:message key="msgShowingResultsTo" />",
                        msgShowingResultsOf: "<fmt:message key="msgShowingResultsOf" />",
                        msgShowingResultsTotal: "<fmt:message key="msgShowingResultsTotal" />",
                        msgNoResults: "<fmt:message key="msgNoResults" />",
                        msgInTheClassificationOf: "<fmt:message key="msgInTheClassificationOf" />",
                        msgGoToTheMap: "<fmt:message key="msgGoToTheMap" />",
                        msgPrice: "<fmt:message key="msgPrice" />",
                        msgRating: "<fmt:message key="msgRating" />",
                        msgOwnedBy: "<fmt:message key="msgOwnedBy" />"
                    }
                });
            });
        </script>
        <script src="<%=request.getContextPath() + "/js/jsonResultsPlugin.js"%>"></script>
        <script>
            var searchResults = undefined;
            var pagedResults = undefined;
            var selectedPage = 0;
        </script>
        <c:if test="${requestScope.toInit}">
            <script>
                initJsonResultsPlugin({
                    baseURL: "<%=request.getContextPath()%>", 
                    request: "${requestScope.initRequest}",
                    strings: {
                        msgShowingResultsFrom: "<fmt:message key="msgShowingResultsFrom" />",
                        msgShowingResultsTo: "<fmt:message key="msgShowingResultsTo" />",
                        msgShowingResultsOf: "<fmt:message key="msgShowingResultsOf" />",
                        msgShowingResultsTotal: "<fmt:message key="msgShowingResultsTotal" />",
                        msgNoResults: "<fmt:message key="msgNoResults" />",
                        msgInTheClassificationOf: "<fmt:message key="msgInTheClassificationOf" />",
                        msgGoToTheMap: "<fmt:message key="msgGoToTheMap" />",
                        msgPrice: "<fmt:message key="msgPrice" />",
                        msgRating: "<fmt:message key="msgRating" />",
                        msgOwnedBy: "<fmt:message key="msgOwnedBy" />",
                        msgTotalReviews: "<fmt:message key="msgTotalReviews" />"
                    }
                });
            </script>
        </c:if>
    </body>
</html>

