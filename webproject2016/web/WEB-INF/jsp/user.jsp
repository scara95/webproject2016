<%--
    Document   : user
    Created on : 14-nov-2016, 17.40.13
    Author     : Enrico
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>

    <head>
        <%@include file="head.jsp" %>
        <title><fmt:message key="titleUser" /> - <fmt:message key="siteName" /></title>
    </head>

    <body>
        <%@include file="menu.jsp" %>
        <c:set var="logged" value="${sessionScope.user!=null}" />
        <c:set var="isMe" value="${logged && sessionScope.user.getId() == requestScope.user.getId()}" />

        <div class="container">
            <div class="panel user panel-primary">
                <div class="panel-head">
                    <div class="list-group">
                        <div class="list-group-item" style="height: 120px;">
                            <div class="row-picture">
                                <c:choose>
                                    <c:when test="${requestScope.user.getAvatar()!=null}">
                                        <div src="<%=request.getContextPath()%>/static/avatar/${requestScope.user.getId()}.jpg"></div>
                                    </c:when>
                                    <c:otherwise>
                                        <i class="material-icons">person</i>
                                    </c:otherwise>
                                </c:choose>
                                <c:if test="${isMe}">
                                    <div class="row-picture-add">
                                        <i class="material-icons" data-toggle="modal" data-target="#editAvatar">add</i>
                                    </div>
                                </c:if>
                            </div>

                            <div class="row-content">
                                <h2>${requestScope.user.getName()} ${requestScope.user.getSurname()}</h2>
                                <i class="material-icons rating-reputazione" rating="${requestScope.userRate}" data-content="star star star star star" data-toggle="tooltip" data-placement="right" data-original-title="">star star star star star</i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <c:if test="${isMe}">
                        <div id="row">
                            <div class="modal fade" id="editAvatar" tabindex="-1" role="dialog" aria-labelledby="editAvatarTitle">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="<%=request.getContextPath() + "/modify_avatar"%>">
                                            <fieldset>
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="editAvatarTitle"><fmt:message key="titleModalEditAvatar" /></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label"><fmt:message key="fieldActualAvatar" /></label>
                                                        <div class="list-group col-sm-10">
                                                            <div class="list-group-item">
                                                                <div class="row-picture">
                                                                    <c:choose>
                                                                        <c:when test="${requestScope.user.getAvatar()!=null}">
                                                                            <div src="<%=request.getContextPath()%>/static/avatar/${requestScope.user.getId()}.jpg"></div>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <i class="material-icons">person</i>
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="inputFile" class="col-sm-2 control-label"><fmt:message key="fieldNewAvatar" /></label>
                                                        <div class="col-sm-10">
                                                            <input type="text" readonly="" class="form-control" placeholder="<fmt:message key="msgImage" />">
                                                            <input type="file" id="inputFile" name="inputFile" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message key="btnDismiss" /></button>
                                                    <button type="submit" class="btn btn-primary"><fmt:message key="btnConfirm" /></button>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="editPassword" tabindex="-1" role="dialog" aria-labelledby="editPasswordTitle">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <form class="form-horizontal">
                                            <fieldset>
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="editPasswordTitle"><fmt:message key="titleModalEditPassword" /></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="inputActualPassword" class="col-sm-2 control-label"><fmt:message key="fieldActualPassword" /></label>
                                                        <div class="col-sm-10">
                                                            <input type="password" class="form-control" id="inputActualPassword" name="oldPwd" placeholder="<fmt:message key="fieldPassword" />" autofocus required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="inputPassword1" class="col-sm-2 control-label"><fmt:message key="fieldNewPassword" /></label>
                                                        <div class="col-sm-10">
                                                            <input type="password" class="form-control" id="inputPassword1" name="newPwd1" placeholder="<fmt:message key="fieldPassword" />" required>
                                                            <div class="help-block" id="pwdStrength"></div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="inputPassword2" class="col-sm-2 control-label"><fmt:message key="fieldNewPasswordAgain" /></label>
                                                        <div class="col-sm-10">
                                                            <input type="password" class="form-control" id="inputPassword2" name="newPwd2" placeholder="<fmt:message key="fieldPassword" />" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message key="btnDismiss" /></button>
                                                    <button type="button" id="editPasswordBtn" class="btn btn-primary"><fmt:message key="btnConfirm" /></button>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <button type="button" class="btn btn-primary btn-lg pull-right" data-toggle="modal" data-target="#editPassword">
                                    <fmt:message key="btnEditPassword" />
                                </button>
                            </div>
                        </div>
                    </c:if>
                    <c:set var="rates" value="${requestScope.lastRates}"/>
                    <c:choose>
                        <c:when test="${not empty rates}">
                            <h3><fmt:message key="msgLastRates" /></h3>
                            <div class="list-group">
                                <c:forEach var="rate" items="${rates}">
                                    <div class="list-group-item">
                                        <div class="row-content">
                                            <div class="row">
                                                <div class="col-md-10 col-sm-9 col-xs-12">
                                                    <a href="<%=request.getContextPath()%>/restaurant/${rate.getRestaurant().getId()}">
                                                        <h4 class="list-group-item-heading">
                                                            <c:out value="${rate.getRestaurant().getName()}" />
                                                        </h4>
                                                    </a>
                                                    <p>
                                                        <i class="material-icons rating" rating="${rate.getRate()}" data-content="star star star star star" data-toggle="tooltip" data-placement="right" data-original-title="">star star star star star</i>
                                                    </p>
                                                </div>
                                                <div class="col-md-2 col-sm-3 col-xs-12">
                                                    <div class="thumbs -fw-pull-right" rate_user="${rate.getUser().getId()}" rate_restaurant="${rate.getRestaurant().getId()}" rate_day="${rate.getDay()}">
                                                        <c:if test="${logged}">
                                                            <c:set var="rated" value="${applicationScope.dbmanager.users.hasRatedRate(sessionScope.user, rate)}"></c:set>
                                                            <c:if test="${rated}">
                                                                <c:set var="vote" value="${applicationScope.dbmanager.users.getRateRate(sessionScope.user, rate)}"></c:set>
                                                            </c:if>
                                                        </c:if>
                                                        <div>${rate.getDay()}</div>
                                                        <span class="thumb-up-num">${rate.getUp()}</span>
                                                        <i class="material-icons
                                                           <c:choose>
                                                               <c:when test="${logged && rated && vote}">
                                                                   thumb-up-voted
                                                               </c:when>
                                                               <c:when test="${logged}">
                                                                   thumb-up-clickable
                                                               </c:when>
                                                           </c:choose>
                                                           ">thumb_up</i>
                                                        <i class="material-icons
                                                           <c:choose>
                                                               <c:when test="${logged && rated && !vote}">
                                                                   thumb-down-voted
                                                               </c:when>
                                                               <c:when test="${logged}">
                                                                   thumb-down-clickable
                                                               </c:when>
                                                           </c:choose>
                                                           ">thumb_down</i>
                                                        <span class="thumb-down-num">${rate.getDown()}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="list-group-item-text"><c:out value="${rate.getReview()}" /></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-group-separator"></div>
                                </c:forEach>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <p><fmt:message key="msgNoRates" /></p>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
        <script src="<%=request.getContextPath() + "/js/starsAndDollarsPlugin.js"%>"></script>
        <script>
            $(window).on("load", function () {
                initStarsAndDollarsPlugin({
                    strings: {
                        msgRating: "<fmt:message key="msgRating" />",
                        msgReputation: "<fmt:message key="msgReputation" />"
                    }
                });
            });
        </script>
        <script src="<%=request.getContextPath() + "/js/propicPlugin.js"%>"></script>
        <script>
            $(window).on("load", function () {
                initPropicPlugin({selector: ".user .panel-head .row-picture > div:first-child, .user .modal-body .row-picture > div:first-child"});
            });
        </script>
        <c:if test="${isMe}">
            <script src="<%=request.getContextPath() + "/js/passwordCheckPlugin.js"%>"></script>
            <script>
            $(window).on("load", function () {
                initPasswordCheckPlugin({
                    strings: {
                        msgPasswordCheckTooWeak: "<fmt:message key="msgPasswordCheckTooWeak" />",
                        msgPasswordCheckWeak: "<fmt:message key="msgPasswordCheckWeak" />",
                        msgPasswordCheckMedium: "<fmt:message key="msgPasswordCheckMedium" />",
                        msgPasswordCheckStrong: "<fmt:message key="msgPasswordCheckStrong" />"
                    }
                });
            });
            </script>
            <script src="<%=request.getContextPath() + "/js/modifyPasswordPlugin.js"%>"></script>
            <script>
            $(window).on("load", function () {
                initModifyPasswordPlugin({
                    baseURL: "<%=request.getContextPath()%>",
                    strings: {
                        alertSuccess: "<fmt:message key="alertSuccess" />",
                        alertError: "<fmt:message key="alertError" />",
                        msgPasswordNotMatch: "<fmt:message key="msgPasswordNotMatch" />",
                        msgPasswordModified: "<fmt:message key="msgPasswordModified" />",
                        msgPasswordWrong: "<fmt:message key="msgPasswordWrong" />",
                        msgPasswordWeak: "<fmt:message key="msgPasswordWeak" />",
                        msgRetry: "<fmt:message key="msgRetry" />"
                    }
                });
            });
            </script>
            <script src="<%=request.getContextPath() + "/js/fileSizeCheckPlugin.js"%>"></script>
            <script>
            $(window).on("load", function () {
                initFileSizeCheckPlugin({
                    selector: "#editAvatar form",
                    strings: {
                        alertError: "<fmt:message key="alertError" />",
                        msgImageNotUploaded: "<fmt:message key="msgImageNotUploaded" />",
                        msgRetry: "<fmt:message key="msgRetry" />"
                    }
                });
            });
            </script>
            <script src="<%=request.getContextPath() + "/js/detectEnterKeyPlugin.js"%>"></script>
            <script>
            $(window).on("load", function () {
                initDetectEnterKeyPlugin({
                    selectorFields: ["#editPassword input"],
                    selectorBtn: ["#editPasswordBtn"]
                });
            });
            </script>
        </c:if>
        <c:if test="${logged}">
            <script src="<%=request.getContextPath() + "/js/thumbsPlugin.js"%>"></script>
            <script>
            $(window).on("load", function () {
                initThumbsPlugin({
                    baseURL: "<%=request.getContextPath()%>",
                    strings: {
                        alertError: "<fmt:message key="alertError" />",
                        msgRetry: "<fmt:message key="msgRetry" />",
                        msgGenericError: "<fmt:message key="msgGenericError" />"
                    }
                });
            });
            </script>
        </c:if>
    </body>
</html>
