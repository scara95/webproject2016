<%--
    Document   : restaurant
    Created on : 14-nov-2016, 17.40.13
    Author     : Enrico
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>

    <head>
        <%@include file="head.jsp" %>
        <title><fmt:message key="titleAcceptRateAnswer" /> - <fmt:message key="siteName" /></title>
    </head>
    <body>
        <%@include file="menu.jsp" %>
        <c:set var="owner" value="${requestScope.restaurant.getOwner()}" />

        <div class="container">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <h2>
                        <c:out value="${requestScope.restaurant.getName()}" />
                        <small>
                            di <a href="<%=request.getContextPath()%>/profile/${owner.getId()}"><c:out value="${owner.getName()} ${owner.getSurname()}" /></a>
                        </small>
                    </h2>
                    <p>
                        <i class="material-icons rating" rating="${requestScope.restaurant.getRate()}" data-content="star star star star star" data-toggle="tooltip" data-placement="right" data-original-title="">star star star star star</i>
                    </p>
                    <h4>
                        Fascia di prezzo: <i class="material-icons rating-money" rating="${requestScope.restaurant.getPrice()}" data-content="attach_money attach_money attach_money" data-toggle="tooltip" data-placement="right" data-original-title="">attach_money attach_money attach_money</i>
                    </h4>
                    <p>
                        <c:out value="${requestScope.restaurant.getDescr()}" />
                    </p>
                    <c:if test="${requestScope.restaurant.getSite()!=null}">
                        <p>
                            <a href="${requestScope.restaurant.getSite()}" target="_blank">${requestScope.restaurant.getSite()}</a>
                        </p>
                    </c:if>
                    <div class="btn-group">
                        <c:forEach var="type" items="${requestScope.restaurant.getTypes()}">
                            <a class="btn btn-raised btn-default -fw-btn-fake"><fmt:message key="${type}" /></a>
                        </c:forEach>
                    </div>
                    <br>
                    <div>
                        <div id="map" class="col-md-8 col-sm-12" lat="${requestScope.restaurant.getLat()}" lon="${requestScope.restaurant.getLon()}"></div>
                        <div class="col-md-4 col-sm-12">
                            <div class="col-xs-6 col-md-12" style="padding:0;">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            <i class="material-icons" style="font-size:15px;">schedule</i> <fmt:message key="timetable" />
                                        </h3>
                                    </div>
                                    <div class="panel-body" id="orari"></div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-md-12" style="text-align:center;" id="QRcode">
                                <img src="">
                            </div>
                        </div>
                    </div>
                    <div class="recensioni row">
                        <div class="col-md-12">
                            <div class="bs-component">
                                <div class="list-group">
                                    <div class="list-group-item">
                                        <div class="row-picture immaginina">
                                            <c:set var="user" value="${rate.getUser()}"></c:set>
                                            <c:choose>
                                                <c:when test="${user.getAvatar()!=null}">
                                                    <img class="circle" src="<%=request.getContextPath()%>/static/avatar/${user.getId()}.jpg" alt="icon"/>
                                                </c:when>
                                                <c:otherwise>
                                                    <i class="material-icons">person</i>
                                                </c:otherwise>
                                            </c:choose>
                                        </div>
                                        <div class="row-content">
                                            <div class="row">
                                                <div class="col-md-10 col-sm-9">
                                                    <a href="<%=request.getContextPath()%>/profile/${rate.getUser().getId()}">
                                                        <h4 class="list-group-item-heading">
                                                            <c:out value="${rate.getUser().getName()} ${rate.getUser().getSurname()}" />
                                                        </h4>
                                                    </a>
                                                    <p>
                                                        <i class="material-icons rating" rating="${rate.getRate()}" data-content="star star star star star" data-toggle="tooltip" data-placement="right" data-original-title="">star star star star star</i>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <p class="list-group-item-text"><c:out value="${rate.getReview()}" /></p>
                                                </div>
                                            </div>
                                        </div>
                                        <c:set var="ownerAnswer" value="${rate.getOwnerAnswer()}"></c:set>
                                        <c:choose>
                                            <c:when test="${ownerAnswer != null && rate.getAnswerApprover() == null}">
                                                <div class="list-group-item col-xs-offset-1" id="answer" style="margin-top:10px;">
                                                    <div class="row-picture immaginina">
                                                        <c:choose>
                                                            <c:when test="${owner.getAvatar()!=null}">
                                                                <img class="circle" src="<%=request.getContextPath()%>/static/avatar/${owner.getId()}.jpg" alt="icon"/>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <i class="material-icons">person</i>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </div>
                                                    <div class="row-content">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <a href="<%=request.getContextPath()%>/profile/${owner.getId()}">
                                                                    <h4 class="list-group-item-heading">
                                                                        <c:out value="${owner.getName()} ${owner.getSurname()}" /> (Ristoratore)
                                                                    </h4>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="list-group-item-text"><c:out value="${ownerAnswer}" /></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-8 col-xs-12 col-sm-offset-2" id="pulsantoloni">
                                                        <button class="btn btn-raised" action-accept="refuse"><fmt:message key="btnRefuse" /></button>
                                                        <button class="btn btn-raised btn-primary pull-right" action-accept="accept"><fmt:message key="btnAccept" /></button>
                                                    </div>
                                                </div>
                                            </c:when>
                                        </c:choose>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="<%=request.getContextPath() + "/js/starsAndDollarsPlugin.js"%>"></script>
        <script>
            $(window).on("load", function () {
                initStarsAndDollarsPlugin({
                    strings: {
                        msgRating: "<fmt:message key="msgRating" />",
                        msgReputation: "<fmt:message key="msgReputation" />"
                    }
                });
            });
        </script>
        <script src="<%=request.getContextPath() + "/js/acceptAnswerPlugin.js"%>"></script>
        <script>
            $(window).on("load", function () {
                initAcceptAnswerPlugin({
                    baseURL: "<%=request.getContextPath()%>",
                    userId: ${rate.getUser().getId()},
                    restaurantId: ${rate.getRestaurant().getId()},
                    day: "${rate.getDay()}",
                    strings: {
                        alertSuccess: "<fmt:message key="alertSuccess" />",
                        alertError: "<fmt:message key="alertError" />",
                        msgAnswerAccepted: "<fmt:message key="msgAnswerAccepted" />",
                        msgAnswerNotAccepted: "<fmt:message key="msgAnswerNotAccepted" />"
                    }
                });
            });
        </script>
        <script src="<%=request.getContextPath() + "/js/orariDisplayPlugin.js"%>"></script>
        <script>
            var orari;
            $(window).on("load", function () {
                orari = initOrariDisplayPlugin({
                    selector: "#orari", 
                    orari: ${requestScope.restaurant.getTimetable()},
                    strings: {
                        days: ["<fmt:message key="day0" />", "<fmt:message key="day1" />", "<fmt:message key="day2" />", 
                    "<fmt:message key="day3" />", "<fmt:message key="day4" />", "<fmt:message key="day5" />", "<fmt:message key="day6" />"]
                    }
                });
            });
        </script>
        <script src="<%=request.getContextPath() + "/js/mapQRDisplayerPlugin.js"%>"></script>
        <script>
            $(window).on("load", function () {
                initMapQRDisplayerPlugin({
                    baseURL: "<%=request.getContextPath()%>",
                    lat: "<c:out value="${requestScope.restaurant.getLat()}" />",
                    lng: "<c:out value="${requestScope.restaurant.getLon()}" />",
                    restaurantName: "<c:out value="${requestScope.restaurant.getName()}" />",
                    orari: orari,
                    strings: {
                        msgQRName: "<fmt:message key="msgQRName" />",
                        msgQRAddress: "<fmt:message key="msgQRAddress" />",
                        msgQRTimetable: "<fmt:message key="msgQRTimetable" />"
                    }
                });
            });
        </script>
    </body>
</html>
