/**
 * This plugin manages the AJAX request of the report image feature
 * @param init object containing all of the dynamically generated data
 */
function initReportImgPlugin(init) {
    $(".report-img a[action='report']").click(function () {
        var image = $(this).parent().attr("img");
        var parent = $(this).parent();
        var nonno = parent.parent();
        $.ajax({
            method: "POST",
            url: init.baseURL + "/restaurant_picture/report",
            data: {id: image}
        }).done(function () {
            nonno.slideUp(300);
            $.alert("success", init.strings.alertSuccess, init.strings.msgImageReported);
            $.getJSON(init.baseURL + "/notifications", {}, renderNotifications);
        }).fail(function () {
            $.alert("danger", init.strings.alertError, init.strings.msgGenericError + " " + init.strings.msgRetry);
        });
    });

    $(".report-img a[action='accept']").click(function () {
        var image = $(this).parent().attr("img");
        var parent = $(this).parent();
        $.ajax({
            method: "POST",
            url: init.baseURL + "/restaurant_picture/accept",
            data: {id: image}
        }).done(function () {
            parent.slideUp(300);
            $.alert("success", init.strings.alertSuccess, init.strings.msgImageAccepted);
            $.getJSON(init.baseURL + "/notifications", {}, renderNotifications);
        }).fail(function () {
            $.alert("danger", init.strings.alertError, init.strings.msgGenericError + " " + init.strings.msgRetry);
        });
    });
}