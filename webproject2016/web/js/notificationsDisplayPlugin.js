/** 
 * This plugin manages the building of the notifications list in all_notifications.jsp
 * @param init object containing all of the dynamically generated data
 */
function initNotificationsDisplayPlugin(init) {
    console.log(init.content);
    //var ns = JSON.parse(init.content); //if content is stringified
    var ns = init.content;
    if (ns.length === 0) {
        $(init.selector).text(init.strings.msgNoNotifications);
    } else {

        for (var i = 0; i < ns.length; ++i) {
            var nRow = $(document.createElement("div")).appendTo(init.selector);

            var nHeader = $(document.createElement("div")).appendTo(nRow);
            $(document.createElement("h4")).text(ns[i].name).appendTo(nHeader);
            $(document.createElement("p")).text(ns[i].message).appendTo(nHeader);

            var nBody = $(document.createElement("div")).appendTo(nRow);
            $.each(ns[i].controls, function (index, c) {
            //for (var j = 0; j < ns[i].controls.length; ++j) {
                var button = $(document.createElement("button")).addClass("btn btn-raised").text(c.name).appendTo(nBody);
                if (c.primary) {
                    button.addClass("btn-primary");
                }
                if (c.type === "AJAX") {
                    button.click(function () {
                        var callbacks = {};
                        $.each(c.AJAXMessages, function (index, m) {
                        //for (var k = 0; k < c.AJAXMessages.length; ++k) {
                            callbacks[m.status] = function () {
                                if (m.successful) {
                                    $.alert("success", init.strings.alertSuccess, m.message);
                                } else {
                                    $.alert("danger", init.strings.alertError, m.message);
                                }
                                button.parent().parent().remove();
                            };
                        });

                        $.ajax({
                            method: "POST",
                            url: init.baseURL + c.URL,
                            data: c.parameters,
                            statusCode: callbacks
                        });
                    });
                } else if (c.type === "REDIRECT") {
                    button.click(function () {
                        console.log(i);
                        var requestURL = init.baseURL + c.URL;
                        var requestParameters = c.parameters;
                        console.log(requestParameters);
                        $.redirectPost(requestURL, requestParameters);
                    });
                }
            });
        }
    }
}