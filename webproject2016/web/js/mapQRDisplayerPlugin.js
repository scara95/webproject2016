/**
 * This plugin manages the build of the restaurant map and QR code
 * @param init object containing all of the dynamically generated data
 */
function initMapQRDisplayerPlugin(init) {
    var address = "";
    var latlng = new google.maps.LatLng(init.lat, init.lng);
    var map = new google.maps.Map(document.getElementById("map"), {
        center: latlng,
        zoom: 17
    });


    $.ajax({
        type: "GET",
        url: "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latlng.toUrlValue() + "&key=AIzaSyA_Xa5O4PLHCnlWcHfnRwlZYJwEoF4j-kw&language=en",
        dataType: "json"
    }).done(function (json) {
        console.log(json);
        address = json.results[0].formatted_address;
        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: init.restaurantName,
            html: "<h2>" + init.restaurantName + "</h2>" + "<p>" + address + "</p>"
        });
        
        var infowindow = new google.maps.InfoWindow({
            content: marker.html,
            maxWidth: 400
        });

        infowindow.open(map, marker);

        marker.addListener('click', function () {
            infowindow.setContent(this.html);
            infowindow.open(map, marker);
        });

        $.ajax({
            dataType: "json",
            url: init.baseURL + "/search_json/ALL",
            data: {}
        }).done(function (x) {
            for (var i = 0; i < x.length; ++i) {
                if (x[i].lat !== parseFloat(init.lat) && x[i].lon !== parseFloat(init.lng)) {
                    var latlng = new google.maps.LatLng(x[i].lat, x[i].lon);
                    var marker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        title: x[i].name,
                        icon: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png",
                        html: "<h2>" + x[i].name + "</h2>" + "<p>" + x[i].descr + "</p>"
                    });

                    marker.addListener('click', function () {
                        infowindow.setContent(this.html);
                        infowindow.open(map, this);
                    });
                }
            }
        });

        /* QR */
        var nome = init.restaurantName;
        var orari = init.orari;
        var src = init.strings.msgQRName + " " + encodeURI(nome) + "%0D%0A" + init.strings.msgQRAddress + " " + encodeURI(address) + "%0D%0A" + init.strings.msgQRTimetable + " " + orari;
        $("#QRcode img").attr("src", "https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=" + src);
    });


}