/**
 * This plugin manages the AJAX request of the answer feature
 * @param init object containing all of the dynamically generated data
 */
function initNewAnswerPlugin(init) {
    $("#newAnswerBtn").click(function () {
        var testo = $.trim($("#textArea").val());
        if (testo.length === 0) {
            $.alert("danger", init.strings.alertError, init.strings.msgNewAnswerEmpty + " " + init.strings.msgRetry);
        } else {
            var rate_restaurant = $(this).attr("rate_r");
            var rate_day = $(this).attr("rate_d");
            var rate_user = $(this).attr("rate_u");
            $.ajax({
                method: "POST",
                url: init.baseURL + "/answer_rate",
                data: {answer: testo, restaurant_id: rate_restaurant, user_id: rate_user, day: rate_day}
            }).done(function () {
                $("#newAnswer").modal("toggle");
                $("#newAnswer form").get(0).reset();
                $(".recensioni button[rate_d='" + rate_day + "'][rate_r='" + rate_restaurant + "'][rate_u='" + rate_user + "'][esterno]").slideUp(300);
                $.alert("success", init.strings.alertSuccess, init.strings.msgNewAnswerSent);
                $.getJSON(init.baseURL + "/notifications", {}, renderNotifications);
            }).fail(function () {
                $.alert("danger", init.strings.alertError, init.strings.msgGenericError + " " + init.strings.msgRetry);
            });
        }
    });
}