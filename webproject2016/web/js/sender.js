/** 
 * This plugin manages the creation of the URL for the search and redirects to it
 * @param baseURL the base URL of the website
 * @param type the type of advanced search
 * @param state the state searched
 * @param province the province searched
 * @param city the city searched
 * @param lon the longitude searched
 * @param lat the latitude searched
 * @param radius the radius selected
 * @param price the price range selected
 * @param taglist the taglist inserted
 * @param name the name searched
 */
function search(baseURL, type, state, province, city, lon, lat, radius, price, taglist, name) {
    var args = [price, ...taglist];
    switch(type) {
        case 'state':
            args.unshift(state);
            break;
        case 'city':
            args.unshift(city);
            break;
        case 'province':
            args.unshift(province);
            break;
        case 'near':
            args.unshift([lon, lat, radius]);
            break;
        case 'address':
            args.unshift([lon, lat]);
            break;
    }
    if(!('none'===type)) {
        args.unshift(type);
    }
    
    var URL = baseURL+URIfy(...args);
    if(!((name === null) || (name === ""))) {
        URL += "?name=" + encodeURIComponent(name);
    }
    $.redirect(URL);
}