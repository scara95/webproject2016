/** 
 * This plugin manages the creation of yellow stars for reputation and green dollars for price range
 * @param init object containing all of the dynamically generated data
 */
function initStarsAndDollarsPlugin(init) {
    $(".rating").not("#newRateStars").each(function (index) {
        $(this).addClass("ratingStar-" + index);
        var thisRating = $(this).attr("rating") * 20;
        $(document.createElement("style")).text(".ratingStar-" + index + ":before{width:" + thisRating + "% !important;} .ratingStar-" + index + ":after{width:" + (100 - thisRating) + "% !important; left:" + thisRating + "% !important;}").appendTo("head");
        $(this).attr("data-original-title", init.strings.msgRating + " " + Math.round($(this).attr("rating") * 10) / 10 + "/5");
    });
    $(".rating-reputazione").each(function (index) {
        $(this).addClass("ratingStarRepu-" + index);
        var thisRating = $(this).attr("rating") * 20;
        $(document.createElement("style")).text(".ratingStarRepu-" + index + ":before{width:" + thisRating + "% !important;} .ratingStarRepu-" + index + ":after{width:" + (100 - thisRating) + "% !important; left:" + thisRating + "% !important;}").appendTo("head");
        $(this).attr("data-original-title", init.strings.msgReputation + " " + Math.round($(this).attr("rating") * 10) / 10 + "/5");
    });
    $(".rating-money").each(function (index) {
        $(this).addClass("ratingMoney-" + index);
        var ratingMoney = $(this).attr("rating");
        var ratingM;
        switch (ratingMoney) {
            case "LOW":
                ratingM = 33;
                break;
            case "MID":
                ratingM = 66;
                break;
            case "HIGH":
                ratingM = 100;
                break;
            default:
                ratingM = 0;
        }
        $(document.createElement("style")).text(".ratingMoney-" + index + ":before{width:" + ratingM + "%;} .ratingMoney-" + index + ":after{width:" + (100 - ratingM) + "%; left:" + ratingM + "%;}").appendTo("head");
    });
}