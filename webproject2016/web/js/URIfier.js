/**
 * accept a variable argument list and format them as an URI,
 * arrays are formatted as a single dash-separeted component
 * example:
 * URIfy("pollo con", [5, "mandorle"], "pearà") === "/pollo%20con/5-mandorle/pear%C3%A0"
 */
function URIfy() {
    var uri = "/";
    var lastA = arguments.length-1;
    $.each(arguments, function(index, value) {
        var t = typeof(value);
        if(Array.isArray(value)) {
            var lastV = value.length - 1;
            $.each(value, function(index, value) {
                uri += encodeURIComponent(value);
                if(index < lastV) {
                    uri += ",";
                }
            });
        }
        else if(t === "string" || t === "boolean" || t === "number") {
            uri += encodeURIComponent(value);
        }
        if(index < lastA) {
            uri += "/";
        }
    });
    return uri;
}