var RestaurantOrdering = function(){
    var id = function(x){return x;};
    var bySelector = function(mapping, sel) {
        return function(a, b) {
            var a1 = mapping(a[sel]);
            var b1 = mapping(b[sel]);
            if(a1 < b1) {
                return -1;
            }
            else if(a1 === b1) {
                return 0;
            }
            else {
                return 1;
            }
        };
    };
    
    var prices = { LOW: 0, MID: 1, HIGH: 2 };
    
    return {
        byName: bySelector(id, "name"),

        byPrice: bySelector(function(x) { return prices[x]; }, "price"),

        byClassification: bySelector(id, "classification"),

        byCity: bySelector(id, "city"),

        byRate: bySelector(id, "rate")
    };
}();


