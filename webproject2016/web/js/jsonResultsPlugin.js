/**
 * This function rebuilds the results of a search due to a page switch
 * @param i the page to redirect to
 * @param init object containing all of the dynamically generated data
 */
function switchPage(i, init) {
    selectedPage = i;
    $('#searchSorting').nextAll('div').remove();
    $("head style").remove();
    buildResults(init);
}

/**
 * This function builds the pagination container of a search
 * @param init object containing all of the dynamically generated data
 */
function initPaginator(init) {
    var paginationPanel = $(document.createElement('div')).addClass("panel panel-primary").appendTo(".container");
    var pagBody = $(document.createElement('div')).addClass("panel-body")
            .text(init.strings.msgShowingResultsFrom + " " + pagedResults.getStartIndexFromPage(selectedPage) + " " + init.strings.msgShowingResultsTo + " " + pagedResults.getStopIndexFromPage(selectedPage) + " (" + init.strings.msgShowingResultsOf + " " + pagedResults.length() + " " + init.strings.msgShowingResultsTotal + ")").appendTo(paginationPanel);
    /* Assuming pagedResults.getPagesCount() > 0 */
    if (pagedResults.getPagesCount() !== 1) {
        var paginatorContainer = $(document.createElement('div')).addClass("paginator").appendTo(pagBody);
        for (var i = 1; i <= pagedResults.getPagesCount(); ++i) {
            $(document.createElement('a')).attr({onClick: "switchPage(" + (i - 1) + ", " + JSON.stringify(init) + ")"}).text(i).appendTo(paginatorContainer);
        }
    }
}

/**
 * This function builds the results of a search
 * @param init object containing all of the dynamically generated data
 */
function buildResults(init) {
    var rs = pagedResults.getPage(selectedPage);
    if (searchResults.length() === 0) {
        var infoPanel = $(document.createElement('div')).addClass("panel panel-primary").appendTo(".container");
        $(document.createElement('div')).addClass("panel-body").text(init.strings.msgNoResults).appendTo(infoPanel);
    } else if (searchResults.length() === 1) {
        $.followLink(init.baseURL + "/restaurant/" + searchResults.at(0).id);
    } else {
        initPaginator(init);

        for (var i = 0; i < rs.length; ++i) {
            (function (i) {
                var restContainer = $(document.createElement('div')).addClass("row restaurant-list").appendTo(".container");
                var imageContainer = $(document.createElement('div')).addClass("col-sm-3 col-xs-12").appendTo(restContainer);
                if (rs[i].hasOwnProperty("image")) {
                    var restImageLink = $(document.createElement('a')).attr({href: init.baseURL + "/restaurant/" + rs[i].id}).appendTo(imageContainer);
                    $(document.createElement('div')).addClass("img-gallery img-gallery-" + i).appendTo(restImageLink);
                    $(document.createElement("style")).text(".img-gallery-" + i + " {background: url(\"" + init.baseURL + "/static/picture/" + rs[i].image + ".jpg\") center center no-repeat;}").appendTo("head");
                } else {
                    $(document.createElement('i')).addClass("material-icons").text("image").appendTo(imageContainer);
                }


                var mainContainer = $(document.createElement('div')).addClass("col-sm-9 col-xs-12").appendTo(restContainer);
                var panel = $(document.createElement('div')).addClass("panel panel-primary").appendTo(mainContainer);
                var panelHeading = $(document.createElement('div')).addClass("panel-heading").appendTo(panel);
                var panelTitle = $(document.createElement('span')).addClass("panel-title").appendTo(panelHeading);
                $(document.createElement('strong')).attr({
                    "data-toggle": "tooltip",
                    "data-placement": "bottom",
                    "data-original-title": init.strings.msgInTheClassificationOf + " " + rs[i].city
                }).text("#" + rs[i].classification + " ").appendTo(panelTitle);
                $(document.createElement('i')).addClass("material-icons rating ratingStar-" + i).attr({
                    "data-content": "star star star star star",
                    "data-toggle": "tooltip",
                    "data-placement": "bottom",
                    "data-original-title": init.strings.msgRating + " " + Math.round(rs[i].rate * 10) / 10 + "/5"
                }).text("star star star star star").appendTo(panelTitle);
                $(document.createElement("style")).text(".ratingStar-" + i + ":before{width:" + (rs[i].rate * 20) + "% !important;} .ratingStar-" + i + ":after{width:" + (100 - (rs[i].rate * 20)) + "% !important; left:" + (rs[i].rate * 20) + "% !important;}").appendTo("head");
                $(document.createTextNode(" ")).appendTo(panelTitle);
                $(document.createElement('a')).attr({
                    style: "font-weight:bold;",
                    href: init.baseURL + "/restaurant/" + rs[i].id
                }).text(rs[i].name).appendTo(panelTitle);
                if (rs[i].hasOwnProperty("owner") && rs[i].hasOwnProperty("ownerapprover")) {
                    var ownerTitle = $(document.createElement('small')).text(" " + init.strings.msgOwnedBy + " ").appendTo(panelTitle);
                    $(document.createElement('a')).attr({href: init.baseURL + "/profile/" + rs[i].owner.id}).text(rs[i].owner.name + " " + rs[i].owner.surname).appendTo(ownerTitle);
                }
                $(document.createTextNode(" ")).appendTo(panelTitle);
                $(document.createElement('small')).text("(" + rs[i].ratesCount + " " + init.strings.msgTotalReviews + ")").appendTo(panelTitle);

                var panelBody = $(document.createElement('div')).addClass("panel-body").appendTo(panel);
                $(document.createElement('p')).text(rs[i].descr).appendTo(panelBody);
                var typeContainer = $(document.createElement('div')).addClass("btn-group").appendTo(panelBody);
                for (var j = 0; j < rs[i].types.length; ++j) {
                    $(document.createElement('a')).addClass("btn btn-default btn-raised btn-sm -fw-btn-fake").text(rs[i].types[j]).appendTo(typeContainer);
                }
                var mapContainer = $(document.createElement('div')).addClass("row gMapsLink").appendTo(panelBody);
                var mapLink = $(document.createElement('a')).attr({target: "_blank"}).text(init.strings.msgGoToTheMap).appendTo(mapContainer);
                var latlng = new google.maps.LatLng(rs[i].lat, rs[i].lon);
                $.ajax({
                    type: "GET",
                    url: "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latlng.toUrlValue() + "&key=AIzaSyA_Xa5O4PLHCnlWcHfnRwlZYJwEoF4j-kw&language=en",
                    dataType: "json"
                }).done(function (json) {
                    var address = json.results[0].formatted_address;
                    mapLink.attr({href: "https://www.google.com/maps/place/" + address});
                });
                var priceContainer = $(document.createElement('div')).addClass("row").appendTo(panelBody);
                var priceContainerDUE = $(document.createElement('div')).addClass("pull-right").text(init.strings.msgPrice + " ").appendTo(priceContainer);
                $(document.createElement('i')).addClass("material-icons rating-money ratingMoney-" + i).attr({
                    "data-content": "attach_money attach_money attach_money"
                }).text("attach_money attach_money attach_money").appendTo(priceContainerDUE);
                var ratingM;
                switch (rs[i].price) {
                    case "LOW":
                        ratingM = 33;
                        break;
                    case "MID":
                        ratingM = 66;
                        break;
                    case "HIGH":
                        ratingM = 100;
                        break;
                    default:
                        ratingM = 0;
                }
                $(document.createElement("style")).text(".ratingMoney-" + i + ":before{width:" + ratingM + "%;} .ratingMoney-" + i + ":after{width:" + (100 - ratingM) + "%; left:" + ratingM + "%;}").appendTo("head");
            })(i);
        }
        initPaginator(init);

        /* I need to recall some functions */
        $('[data-toggle="tooltip"]').tooltip();
    }
}

/** 
 * This plugin allows to manage the search and builds the results
 * @param init object containing all of the dynamically generated data
 */
function initJsonResultsPlugin(init) {
    $.ajax({
        dataType: "json",
        url: init.request,
        data: {}
    }).done(function (x) {
        searchResults = new Ordering(x);
        for (o in RestaurantOrdering) {
            searchResults.addCriterium(o, RestaurantOrdering[o]);
        }
        searchResults.incrementalSort("byClassification", false);
        pagedResults = new Paging(searchResults, 15);
        buildResults(init);
    });
}