/** 
 * This plugin manages the visualization of the avatars
 * @param init object containing all of the dynamically generated data
 */
function initPropicPlugin(init) {
    var div = $(init.selector);
    div.each(function (index) {
        $(this).addClass("immaginina-" + index);
        var link = $(this).attr("src");
        $(document.createElement("style")).text(".immaginina-" + index + " {background-image: url(\"" + link + "\")}").appendTo("head");
    });
}