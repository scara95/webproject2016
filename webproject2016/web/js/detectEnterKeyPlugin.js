/** 
 * This plugin extends the enter key to some other elements of a form
 * @param init object containing all of the dynamically generated data
 */
function initDetectEnterKeyPlugin(init) {
    $(init.selectorFields).each(function(i, e) {
        $(e).keypress(function (event) {
            if (event.which === 13) {
                event.preventDefault();
                $(init.selectorBtn[i]).click();
            }
        });
    });
}