/** 
 * This plugin manages the creation and editing of cooking types in the restaurant data, for searching and adding purpouses
 * @param init object containing all of the dynamically generated data
 */
function initTypesComboboxPlugin(init) {
    for (var i = 0; i < init.types.length; ++i) {
        var cook = $(document.createElement("li")).attr({el: init.types[i][1].replace(/ /g, "-")}).appendTo("#listTypes" + init.level);
        $(document.createElement("a")).attr({onClick: "metti('" + init.types[i][1] + "', '" + init.types[i][0] + "', " + init.level + ")"}).text(init.types[i][0]).appendTo(cook);
    }

    $("#moreTypes").click(function () {
        init.level++;
        var row = $(document.createElement('div')).addClass("col-xs-offset-2 col-xs-10").insertBefore($(this).parent());
        var newRow = $(document.createElement('div')).addClass("btn-group").attr({id: "typesButtons" + init.level}).appendTo(row);
        var cosoCheCiClicchiSopra = $(document.createElement('a')).addClass("btn btn-default btn-raised dropdown-toggle")
                .attr({"data-target": "listTypes" + init.level, "data-toggle": "dropdown"}).text(init.strings.msgChooseTypes).appendTo(newRow);
        $(document.createElement("span")).addClass("caret").appendTo(cosoCheCiClicchiSopra);
        var cookList = $(document.createElement("ul")).addClass("dropdown-menu").attr({id: "listTypes" + init.level}).appendTo(newRow);
        for (var i = 0; i < init.types.length; ++i) {
            var cook = $(document.createElement("li")).attr({el: init.types[i][1].replace(/ /g, "-")}).appendTo(cookList);
            $(document.createElement("a")).attr({onClick: "metti('" + init.types[i][1] + "', '" + init.types[i][0] + "',  " + init.level + ")"}).text(init.types[i][0]).appendTo(cook);
        }
        
        var actual = JSON.parse($("#types").val());
        actual.push([]);
        $("#types").val(JSON.stringify(actual));
    });
}

/** 
 * This function adds a cooking type to the selected filter/list
 * @param el string representing which cooking type we are adding
 * @param el_lang el translated 
 * @param level the level of the filtering, starting with 0
 */
function metti(el, el_lang, level) {   
    var typeBtn = $(document.createElement("button")).addClass("btn btn-raised btn-default -fw-btn-fake")
            .attr({type: "button", el: el.replace(/ /g, "-")}).text(el_lang).appendTo("#typesButtons" + level);
    $(document.createElement("a")).attr({onClick: "togli('" + el + "', '"+el_lang+"', "+level+")"}).text("×").appendTo(typeBtn);
    $("#listTypes" + level).find("[el=" + el.replace(/ /g, "-") + "]").remove();
    
    var actual = JSON.parse($("#types").val());
    actual[level-1].push(el);
    $("#types").val(JSON.stringify(actual));
}

/** 
 * This function removes a cooking type from the selected filter/list
 * @param el string representing which cooking type we are deleting
 * @param el_lang el translated
 * @param level the level of the filtering, starting with 0
 */
function togli(el, el_lang, level) {
    $("#typesButtons"+level+" button[el=" + el.replace(/ /g, "-") + "]").slideUp(300);
    var cook = $(document.createElement("li")).attr({el: el.replace(/ /g, "-")}).appendTo("#listTypes"+level);
    $(document.createElement("a")).attr({onClick: "metti('" + el + "', '"+el_lang+"' ," + level + ")"}).text(el_lang).appendTo(cook);
    
    var actual = JSON.parse($("#types").val());
    var index = actual[level-1].indexOf(el);
    actual[level-1].splice(index,1);
    $("#types").val(JSON.stringify(actual));
}