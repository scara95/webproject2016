/** 
 * This plugin manages the submitting of the search
 * @param init object containing all of the dynamically generated data
 */
function initSearchPlugin(init) {
    if ($.urlParam("name") !== null) {
        $("#name").val($.urlParam("name"));
    }

    $("#search").submit(function (e) {
        e.preventDefault();

        if ($("#nearType").val() !== "none" && $("#inputLat").val() === "") {
            $.alert("danger", init.strings.alertError, init.strings.msgSearchMissingAddress);
        } else {
            var baseURL = init.baseURL + "/search";
            var type = $("#nearType").val();
            if (type === "me")
                type = "near";
            var state = $("#inputState").val();
            var province = $("#inputProvince").val();
            var city = $("#inputCity").val();
            var lon = $("#inputLon").val();
            var lat = $("#inputLat").val();
            var radius = $("#radius").val();
            var price = $("#price").val();
            var taglist = JSON.parse($("#types").val());
            var name = $("#name").val();
            $("#nearType").val(0);
            search(baseURL, type, state, province, city, lon, lat, radius, price, taglist, name);
        }
    });
}