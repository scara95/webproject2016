/**
 * This plugin manages the map in the add_restaurant.jsp
 */
$(window).on("load", function () {
    $("#inputName").keyup(function () {
        $("#pac-input").val($(this).val());
    });

    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 41.8719400, lng: 12.5673800},
        zoom: 5
    });
    var input = document.getElementById('pac-input');

    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

    var infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });

    autocomplete.addListener('place_changed', function () {

        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();

        $.ajax({
            type: "GET",
            url: "https://maps.googleapis.com/maps/api/geocode/json?address=" + place.formatted_address + "&key=AIzaSyA_Xa5O4PLHCnlWcHfnRwlZYJwEoF4j-kw&language=en",
            dataType: "json"
        }).done(function (json) {
            var lat = json.results[0].geometry.location.lat;
            var lon = json.results[0].geometry.location.lng;
            var addrComp = json.results[0].address_components;
            for(var i = 0; i < addrComp.length; ++i) {
                if(addrComp[i].types[0] === "locality") {
                    var city = addrComp[i].long_name;
                }
                if(addrComp[i].types[0] === "administrative_area_level_2") {
                    var province = addrComp[i].short_name;
                }
                if(addrComp[i].types[0] === "country") {
                    var state = addrComp[i].short_name;
                }
            }
            
            $("#inputLat").val(lat);
            $("#inputLon").val(lon);
            $("#inputProvince").val(province);
            $("#inputCity").val(city);
            $("#inputState").val(state);
        });

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
        }
        marker.setIcon(/** @type {google.maps.Icon} */({
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
        }));
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        var address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }

        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
        infowindow.open(map, marker);
    });
});