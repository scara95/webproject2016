//Define a selectable order on some generic data
function Ordering(data) {
    //table name -> function for saving ordering criteria
    this.criteria = {};

    //add an ordering critirium
    //order is a comparison function: -1 if a < b, 0 if a = b, 1 if a > b
    this.addCriterium = function(name, order) {
        //map a key array to original data
        this.criteria[name] = function(a, b) { return order(data[a], data[b]); };;
    };

    //current ordering criteria: name & reversed
    this.current = {};
    
    this.currentMethod = function() { return this.current.name; };
    
    this.currentFlaw = function() { return this.current.reversed; };

    //utility array to keep an ordered list of keys
    var ordered = data.map(function(_, i) { return i; });
    
    //initial sorted order
    this.sorted = ordered.slice();

    //incrementally sort the data starting from ordered sort
    this.sortFromOrder = function(name, reversed, ordered) {
        var current = this.current;
        if(current.name === name) { //ordering method has not changed
            if(current.reversed === reversed) { //the same flaw as before, nop
                return;
            }
            else { //different flaw, reverse the data and update current
                current.reversed = reversed;
                this.sorted.reverse();
                return;
            }
        }
        else { //ordering method has changed, reconstruct sorted from original
               //data and update current
            current.name = name;
            current.reversed = reversed;
            this.sorted = ordered.slice().mergeSort(this.criteria[name]);
            if(reversed) {
                this.sorted.reverse();
            }
            return;
        }
    };
    
    //sort the data starting from original order
    this.sort = function(name, reversed) { this.sortFromOrder(name, reversed, ordered); };
    
    //incremental sort, keep relative position between equal elements
    this.incrementalSort = function(name, reversed) { this.sortFromOrder(name, reversed, this.sorted); };

    //return object at i in sorted order
    this.at = function(i) { return data[this.sorted[i]]; };
    
    //get a slice of data in sorted order
    this.slice = function(a, b) { return this.sorted.slice(a, b).map(function(i) { return data[i]; }); };
    
    //return length
    this.length = function() { return data.length; };
    
    /* flow style methods begin */
    
    this.withCriterium = function(name, order) {
        this.addCriterium(name, order); return this;
    };
    
    this.sortedAs = function(name, reversed) { this.sort(name, reversed); return this; };
    
    this.incrementallySortedAs = function(name, reversed) { this.incrementalSort(name, reversed); return this; };
    
    /* flow style methods end */
};

//Data paging
//source object with length and at methods
//epp number of elements per page
function Paging(source, epp) {
    //return the total number of pages
    this.getPagesCount = function() { return Math.ceil(source.length()/epp); };
    
    //get a page of data
    this.getPage = function(i) { return source.slice(epp*i, epp*(i+1)); };
    
    //1-based inclusive indexes [Start,Stop]
    this.getStartIndexFromPage = function (i) { return Math.min(source.length(), epp*i+1); };
    
    this.getStopIndexFromPage = function (i) { return Math.min(source.length(), epp*(i+1)); };
    
    this.length = source.length;
};


/* simple usage example
var a = new Paging(
    new Ordering([1,2,3,4,5,6,7, 8])
    .withCriterium("odd", function(a, b) { return (a%2 > b%2) ? 1 : -1; })
    .withCriterium("natural", function(a, b) { return (a > b) ? 1 : -1; })
    .sortedAs("natural", true)
    .incrementallySortedAs("odd", false), 3
);
for(var i = 0; i < a.getPagesCount(); ++i) {
    console.log(JSON.stringify(a.getPage(i)));
}
*/