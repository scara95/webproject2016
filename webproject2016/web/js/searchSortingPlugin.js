function initSearchSortingPlugin(init) {
    var flaws = { false: '<i class="material-icons">keyboard_arrow_up</i>', true: '<i class="material-icons">keyboard_arrow_down</i>' };
    $("#searchSorting a").click(function () {
        var criterium = $(this).attr("data-criterium");
        
        var flaw = false;
        
        if(criterium === searchResults.currentMethod()) {
            flaw = !searchResults.currentFlaw();
        }
        
        searchResults.incrementalSort(criterium, flaw);
        
        $('#searchSorting').nextAll('div').remove();
        $("head style").remove();
        $("#searchSorting .active").children("i").remove();
        $("#searchSorting .active").removeClass("active");
        $(this).addClass("active");
        $(this).append(flaws[flaw]);
        
        buildResults(init);
    });
}
;