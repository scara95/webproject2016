/** 
 * This plugin controls the not-emptiness of the required field before the form submitting
 * @param init object containing all of the dynamically generated data
 */
function initNewRestaurantFormControlPlugin(init) {
    $("form").submit(function (e) {
        if ($("input[name=types]").val() === "[[]]") {
            e.preventDefault();
            $.alert("danger", init.strings.alertError, init.strings.msgNewRestaurantMissingType);
        } else if ($('input[type=radio][name=price]:checked').size() === 0) {
            e.preventDefault();
            $.alert("danger", init.strings.alertError, init.strings.msgNewRestaurantMissingPrice);
        } else if ($("#inputLat").val() === "") {
            e.preventDefault();
            $.alert("danger", init.strings.alertError, init.strings.msgNewRestaurantMissingAddress);
        } else if ($.trim($("#textArea").val()) === "") {
            e.preventDefault();
            $.alert("danger", init.strings.alertError, init.strings.msgNewRestaurantMissingDescription);
        } else if ($("#orariSelector input[type=time]").filter(function(){return ($(this).val()!=="");}).length > 0) {
            e.preventDefault();
            $.alert("danger", init.strings.alertError, init.strings.msgNewRestaurantPendingTimetable);
        }
    });
}