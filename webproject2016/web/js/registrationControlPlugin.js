/** 
 * This plugin manages the password testing for the registration
 * @param init object containing all of the dynamically generated data
 */
function initRegistrationControlPlugin (init) {
    $("#registerBtn").click(function () {
        var pwd = $("#inputPassword1").val();
        var pwd2 = $("#inputPassword2").val();
        if (pwd !== pwd2) {
            $.alert("danger", init.strings.alertError, init.strings.msgPasswordNotMatch+" "+init.strings.msgRetry);
        } else
        {
            var enoughRegex = new RegExp("(?=.{6,}).*", "g");
            if (enoughRegex.test(pwd)) {
                $("#registerSubmit").trigger("click");
            } else
            {
                $.alert("danger", init.strings.alertError, init.strings.msgPasswordWeak);
            }
        }
    });
}