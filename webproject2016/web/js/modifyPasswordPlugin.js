/**
 * This plugin manages the AJAX request of the modify password feature
 * @param init object containing all of the dynamically generated data
 */
function initModifyPasswordPlugin(init) {
    $("#editPassword #editPasswordBtn").click(function () {
        var newPwd = $("#inputPassword1").val();
        var newPwd2 = $("#inputPassword2").val();
        var oldPwd = $("#inputActualPassword").val();
        if (newPwd !== newPwd2) {
            $.alert("danger", init.strings.alertError, init.strings.msgPasswordNotMatch + " " + init.strings.msgRetry);
        } else {
            var enoughRegex = new RegExp("(?=.{6,}).*", "g");
            if (enoughRegex.test(newPwd)) {
                $.ajax({
                    method: "POST",
                    url: init.baseURL + "/modify_password",
                    data: {new : newPwd, password: oldPwd}
                }).done(function () {
                    $("#editPassword").modal("toggle");
                    $("#editPassword form").get(0).reset();
                    $.alert("success", init.strings.alertSuccess, init.strings.msgPasswordModified);
                }).fail(function () {
                    $("#inputActualPassword").val("").parent().parent().addClass("has-error");
                    $.alert("danger", init.strings.alertError, init.strings.msgPasswordWrong + " " + init.strings.msgRetry);
                });
            } else {
                $.alert("danger", init.strings.alertError, init.strings.msgPasswordWeak);
            }
        }
    });
}