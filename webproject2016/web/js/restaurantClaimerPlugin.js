/**
 * This plugin manages the AJAX request of the restaurant claiming feature
 * @param init object containing all of the dynamically generated data
 */
function initRestaurantClaimerPlugin(init) {
    $("#restaurantClaimer").click(function () {
        var id = init.restaurantId;
        $.ajax({
            method: "POST",
            url: init.baseURL + "/claim_restaurant",
            data: {id: id}
        }).done(function () {
            $(".panel-body h2 small").html(init.strings.msgClaimedBy + " <a href='" + init.baseURL + "/profile/" + init.userId + "'>" + init.fullname + "</a>");
            $.alert("success", init.strings.alertSuccess, init.strings.msgRestaurantClaimed);
            $.getJSON(init.baseURL + "/notifications", {}, renderNotifications);
        }).fail(function (request, status, error) {
            if (request.status === 403) {
                $.alert("danger", init.strings.alertError, init.strings.msgRestaurantAlreadyClaimed);
                window.setTimeout(function () {
                    location.reload(true);
                }, 2000);
            } else {
                $.alert("danger", init.strings.alertError, init.strings.msgGenericError + " " + init.strings.msgRetry);
            }
        });
    });
}