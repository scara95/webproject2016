/** 
 * This plugin manages the stylings for the password insertion errors
 * @param init object containing all of the dynamically generated data
 */
function initPasswordCheckPlugin (init) {
    $("#inputPassword1, #inputPassword2").keyup(function () {
        var pwd = $("#inputPassword1").val();
        var pwd2 = $("#inputPassword2").val();
        if (pwd !== pwd2) {
            $("#inputPassword1, #inputPassword2").parents(".form-group").removeClass("has-success").addClass("-fw-has-error");
        } else {
            $("#inputPassword1, #inputPassword2").parents(".form-group").removeClass("-fw-has-error").addClass("has-success");
        }
        var strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g"); // 8 caratteri, di cui almeno 1 maiuscola, 1 minuscola, 1 numero, 1 simbolo
        var mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g"); // almeno 7 caratteri, presi da almeno due dei 3 tipi: maiusc, minusc, numeri
        var enoughRegex = new RegExp("(?=.{6,}).*", "g"); // 6 caratteri
        if (false === enoughRegex.test(pwd)) {
            $('#pwdStrength').removeClass("text-warning text-info text-success").addClass("text-danger").text(init.strings.msgPasswordCheckTooWeak);
        } else if (strongRegex.test(pwd)) {
            $('#pwdStrength').removeClass("text-danger text-warning text-info").addClass("text-success").text(init.strings.msgPasswordCheckStrong);
        } else if (mediumRegex.test(pwd)) {
            $('#pwdStrength').removeClass("text-danger text-warning text-success").addClass("text-info").text(init.strings.msgPasswordCheckMedium);
        } else {
            $('#pwdStrength').removeClass("text-danger text-info text-success").addClass("text-warning").text(init.strings.msgPasswordCheckWeak);
        }
    });
}