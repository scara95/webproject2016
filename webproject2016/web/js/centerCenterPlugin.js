/** 
 * This plugin positions a DOM element in the center of its relative parent
 * @param init object containing all of the dynamically generated data
 */
function initCenterCenterPlugin (init) {
    var h = $(init.selector).height()/2;
    var w = $(init.selector).width()/2;
    $(init.selector).css({
        top: "calc(50% - "+h+"px)",
        left: "calc(50% - "+w+"px)"
    });
}