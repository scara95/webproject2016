function TTDate(day, hour, minute) {
    this.day = day;
    this.hour = hour;
    this.minute = minute;

    /** 
     * This method converts a Date into its minutes representation
     */
    this.dateToMinutes = function () {
        return (this.day * 60 * 24 + this.hour * 60 + this.minute);
    };

    /** 
     * This function checks if a given date is the same as this one
     * @param d a Date
     */
    this.equalsDate = function (d) {
        if (this.dateToMinutes() === d.dateToMinutes()) {
            return true;
        } else
            return false;
    };

    /** 
     * This function checks if this Date is previous than another one
     * @param d a Date
     */
    this.lessThanDate = function (d) {
        if (this.dateToMinutes() < d.dateToMinutes()) {
            return true;
        } else
            return false;
    };

    /** 
     * This function checks if this Date is greater than or equal to another one
     * @param d a Date
     */
    this.greaterThanEqualsDate = function (d) {
        return !(this.lessThanDate(d));
    };

    this.toObjLiteral = function () {
        return {d: this.day, h: this.hour, m: this.minute};
    };
    
    this.toStringifiedObj = function () {
        return JSON.stringify(this.toObjLiteral());
    };

}