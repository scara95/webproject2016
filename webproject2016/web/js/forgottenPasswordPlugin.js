/**
 * This plugin manages the AJAX request of the password recovery feature
 * @param init object containing all of the dynamically generated data
 */
function initForgottenPasswordPlugin(init) {
    $("#forgotPwd #forgotPwdBtn").click(function () {
        var email = $("#inputEmailForgotten").val();
        if (email.length === 0)
        {
            $.alert("danger", init.strings.alertError, init.strings.msgEmailMissing);
        } else {
            $.ajax({
                method: "POST",
                url: init.baseURL + "/password_forgotten",
                data: {mail: email}
            }).done(function () {
                $("#forgotPwd").modal("toggle");
                $.alert("success", init.strings.alertSuccess, init.strings.msgPasswordRecovered);
            }).fail(function (request, status, error) {
                if (request.status === 404)
                {
                    $.alert("danger", init.strings.alertError, init.strings.msgEmailNotFound + " " + init.strings.msgRetry);
                } else {
                    $.alert("danger", init.strings.alertError, init.strings.msgGenericError + " " + init.strings.msgRetry);
                }
            });
        }
    });
}