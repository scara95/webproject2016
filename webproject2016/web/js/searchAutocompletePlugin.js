/** 
 * A global variable that will contain the full dataset
 */
var res = [];

/** 
 * This plugin manages the logic for autocomplete
 * @param init object containing all of the dynamically generated data
 */
function initSearchAutocompletePlugin(init) {
    $.ajax({
        dataType: "json",
        url: init.baseURL + "/search_json/ALL",
        data: {}
    }).done(function (x) {
        for (var i = 0; i < x.length; ++i) {
            res.push({label: x[i].name, name: x[i].name, description: x[i].descr, link: init.baseURL + "/restaurant/" + x[i].id});
            /*res.push({label: x[i].name, category: init.strings.msgAutocompleteRestaurants, link: init.baseURL + "/restaurant/" + x[i].id});
             res.push({label: x[i].descr.substring(0, 75) + " [...]", category: init.strings.msgAutocompleteDescriptions, link: init.baseURL + "/restaurant/" + x[i].id});*/
        }
        res.sort(function (a, b) {
            /*if (a.category !== b.category) {
             return (a.category === init.strings.msgAutocompleteRestaurants) ? -1 : 1;
             }*/
            var x = a.label.toLowerCase(), y = b.label.toLowerCase();
            return x < y ? -1 : x > y ? 1 : 0;
        });
    });
}

/** 
 * This allows to hide or unhide the autocomplete menu
 * @param e the click event
 */
$(document).click(function (e) {
    if (!$(e.target).is('#autoCompleter') && !$(e.target).is('#search #name')) {
        $("#autoCompleter").css({display: "none"});
    } else if ($("#autoCompleter").children().length > 0) {
        $("#autoCompleter").css({display: "block"});
    }
});

/** 
 * As you digit, it updates.
 */
$("#search #name").keyup(function () {
    $("#autoCompleter").empty();
    var attuale = $("#search #name").val();

    var tmp = res.slice().filter(function (val) {
        return (val.name.toLowerCase().indexOf(attuale) !== -1 || val.description.toLowerCase().indexOf(attuale) !== -1);
        // return val.label.toLowerCase().indexOf(attuale) !== -1;
    });
    if (tmp.length > 0 && attuale.length > 0) {
        $("#autoCompleter").css({display: "block"});
        //var category = "";
        for (var i = 0; i < tmp.length; i++) {
            /*if (category !== tmp[i].category) {
             $("#autoCompleter").append("<li class='category'>" + tmp[i].category + "</li>");
             category = tmp[i].category;
             }*/
            $("#autoCompleter").append("<li><a href=" + tmp[i].link + ">" + tmp[i].label + "</a></li>");
        }
    } else {
        $("#autoCompleter").css({display: "none"});
    }
});