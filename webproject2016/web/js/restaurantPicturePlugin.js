/** 
 * This plugin manages the visualization of the images of the restaurant
 */
$(window).on({
    load: function () {
        $(".img-gallery").each(function (index) {
            var link = $(this).attr("src");
            $(this).addClass("img-gallery-" + index);
            $(document.createElement("style")).text(".img-gallery-" + index + " {background: url(\"" + link + "\") center center no-repeat;}").appendTo("head");
            $(this).height($(this).width());
            var self = this;

            $("<img/>").attr("src", link).load(function () {
                $(self).attr("max-height", this.height);
            });
        });
    },
    resize: function () {
        $(".img-gallery").each(function () {
            $(this).height(Math.min($(this).width(), $(this).attr("max-height")));
        });
    }
});