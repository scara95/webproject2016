/**
 * This plugin manages the map that can be used for the advanced search
 * @param init object containing all of the dynamically generated data
 */
function initSearchMapPlugin(init) {
    var fieldSearchNearTypeMe = init.strings.fieldSearchNearTypeMe;
    var userPos;
    var geoSuccess = function (position) {
        $("#nearType").append("<option value=\"me\">" + fieldSearchNearTypeMe + "</option>");

        // Do magic with location
        userPos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    };

    navigator.geolocation.getCurrentPosition(geoSuccess);
    var startingLatlng = new google.maps.LatLng(41.8719400, 12.5673800);
    var map = new google.maps.Map(document.getElementById('advSearchMap'), {
        zoom: 5,
        styles: [
            {
                featureType: "poi.business",
                stylers: [
                    {visibility: "off"}
                ]
            }
        ]
    });
    var init = false;

    var input = /** @type {!HTMLInputElement} */(
            document.getElementById('near'));

    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

    var infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });

    var circle = new google.maps.Circle({
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 1,
        map: map
    });

    autocomplete.addListener('place_changed', function () {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();

        $.ajax({
            type: "GET",
            url: "https://maps.googleapis.com/maps/api/geocode/json?address=" + place.formatted_address + "&key=AIzaSyA_Xa5O4PLHCnlWcHfnRwlZYJwEoF4j-kw&language=en",
            dataType: "json"
        }).done(function (json) {
            var lat = json.results[0].geometry.location.lat;
            var lon = json.results[0].geometry.location.lng;
            var addrComp = json.results[0].address_components;
            for (var i = 0; i < addrComp.length; ++i) {
                if (addrComp[i].types[0] === "locality") {
                    var city = addrComp[i].long_name;
                }
                if (addrComp[i].types[0] === "administrative_area_level_2") {
                    var province = addrComp[i].short_name;
                }
                if (addrComp[i].types[0] === "country") {
                    var state = addrComp[i].short_name;
                }
            }

            $("#inputLat").val(lat);
            $("#inputLon").val(lon);
            $("#inputProvince").val(province);
            $("#inputCity").val(city);
            $("#inputState").val(state);
        });

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
        }
        init = true;
        marker.setIcon(/** @type {google.maps.Icon} */({
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
        }));
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        var address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }

        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
        infowindow.open(map, marker);
        circle.setCenter(marker.position);
        if ($("#nearType").val() === "near" || $("#nearType").val() === "me") {
            circle.setRadius($("#radius").val() * 1000);
        } else {
            circle.setRadius(0);
        }

    });

    $("#radius").on('input', function () {
        circle.setCenter(marker.position);
        var value = $("#nearType").val();
        if (value === "me" || value === "near")
        {
            circle.setRadius($("#radius").val() * 1000);
        }
    });

    $("#nearType").change(function () {
        var value = $(this).val();
        if (value === "none") {
            $("#near").prop({disabled: true, required: false}).val("");
            $("#fgRadius, #fgAdvSearchMap").slideUp(300);
            circle.setRadius(0);
        } else if (value === "me") {
            $("#near").prop({disabled: true, required: false}).val("");
            $("#fgRadius, #fgAdvSearchMap").slideDown(300);
            $.ajax({
                type: "GET",
                url: "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + userPos.toUrlValue() + "&key=AIzaSyA_Xa5O4PLHCnlWcHfnRwlZYJwEoF4j-kw&language=en",
                dataType: "json"
            }).done(function (json) {
                var lat = json.results[0].geometry.location.lat;
                var lon = json.results[0].geometry.location.lng;
                var addrComp = json.results[0].address_components;
                for (var i = 0; i < addrComp.length; ++i) {
                    if (addrComp[i].types[0] === "locality") {
                        var city = addrComp[i].long_name;
                    }
                    if (addrComp[i].types[0] === "administrative_area_level_2") {
                        var province = addrComp[i].short_name;
                    }
                    if (addrComp[i].types[0] === "country") {
                        var state = addrComp[i].short_name;
                    }
                }

                $("#inputLat").val(lat);
                $("#inputLon").val(lon);
                $("#inputProvince").val(province);
                $("#inputCity").val(city);
                $("#inputState").val(state);
            });
            google.maps.event.trigger(map, 'resize');
            infowindow.close();
            map.setCenter(userPos);
            map.setZoom(12);
            init = true;
            marker.setPosition(userPos);
            circle.setCenter(marker.position);
            circle.setRadius($("#radius").val() * 1000);
        } else if (value === "near") {
            $("#near").prop({disabled: false, required: true});
            $("#fgRadius, #fgAdvSearchMap").slideDown(300);
            google.maps.event.trigger(map, 'resize');
            if (!init)
                map.setCenter(startingLatlng);
        } else {
            $("#near").prop({disabled: false, required: true});
            $("#fgRadius").slideUp(300);
            $("#fgAdvSearchMap").slideDown(300);
            google.maps.event.trigger(map, 'resize');
            if (!init)
                map.setCenter(startingLatlng);
            circle.setRadius(0);
        }
    });

    $("#expander").click(function () {
        $("#advSearch").slideToggle(300);
        var a = $(this).hasClass("active");
        var obj = a ? {"-webkit-transform": "rotate(0deg)",
            "transform": "rotate(0deg)",
            "-webkit-transition": "all 0.5s",
            "transition": "all 0.5s"} : {"-webkit-transform": "rotate(180deg)",
            "transform": "rotate(180deg)",
            "-webkit-transition": "all 0.5s",
            "transition": "all 0.5s"};
        $(this).css(obj).toggleClass("active");
        google.maps.event.trigger(map, 'resize');
        map.setCenter(startingLatlng);
    });
}