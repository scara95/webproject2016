/**
 * This plugin manages the AJAX request of the thumb up/down feature
 * @param init object containing all of the dynamically generated data
 */
function initThumbsPlugin(init) {
    $(".thumbs i").click(function () {
        var self = this;
        var parent = $(this).parents(".thumbs");
        var rate_user = parent.attr("rate_user");
        var rate_restaurant = parent.attr("rate_restaurant");
        var rate_day = parent.attr("rate_day");
        if ($(this).hasClass("thumb-up-clickable")) {
            var what = "up";
            $.ajax({
                method: "POST",
                url: init.baseURL + "/rate_rate",
                data: {rate_user: rate_user, rate_restaurant: rate_restaurant, rate_day: rate_day, what: what}
            }).done(function() {
                $(self).removeClass("thumb-up-clickable").addClass("thumb-up-voted");
                    $(self).siblings(".thumb-up-num").text(parseInt($(self).siblings(".thumb-up-num").text()) + 1);
                    var quello = $(self).siblings(".thumb-down-voted");
                    if (quello.length > 0) {
                        quello.removeClass("thumb-down-voted").addClass("thumb-down-clickable");
                        $(self).siblings(".thumb-down-num").text(parseInt($(self).siblings(".thumb-down-num").text()) - 1);
                    }
            }).fail(function(){
                $.alert("danger", init.strings.alertError, init.strings.msgGenericError+" "+init.strings.msgRetry);
            });
        } else if ($(this).hasClass("thumb-down-clickable")) {
            var what = "down";
            $.ajax({
                method: "POST",
                url: init.baseURL + "/rate_rate",
                data: {rate_user: rate_user, rate_restaurant: rate_restaurant, rate_day: rate_day, what: what}
            }).done(function(){
                $(self).removeClass("thumb-down-clickable").addClass("thumb-down-voted");
                    $(self).siblings(".thumb-down-num").text(parseInt($(self).siblings(".thumb-down-num").text()) + 1);
                    var quello = $(self).siblings(".thumb-up-voted");
                    if (quello.length > 0)
                    {
                        quello.removeClass("thumb-up-voted").addClass("thumb-up-clickable");
                        $(self).siblings(".thumb-up-num").text(parseInt($(self).siblings(".thumb-up-num").text()) - 1);
                    }
            }).fail(function(){
                $.alert("danger", init.strings.alertError, init.strings.msgGenericError + " " + init.strings.msgRetry);
            });
        }
    });
}