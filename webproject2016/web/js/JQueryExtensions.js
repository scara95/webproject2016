$.extend({
    /** 
     * This method is a simple redirect that creates a form and passes with POST some values 
     * @param location location to be redirected to
     * @param args object literal with the keys and values to be posted
     */
    redirectPost: function (location, args)
    {
        /*var form = $(document.createElement("form")).attr({method:"post", action: location});
        for(var key in args) {
            $(document.createElement("input")).attr({type:"hidden", name: key, value: args[key]}).appendTo("form");
        }
        form.submit();*/
        
        var form = $('<form></form>');
        form.attr("method", "post");
        form.attr("action", location);

        $.each(args, function (key, value) {
            var field = $('<input></input>');

            field.attr("type", "hidden");
            field.attr("name", key);
            field.attr("value", value);

            form.append(field);
        });
        $(form).appendTo('body').submit();
    },
    /**
     * This method is the same as redirectPost, but it passes the arguments with a GET method 
     * @param location location to be redirected to
     * @param args object literal with the keys and values to be passed with get
     */
    redirectGet: function (location, args)
    {
        /*
        var form = $(document.createElement("form")).attr({method:"get", action: location});
        for(var key in args) {
            $(document.createElement("input")).attr({type:"hidden", name: key, value: args[key]}).appendTo("form");
        }
        form.submit();
        */
        
        var form = $('<form></form>');
        form.attr("method", "get");
        form.attr("action", location);

        $.each(args, function (key, value) {
            var field = $('<input></input>');

            field.attr("type", "hidden");
            field.attr("name", key);
            field.attr("value", value);

            form.append(field);
        });
        $(form).appendTo('body').submit();
    },
    /** 
     * This method redirects to a specified location preventing the user to go back to the previous page, instead it's skipped 
     * @param location location to be redirected to
     */
    redirect: function (location)
    {
        window.location.replace(location);
    },
    /** 
     * This method redirects to a specific location 
     * @param location location to be redirected to
     */
    followLink: function (location)
    {
        window.location.href = location;
    },
    /** 
     * This method creates a bootstrap-compatible alert, which positions relatively to other alerts previously created, and autoremoves after 3 seconds. 
     * @param type bootstrap class type
     * @param msgHead string with the head of the alert
     * @param msgBody string with the body of the alert
     */
    alert: function (type, msgHead, msgBody) {
        var alertNumber = $(".alert").length;
        
        var alertContainer = $(document.createElement("div")).addClass("alert alert-dismissible alert-" + type + " -fw-alert").attr({id: "alert-" + alertNumber}).appendTo("body");
        $(document.createElement("button")).addClass("close").attr({type:"button", "data-dismiss":"alert"}).text("×").appendTo(alertContainer);
        if(msgHead !== "") {
            $(document.createElement("strong")).text(msgHead).appendTo(alertContainer);
            $(document.createElement("br")).appendTo(alertContainer);
        }
        $(document.createElement("span")).text(msgBody).appendTo(alertContainer);
        
        var top = 15;
        if (alertNumber !== 0) {
            var prev = $("#alert-" + (alertNumber - 1));
            top += prev.outerHeight() + prev.position().top;
        }

        $("#alert-" + alertNumber).css("top", top);
        setTimeout(function () {
            $("#alert-" + alertNumber).slideUp(300, function () {
                $(this).alert('close');
            });
        }, 3000);
    },
    /** 
     * This method checks if the supplied parameter name exists within the parameters in the current URI, and returns it if it exists or null if it doesn't 
     * @param name the name of the parameter
     * */
    urlParam: function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results === null) {
            return null;
        } else {
            return decodeURI(results[1]) || 0;
        }
    }
});