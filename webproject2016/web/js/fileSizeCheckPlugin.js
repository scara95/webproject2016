/** 
 * This plugin checks the filesize of an uploaded files, and prevents the submitting if it's too big
 * @param init object containing all of the dynamically generated data
 */
function initFileSizeCheckPlugin(init) {
    var fileSize = 0;
    $("#inputFile").on("change", function () {
        fileSize = this.files[0].size;
    });

    $(init.selector).submit(function (e) {
        if (fileSize > 2097152)
        {
            $.alert("danger", init.strings.alertError, init.strings.msgImageNotUploaded+" "+init.strings.msgRetry);
            e.preventDefault();
        }
    });
}