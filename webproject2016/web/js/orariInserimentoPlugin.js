/** 
 * Global variable for the timetable tracking
 */
var actualOrari = [];

/**
 * This function checks if two intervals of Dates are in conflict. 
 * Given that orari[index] is a, orari[index+1] is b, from is c and to is d, 
 * checking for no conflict would be saying that:
 *      (a<b & b<c & d<a) | (c<d & d<a & b<c) | (a<b & c<d & (b<c | d<a)) 
 * So checking for a conflict is:
 *      (a >= b && (b >= c || c >= d || d >= a)) 
 *          || (b >= c && (c >= d || d >= a)) 
 *          || (c >= d && d >= a)
 * @param orari the complete timetable up to now
 * @param index the index in which i am recursively checking
 * @param from the new starting point
 * @param to the new end point
 * @param init object containing all of the dynamically generated data
 */
function checkForConflicts(orari, index, from, to, init) {
    if (typeof orari[index] === "undefined") {
        return false;
    } else {
        var aStart = new TTDate(orari[index].d, orari[index].h, orari[index].m);
        var aEnd = new TTDate(orari[index + 1].d, orari[index + 1].h, orari[index + 1].m);
        if (from.equalsDate(to)) {
            $.alert("danger", init.strings.alertError, init.strings.msgSameDate + " " + init.strings.msgRetry);
            return true;
        } else if (aStart.greaterThanEqualsDate(aEnd)
                && (aEnd.greaterThanEqualsDate(from) || from.greaterThanEqualsDate(to) || to.greaterThanEqualsDate(aStart))
                || (aEnd.greaterThanEqualsDate(from) && (from.greaterThanEqualsDate(to) || to.greaterThanEqualsDate(aStart)))
                || (from.greaterThanEqualsDate(to) && to.greaterThanEqualsDate(aStart))) {
            $.alert("danger", init.strings.alertError, init.strings.msgDateConflict + " " + init.strings.msgRetry);
            return true;
        }
        return checkForConflicts(orari, index + 2, from, to);
    }
}

/** 
 * This function is the one called when the button for the insertion is clicked
 * @param index the day in which it's inserting
 * @param init object containing all of the dynamically generated data
 */
function add_day(index, init) {
    var $from = $("input[name=" + index + "-from]");
    var $to = $("input[name=" + index + "-to]");

    if ($from.val() === "" || $to.val() === "") {
        $.alert("danger", init.strings.alertError, init.strings.msgInsertDate);
    } else {
        var fromString = $from.val().split(":");
        var toString = $to.val().split(":");
        var fromInt = fromString.map(Number);
        var toInt = toString.map(Number);

        var day2 = index;
        if (fromInt[0] > toInt[0] || (fromInt[0] === toInt[0] && fromInt[1] > toInt[1]))
            day2 = (index + 1) % 7;
        var dateFrom = new TTDate(index, fromInt[0], fromInt[1]);
        var dateTo = new TTDate(day2, toInt[0], toInt[1]);
        if (!checkForConflicts(actualOrari, 0, dateFrom, dateTo, init)) {
            var inserted = false;
            for (var i = 0; i < actualOrari.length && !inserted; ++i) {
                var cur = new TTDate(actualOrari[i].d, actualOrari[i].h, actualOrari[i].m);
                if (dateFrom.lessThanDate(cur)) {
                    actualOrari.splice(i, 0, JSON.parse(dateFrom.toStringifiedObj()));
                    actualOrari.splice(i + 1, 0, JSON.parse(dateTo.toStringifiedObj()));
                    inserted = true;
                }
            }
            if (!inserted) {
                actualOrari.push(JSON.parse(dateFrom.toStringifiedObj()));
                actualOrari.push(JSON.parse(dateTo.toStringifiedObj()));
            }

            $("[day=" + index + "]").prepend(
                    "<div class=\"col-xs-12\">" +
                    fromString[0] + ":" + fromString[1] + " - " + toString[0] + ":" + toString[1] +
                    ((day2 !== index) ? " " + init.strings.msgDayOverflow : "") +
                    "   <a onClick='remove_day(this, " + dateFrom.toStringifiedObj() + ")' style= \"cursor:pointer;\">&times;</a>" +
                    "</div>"
                    );

            $from.val("");
            $to.val("");
        }
    }
}

/** 
 * This function is the one called when removing an interval
 * @param elem the element clicked
 * @param from the starting time of the deleting element
 */
function remove_day(elem, from) {
    $(elem).parent().remove();
    var date = new TTDate(from.d, from.h, from.m);

    for (var i = 0; i < actualOrari.length; i = i + 2) {
        var now = new TTDate(actualOrari[i].d, actualOrari[i].h, actualOrari[i].m);
        if (date.equalsDate(now)) {
            actualOrari.splice(i, 2);
        }
    }
}

/** 
 * This plugin manages the insertion and editing of the timetables for opening-hours of the restaurants
 * @param init object containing all of the dynamically generated data
 */
function initOrariInserimentoPlugin(init) {
    if (typeof $("#editRestaurant #orariSelector [name=timetable]").val() !== "undefined") {
        actualOrari = JSON.parse($("#editRestaurant #orariSelector [name=timetable]").val());
    }
    for (var i = 0; i < init.strings.days.length; ++i) {
        var formGroup = $(document.createElement("div")).addClass("form-group").css({"margin-top": 0}).appendTo("#orariSelector");
        $(document.createElement("label")).addClass("col-xs-2 control-label").text(init.strings.days[i]).appendTo(formGroup);
        var dayContainer = $(document.createElement("div")).addClass("col-xs-10 input-group").attr({day: i}).appendTo(formGroup);
        var dayOpen = $(document.createElement("div")).addClass("col-xs-6 input-group").css({float: "left"}).appendTo(dayContainer);
        $(document.createElement("span")).addClass("input-group-addon").text(init.strings.msgOpensAt).appendTo(dayOpen);
        $(document.createElement("input")).addClass("form-control").attr({type: "time", name: i + "-from"}).appendTo(dayOpen);
        var dayClose = $(document.createElement("div")).addClass("col-xs-6 input-group").css({float: "left"}).appendTo(dayContainer);
        $(document.createElement("span")).addClass("input-group-addon").text(init.strings.msgClosesAt).appendTo(dayClose);
        $(document.createElement("input")).addClass("form-control").attr({type: "time", name: i + "-to"}).appendTo(dayClose);
        var confirm = $(document.createElement("span")).addClass("input-group-addon").appendTo(dayClose);
        var btnContainer = $(document.createElement("div")).addClass("btn btn-default btn-fab btn-fab-mini").
                attr({onClick: "add_day(" + i + ", " + JSON.stringify(init) + ")", id: i + "-btn"}).appendTo(confirm);
        $(document.createElement("i")).addClass("material-icons").text("done").appendTo(btnContainer);
    }

    for (var i = 0; i < actualOrari.length; i = i + 2) {
        var inizio_h = (parseInt(actualOrari[i].h) < 10) ? "0" + actualOrari[i].h : actualOrari[i].h;
        var inizio_m = (parseInt(actualOrari[i].m) < 10) ? "0" + actualOrari[i].m : actualOrari[i].m;
        var fine_h = (parseInt(actualOrari[i + 1].h) < 10) ? "0" + actualOrari[i + 1].h : actualOrari[i + 1].h;
        var fine_m = (parseInt(actualOrari[i + 1].m) < 10) ? "0" + actualOrari[i + 1].m : actualOrari[i + 1].m;

        if (actualOrari[i].d !== actualOrari[i + 1].d) {
            var text = inizio_h + ":" + inizio_m + " - " + fine_h + ":" + fine_m + " " + init.strings.msgDayOverflow;
        } else {
            var text = inizio_h + ":" + inizio_m + " - " + fine_h + ":" + fine_m;
        }

        var newRow = $(document.createElement("div")).addClass("col-xs-12")
                .text(text).prependTo("#orariSelector [day=" + actualOrari[i].d + "]");
        $(document.createElement("a"))
                .attr({style: "cursor:pointer;", onClick: "remove_day(this, " + JSON.stringify(actualOrari[i]) + ")"}).text("×").appendTo(newRow);
    }
}

/** 
 * This manages the actual submission, passing to the form the global variable
 */
$("#addRestaurant, #editRestaurant form").submit(function () {
    $("#orariSelector").find("input[name=timetable]").val(JSON.stringify(actualOrari));
});