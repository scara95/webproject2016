/** 
 * This plugin allows an administrator to accept or refuse an answer 
 * @param init object containing all of the dynamically generated data
 */
function initAcceptAnswerPlugin(init) {
    $("[action-accept]").click(function () {
        var user_id = init.userId;
        var restaurant_id = init.restaurantId;
        var day = init.day;
        var accept = $(this).attr("action-accept");
        $("[action-accept]").slideUp(300);
        $.ajax({
            method: "POST",
            url: init.baseURL + "/accept_rate_answer",
            data: {user_id: user_id, restaurant_id: restaurant_id, day: day, accept: accept}
        }).done(function () {
            $.alert("success", init.strings.alertSuccess, init.strings.msgAnswerAccepted);
            $.getJSON(init.baseURL + "/notifications", {}, renderNotifications);
            if (accept === "refuse")
            {
                $("#answer").slideUp(300);
            }
        }).fail(function () {
            $.alert("danger", init.strings.alertError, init.strings.msgAnswerNotAccepted);
        });
    });
}