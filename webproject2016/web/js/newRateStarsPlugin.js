/** 
 * This plugin manages the star-rating interface method
 */
$(window).on("load", function () {
    $("#newRateStars").hover(function () {
        $("#newRateStars a").hover(function () {
            $(this).prevAll().andSelf().addClass("star-selected");
            $(this).nextAll().removeClass("star-selected");
        }).click(function () {
            $("#newRateStarsInput").val($("#newRateStars a").index(this) + 1);
        });
    }, function () {
        var rate = $("#newRateStarsInput").val();
        $("#newRateStars a").slice(0, rate).addClass("star-selected");
        $("#newRateStars a").slice(rate).removeClass("star-selected");
    });

});