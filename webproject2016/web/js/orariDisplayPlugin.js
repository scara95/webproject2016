/** 
 * This plugin manages the displaying of the timetables for opening-hours of the restaurants
 * @param init object containing all of the dynamically generated data
 * @return the string version of the timetable to be used in the QR
 */
function initOrariDisplayPlugin(init) {
    var continues = false;
    var res = "";
    for (var i = 0; i < init.orari.length; i += 2) {
        var inizio = init.orari[i];
        var fine = init.orari[i + 1];
        var inizio_h = (parseInt(inizio.h) < 10) ? "0" + inizio.h : inizio.h;
        var inizio_m = (parseInt(inizio.m) < 10) ? "0" + inizio.m : inizio.m;
        var fine_h = (parseInt(fine.h) < 10) ? "0" + fine.h : fine.h;
        var fine_m = (parseInt(fine.m) < 10) ? "0" + fine.m : fine.m;
        if (!continues) {
            $(init.selector).append(init.strings.days[inizio.d] + ": ");
            res += encodeURIComponent(init.strings.days[inizio.d] + ": ");
        }
        if (inizio.d === fine.d) {
            $(init.selector).append(inizio_h + ":" + inizio_m + " - " + fine_h + ":" + fine_m);
            res += encodeURIComponent(inizio_h + ":" + inizio_m + " - " + fine_h + ":" + fine_m);
        } else {
            $(init.selector).append(inizio_h + ":" + inizio_m + " - 23:59<br>");
            $(init.selector).append(init.strings.days[fine.d] + ": 00:00 - " + fine_h + ":" + fine_m);
            res += encodeURIComponent(inizio_h + ":" + inizio_m + " - 23:59") + "%0D%0A" + encodeURIComponent(init.strings.days[fine.d] + ": 00:00 - " + fine_h + ":" + fine_m);
        }
        if (typeof init.orari[i + 2] !== "undefined") {
            if (init.orari[i + 2].d === fine.d) {
                $(init.selector).append(" | ");
                res += encodeURIComponent(" | ");
                continues = true;
            } else {
                $(init.selector).append("<br>");
                res += "%0D%0A";
                continues = false;
            }
        }
    }
    return res;
}