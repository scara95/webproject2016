/**
 * This plugin allows an administrator to accept or refuse the report of an image
 * @param init object containing all of the dynamically generated data
 */
function initAcceptReportedPicturePlugin(init) {
    $("[action='keep']").click(function () {
        var id = init.imgId;
        $("#pulsantoloni").slideUp(300);
        $.ajax({
            method: "POST",
            url: init.baseURL + "/show_reported_image/keep",
            data: {id: id}
        }).done(function () {
            $.alert("success", init.strings.alertSuccess, init.strings.msgImageAccepted);
            $.getJSON(init.baseURL + "/notifications", {}, renderNotifications);
        }).fail(function () {
            $.alert("danger", init.strings.alertError, init.strings.msgImageNotAccepted);
        });
    });

    $("[action='delete']").click(function () {
        var id = init.imgId;
        $("#pulsantoloni").slideUp(300);
        $.ajax({
            method: "POST",
            url: init.baseURL + "/show_reported_image/delete",
            data: {id: id}
        }).done(function () {
            $.alert("success", init.strings.alertSuccess, init.strings.msgImageRejected);
            $("#reported-image").slideUp(300);
            $.getJSON(init.baseURL + "/notifications", {}, renderNotifications);
        }).fail(function () {
            $.alert("danger", init.strings.alertError, init.strings.msgImageNotRejected);
        });
    });
}