DROP DATABASE IF EXISTS webproject2016;
CREATE DATABASE webproject2016;
\c webproject2016
\i 'schema\\schema.sql'
\copy users FROM 'data\\users.csv' DELIMITER ';' CSV
\copy types FROM 'data\\types.csv' DELIMITER ';' CSV
\copy restaurants FROM 'data\\restaurants.csv' DELIMITER ';' CSV
\copy cooks FROM 'data\\cooks.csv' DELIMITER ';' CSV
\copy rates FROM 'data\\rates.csv' DELIMITER ';' CSV
\copy rates_rates FROM 'data\\rates_rates.csv' DELIMITER ';' CSV
\copy pictures FROM 'data\\pictures.csv' DELIMITER ';' CSV
ALTER SEQUENCE users_id_seq RESTART 71;
ALTER SEQUENCE restaurants_id_seq RESTART 61;
ALTER SEQUENCE pictures_id_seq RESTART 155;

