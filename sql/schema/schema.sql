CREATE TYPE price_t AS ENUM ('LOW', 'MID', 'HIGH');

CREATE FUNCTION dater() RETURNS trigger AS $$
  BEGIN
    NEW.modified := CURRENT_TIMESTAMP;
    RETURN NEW;
  END;
$$ LANGUAGE plpgsql;

CREATE TABLE users (
  id BIGSERIAL PRIMARY KEY,
  email TEXT UNIQUE,
  name TEXT,
  surname TEXT,
  password TEXT,
  avatar TEXT,
  is_adm BOOLEAN
);

CREATE FUNCTION is_admin(BIGINT) RETURNS BOOLEAN
  AS $$ SELECT is_adm FROM users WHERE id=$1 $$
  LANGUAGE SQL;

CREATE TABLE restaurants (
  id BIGSERIAL PRIMARY KEY,
  name TEXT,
  descr TEXT,
  site TEXT,
  lon DECIMAL CHECK( -180 <= lon AND lon <= 180 ),
  lat DECIMAL CHECK( -90 <= lat AND lat <= 90 ),
  state TEXT,
  province TEXT,
  city TEXT,
  price price_t,
  timetable jsonb,
  owner BIGINT REFERENCES users(id),
  ownerapprover BIGINT REFERENCES users(id)
                 CHECK( ownerapprover IS NULL OR is_admin(ownerapprover)),
  modified TIMESTAMP DEFAULT now()
);

CREATE TRIGGER rates_dater BEFORE UPDATE OF owner ON restaurants FOR EACH ROW EXECUTE PROCEDURE dater();

CREATE TABLE types (
  type TEXT PRIMARY KEY
);

CREATE TABLE cooks (
  restaurant_id BIGINT REFERENCES restaurants(id),
  type_id TEXT REFERENCES types(type),
  PRIMARY KEY(restaurant_id, type_id)
);

CREATE TABLE pictures (
  id BIGSERIAL PRIMARY KEY,
  file TEXT,
  restaurant_id BIGINT REFERENCES restaurants(id),
  nvs BOOLEAN,
  modified TIMESTAMP DEFAULT now()
);

CREATE TRIGGER pictures_dater BEFORE UPDATE OF nvs ON pictures FOR EACH ROW EXECUTE PROCEDURE dater();

CREATE TABLE rates (
  user_id BIGINT REFERENCES users(id),
  restaurant_id BIGINT REFERENCES restaurants(id),
  day DATE,
  rate INTEGER CHECK (0 <= rate AND rate <= 5),
  review TEXT,
  owner_answer TEXT,
  answer_approver BIGINT REFERENCES users(id)
                 CHECK( answer_approver IS NULL OR is_admin(answer_approver)),
  modified TIMESTAMP DEFAULT now(),
  PRIMARY KEY (user_id, restaurant_id, day)
);

CREATE TRIGGER rates_dater BEFORE UPDATE OF owner_answer ON rates FOR EACH ROW EXECUTE PROCEDURE dater();

CREATE TABLE rates_rates (
  user_id BIGINT REFERENCES users(id),
  r_user_id BIGINT,
  r_restaurant_id BIGINT,
  r_day DATE,
  rate BOOLEAN,
  FOREIGN KEY (r_user_id, r_restaurant_id, r_day) REFERENCES rates(user_id, restaurant_id, day),
  PRIMARY KEY (user_id, r_user_id, r_restaurant_id, r_day)
);

CREATE TABLE registrations (
  email TEXT UNIQUE,
  password TEXT,
  uuid TEXT PRIMARY KEY,
  expiration TIMESTAMP DEFAULT (now() + INTERVAL '2 hours')
);


CREATE MATERIALIZED VIEW rate_stats AS (
  SELECT
    r.user_id,
    r.restaurant_id,
    r.day,
    COUNT(rr.rate) FILTER (WHERE rr.rate=FALSE) AS down,
    COUNT(rr.rate) FILTER (WHERE rr.rate=TRUE) AS up,
    coalesce(avg(rr.rate::INTEGER), 0.0) AS average
  FROM rates AS r LEFT OUTER JOIN rates_rates AS rr ON r.user_id = rr.r_user_id AND r.restaurant_id = rr.r_restaurant_id AND r.day = rr.r_day
  GROUP BY r.user_id, r.restaurant_id, r.day
);

CREATE FUNCTION rate_stats_refresh() RETURNS TRIGGER AS $$
  BEGIN
    REFRESH MATERIALIZED VIEW rate_stats;
    RETURN NULL;
  END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER a_rates_rates_modified AFTER INSERT OR UPDATE OR DELETE ON rates_rates FOR EACH STATEMENT EXECUTE PROCEDURE rate_stats_refresh();
CREATE TRIGGER a_rates_modified AFTER INSERT OR UPDATE OF user_id, restaurant_id, day OR DELETE ON rates FOR EACH STATEMENT EXECUTE PROCEDURE rate_stats_refresh();


CREATE MATERIALIZED VIEW user_stats AS (
  SELECT
    id,
    coalesce(avg(round(average)), 0.0) AS rate,
    sum(up) AS up,
    sum(down) AS down
  FROM users LEFT OUTER JOIN rate_stats ON id=user_id
  GROUP BY id
);

CREATE FUNCTION user_stats_refresh() RETURNS TRIGGER AS $$
  BEGIN
    REFRESH MATERIALIZED VIEW user_stats;
    RETURN NULL;
  END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER b_rates_rates_modified AFTER INSERT OR UPDATE OR DELETE ON rates_rates FOR EACH STATEMENT EXECUTE PROCEDURE user_stats_refresh();
CREATE TRIGGER b_rates_modified AFTER INSERT OR UPDATE OF user_id, restaurant_id, day OR DELETE ON rates FOR EACH STATEMENT EXECUTE PROCEDURE user_stats_refresh();
CREATE TRIGGER b_users_modified AFTER INSERT OR UPDATE OF id OR DELETE ON users FOR EACH STATEMENT EXECUTE PROCEDURE user_stats_refresh();


CREATE MATERIALIZED VIEW restaurant_stats AS (
  SELECT id, rate, rates_count, row_number() OVER (PARTITION BY state, province, city ORDER BY rate DESC, name ASC) AS classification
  FROM (
    SELECT
      r.id,
      r.name,
      r.state,
      r.province,
      r.city,
      coalesce(sum((1+u.rate)*rt.rate)/sum(1+u.rate), 0.0) AS rate,
      count(rt.rate) AS rates_count
    FROM restaurants AS r LEFT OUTER JOIN (rates AS rt JOIN user_stats AS u ON rt.user_id=u.id) ON r.id=rt.restaurant_id
    GROUP BY r.id
  ) AS tmp
);

CREATE FUNCTION restaurant_stats_refresh() RETURNS TRIGGER AS $$
  BEGIN
    REFRESH MATERIALIZED VIEW restaurant_stats;
    RETURN NULL;
  END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER c_rates_rates_modified AFTER INSERT OR UPDATE OR DELETE ON rates_rates FOR EACH STATEMENT EXECUTE PROCEDURE restaurant_stats_refresh();
CREATE TRIGGER c_rates_modified AFTER INSERT OR UPDATE OF user_id, restaurant_id, day OR DELETE ON rates FOR EACH STATEMENT EXECUTE PROCEDURE restaurant_stats_refresh();
CREATE TRIGGER c_users_modified AFTER INSERT OR UPDATE OF id OR DELETE ON users FOR EACH STATEMENT EXECUTE PROCEDURE restaurant_stats_refresh();
CREATE TRIGGER c_restaurants_modified AFTER INSERT OR UPDATE OF id OR DELETE ON restaurants FOR EACH STATEMENT EXECUTE PROCEDURE restaurant_stats_refresh();
